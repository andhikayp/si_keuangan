<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login'); 
});

//Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','HomeController@login');
Route::get('/log-out', 'HomeController@logout');
Route::get('/home', 'HomeController@dash');

Route::get('/pilih-proyek','HomeController@pilih');
Route::post('/choose','HomeController@chooseproyek');

Route::group(['middleware' => ['role:Komisaris|Manajer|Admin']], function () {
    Route::post('/tambah_proyek','HomeController@tambah_proyek');
    Route::get('/pemesan/get/{kavling}', 'DependencyController@getDataPemesan');
    Route::get('/investor/get/{id}', 'DependencyController@getDataInvestor');
    Route::get('/kode_akun/get/{idakun}', 'DependencyController@getDataTagihan');
});

Route::group(['middleware' => ['permission:create-pemesanan']], function () {
    Route::get('/pemesanan/form', 'PemesananController@form');
    Route::post('/tambah_pesan','PemesananController@tambah_pesan');
    Route::post('/ppjb', 'PemesananController@ppjb');
});

Route::group(['middleware' => ['permission:view-pemesanan']], function () {
    Route::get('/pemesanan', 'PemesananController@index');
    Route::get('/pemesanan/detail/{id}', 'PemesananController@detail');
    Route::post('/ppjb/pdf','PdfController@ppjb');
    Route::get('/pemesanan/detail/ppjb/{ppjb}', 'PemesananController@lihat_ppjb')->name('pemesanan.ppjb');
});

Route::group(['middleware' => ['permission:approval-kuitansi']], function () {
    Route::get('/approval_kuitansi','ApprovalController@kuitansi');
    Route::post('/terimakwt','ApprovalController@terimakwt');
    Route::post('/tolakkwt','ApprovalController@tolakkwt');
    Route::post('/batalkwt','ApprovalController@batalkwt');
    Route::get('/kwitansi/{id}','PdfController@kwitansi');
});

Route::group(['middleware' => ['permission:approval-ppjb']], function () {
    Route::get('/approval_surat','ApprovalController@surat');
    Route::post('/terimappjb','ApprovalController@terimappjb');
    Route::post('/tolakppjb','ApprovalController@tolakppjb');
    Route::post('/batalppjb','ApprovalController@batalppjb');
});

Route::group(['middleware' => ['permission:approval-pembatalan']], function () {
    Route::get('/approval_pembatalan', 'ApprovalController@pembatalan');
    Route::post('/terimapembatalan', 'ApprovalController@terimapembatalan');
    Route::post('/tolakpembatalan', 'ApprovalController@tolakpembatalan');
    Route::post('/batalpembatalan', 'ApprovalController@batalpembatalan');
});

Route::group(['middleware' => ['permission:create-kodeakun']], function () {
    Route::post('/tambah_akun','MasterdataController@tambah_kode');
});

Route::group(['middleware' => ['permission:view-kodeakun']], function () {
    Route::get('/kode-akun','MasterdataController@kodeakun');
});

Route::group(['middleware' => ['permission:create-datakavling']], function () {
    Route::post('/tambah_kavling','MasterdataController@tambah_kavling');
    Route::post('/editharga', 'MasterdataController@edit_harga');
    Route::post('/hapus-data-kavling', 'MasterdataController@hapus_kavling');
});

Route::group(['middleware' => ['permission:view-datakavling']], function () {
    Route::get('/data-kavling','MasterdataController@datakavling');
});

Route::group(['middleware' => ['permission:create-transaksi']], function () {
    Route::get('/penerimaan/form','PenerimaanController@form');
    Route::post('/tambah_transaksi','PenerimaanController@form_post');
    Route::get('/pengeluaran/form','PengeluaranController@form');
    Route::post('/tambah_pengeluaran','PengeluaranController@form_post');
});

Route::group(['middleware' => ['permission:view-transaksi']], function () {
    Route::get('/penerimaan','PenerimaanController@index');
    //Route::get('/penerimaan/detail','PenerimaanController@detail');
    Route::get('/pengeluaran','PengeluaranController@index');
    //Route::get('/pengeluaran/detail','PengeluaranController@detail');
});

Route::group(['middleware' => ['permission:create-pembatalan']], function () {
    Route::get('/pembatalan_pesan/form','PembatalanPesanController@form_batal');
    Route::post('/pembatalan_pesan/form', 'PembatalanPesanController@tambah_batal');
    Route::get('/pembatalan_beli/form','PembatalanBeliController@form_batal');
    Route::post('/pembatalan_beli/form','PembatalanBeliController@tambah_batal');
});

Route::group(['middleware' => ['permission:view-pembatalan']], function () {
    Route::get('/pembatalan_pesan','PembatalanPesanController@index');
    Route::get('/pembatalan_pesan/detail/{id}','PembatalanPesanController@detail');
    Route::get('/pembatalan_beli','PembatalanBeliController@index');
    Route::get('/pembatalan_beli/detail/{id}','PembatalanBeliController@detail');
});

Route::group(['middleware' => ['permission:view-laporan']], function () {
    Route::get('/keuangan/penerimaan','KeuanganController@penerimaan');
    Route::get('/keuangan/penerimaan/detail/{id}','KeuanganController@detail_penerimaan');
    Route::get('/keuangan/pengeluaran','KeuanganController@pengeluaran');
    Route::get('/keuangan/pengeluaran/detail/{id}','KeuanganController@detail_pengeluaran');
    Route::get('/keuangan/laba_rugi','KeuanganController@laba_rugi');
    Route::get('/keuangan/neraca','KeuanganController@neraca');
    Route::get('/keuangan/arus_kas','KeuanganController@arus_kas');
});

Route::group(['middleware' => ['permission:create-feemarketing']], function () {
    Route::post('/tambah_fee','FeeMarketController@tambah_fee');
    Route::post('/bayar_fee','FeeMarketController@bayar_fee');
});

Route::group(['middleware' => ['permission:view-feemarketing']], function () {
    Route::get('/fee-marketing','FeeMarketController@index');
    Route::get('/fee-marketing/history/{id}','FeeMarketController@history');
});

Route::group(['middleware' => ['permission:create-siteplan']], function () {
    Route::post('/tambah_gambar', 'SiteplanController@tambah_img');
});

Route::group(['middleware' => ['permission:view-siteplan']], function () {
    // Route::get('/siteplan', 'SiteplanController@index');
    Route::get('/site-plan', 'SiteplanController@index');
});

Route::group(['middleware' => ['permission:create-investor']], function () {
    Route::get('/investor/form_setor_modal', 'InvestorController@form_setor_modal');
    Route::post('/investor/form_setor_modal', 'InvestorController@setor_modal');
    Route::get('/investor/form_bayar_investor', 'InvestorController@form_bayar_investor');
    Route::post('/investor/form_bayar_investor', 'InvestorController@bayar_investor');
    Route::post('/investor/bukti_perjanjian', 'InvestorController@bukti_perjanjian');
});

Route::group(['middleware' => ['permission:view-investor']], function () {
    Route::get('/investor', 'InvestorController@index');
    Route::get('/investor/detail/{id}', 'InvestorController@detail');
    Route::get('/investor/detail/perjanjian/{perjanjian}', 'InvestorController@lihat_perjanjian')->name('investor.perjanjian');
});

Route::group(['middleware' => ['permission:create-user']], function () {
    Route::post('/user/manage', 'HomeController@manage_user');
    Route::post('/tambah_user','HomeController@tambah_user');
    Route::get('/user/edit/{id}', 'HomeController@edit_user');
    Route::post('/user/edit_user', 'HomeController@save_edit_user');
    Route::post('/user/resetpass', 'HomeController@resetpass');
});

Route::group(['middleware' => ['permission:view-user']], function () {
    Route::get('/user', 'HomeController@lihat_user');
    Route::get('/user/detail/{id}', 'HomeController@detail_user');
    Route::post('/user/ubahpass', 'HomeController@ubahpass');
});

Route::group(['middleware' => ['permission:config-email']], function () {
    Route::get('/konfigurasi_email', 'KonfigurasiController@email');
    Route::get('/konfigurasi_sms', 'KonfigurasiController@sms');
    Route::get('/konfigurasi_surat', 'KonfigurasiController@surat');
    Route::post('/konfigtgt','KonfigurasiController@konfigtenggat');
    Route::post('/konfigpgt','KonfigurasiController@konfigperingatan');
});

Route::group(['middleware' => ['role_or_permission:Komisaris|manage-rolepermission']], function () {
    Route::get('/users/roles/{id}', 'HomeController@roles')->name('users.roles');
    Route::put('/users/roles/{id}', 'HomeController@setRole')->name('users.set_role');
    Route::post('/users/permission', 'HomeController@addPermission')->name('users.add_permission');
    Route::get('/users/role-permission', 'HomeController@rolePermission')->name('users.roles_permission');
    Route::put('/users/permission/{role}', 'HomeController@setRolePermission')->name('users.setRolePermission');
    Route::resource('/role', 'RoleController')->except([
        'create', 'show', 'edit', 'update'
    ]);
});

//Route::get('/pembatalan_beli/form_pengembalian','PembatalanBeliController@form_pengembalian');
//Route::get('/angsuran/form', 'AngsuranController@form');
//Route::get('/angsuran/history', 'AngsuranController@history');
// Route::get('/progress','ProgressController@index');