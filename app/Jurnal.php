<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    //
    public function transaksis(){
        return $this->belongsTo('App\Transaksi', 'transaksis_id');
    }
    public function proyeks(){
        return $this->belongsTo('App\proyek', 'proyeks_id');
    }
    public function kode_akun_coas(){
        return $this->belongsTo('App\KodeAkunCoa', 'kode_akun_coas_id');
    }
}
