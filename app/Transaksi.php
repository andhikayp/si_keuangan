<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function proyeks(){
        return $this->belongsTo('App\proyek', 'proyeks_id');
    }
    public function pemesanans(){
        return $this->belongsTo('App\pemesanan', 'pemesanans_id');
    }
    public function kode_akuns(){
        return $this->belongsTo('App\KodeAkun', 'kode_akuns_id');
    }
    public function pembatalans(){
        return $this->belongsTo('App\Pembatalan', 'pembatalans_id');
    }
    public function investors(){
        return $this->belongsTo('App\Investor', 'investors_id');
    }
    public function penginput(){
        return $this->belongsTo('App\User', 'penginput_id');
    }
    public function penyetujui(){
        return $this->belongsTo('App\User', 'penyetujui_id');
    }
    public function jurnal(){
        return $this->hasMany('App\Jurnal', 'transaksis_id');
    }
}
