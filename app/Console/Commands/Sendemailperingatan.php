<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\pemesanan;
use App\konfigurasi;
use Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailPeringatan;
use Transaksi;
use KodeAkun;

class Sendemailperingatan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:peringatan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengirim email peringatan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $pesanan = pemesanan::all();
        $konfig = konfigurasi::all()->first();
        $date = \Carbon\Carbon::yesterday()->toDateString();
        $tanggal = \Carbon\Carbon::yesterday()->format('d');
        foreach($pesanan as $daftar){ 
            if($daftar->dp2_tgl == $date){
                $ada = Transaksi::where(array(['pemesanans_id','=',$daftar->id],['kode_akuns_id','=','A2']))->count();
                if($ada<2) Mail::to($daftar->email)->send(new EmailPeringatan($konfig,$daftar,$bayar='DP2'));
            }
            if($daftar->dp3_tgl == $date){
                $ada = Transaksi::where(array(['pemesanans_id','=',$daftar->id],['kode_akuns_id','=','A2']))->count();
                if($ada<3) Mail::to($daftar->email)->send(new EmailPeringatan($konfig,$daftar,$bayar='DP3'));
            }
            if($daftar->tgl_tempo == intval($tanggal)){
                $ada = Transaksi::where(array(['pemesanans_id','=',$daftar->id],['kode_akuns_id','=','A3'],['tgl_transaksi','=',\Carbon\Carbon::yesterday()->toDateString()]))->first();
                if(!isset($ada->id)) Mail::to($daftar->email)->send(new EmailPeringatan($konfig,$daftar,$bayar='Angsuran'));
            }   
        }
    }
}
