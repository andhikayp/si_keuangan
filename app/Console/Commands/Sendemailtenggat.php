<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\pemesanan;
use App\konfigurasi;
use Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailTenggat;

class Sendemailtenggat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:tenggat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengirimkan email ketika tenggat';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $pesanan = pemesanan::all();
        $konfig = konfigurasi::all()->first();
        $date = \Carbon\Carbon::tomorrow()->toDateString();
        $tanggal = \Carbon\Carbon::tomorrow()->format('d');
        foreach($pesanan as $daftar){
            if($daftar->dp2_tgl == $date){
                Mail::to($daftar->email)->send(new EmailTenggat($konfig,$daftar,$bayar='DP2'));
            }
            if($daftar->dp3_tgl == $date){
                Mail::to($daftar->email)->send(new EmailTenggat($konfig,$daftar,$bayar='DP3'));
            }
            if($daftar->tgl_tempo == intval($tanggal)){
                Mail::to($daftar->email)->send(new EmailTenggat($konfig,$daftar,$bayar='Angsuran'));
            }   
        }
    }
}
