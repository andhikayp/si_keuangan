<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pemesanan extends Model
{
    //
    public function kavlings(){
        return $this->belongsTo('App\kavling','kavlings_id');
    }
    public function proyeks(){
        return $this->belongsTo('App\proyek','proyeks_id');
    }
    public function transaksis(){
        return $this->hasMany('App\Transaksi', 'pemesanans_id');
    }
    public function pembatalans(){
        return $this->hasOne('App\Pembatalan', 'pemesanans_id');
    }
    public function marketings(){
        return $this->belongsTo('App\User', 'marketing_id');
    }
    public function penginput(){
        return $this->belongsTo('App\User', 'penginput_id');
    }
    public function penyetujui(){
        return $this->belongsTo('App\User', 'penyetujui_id');
    }
}
