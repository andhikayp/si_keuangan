<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeAkunCoa extends Model
{
    //
    public function jurnals(){
        return $this->hasMany('App\Jurnal', 'kode_akun_coas_id');
    }
    public function kode_akuns(){
        return $this->belongsTo('App\KodeAkun','kode_akuns_id');
    }
    public function akun_debit(){
        return $this->belongsTo('App\AkunCoa','debit');
    }
    public function akun_kredit(){
        return $this->belongsTo('App\AkunCoa','kredit');
    }
}
