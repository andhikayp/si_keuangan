<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AkunCoa extends Model
{
    //
    public function coas(){
        return $this->belongsTo('App\Coa','coas_id');
    }
}
