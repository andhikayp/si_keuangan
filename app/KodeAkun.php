<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeAkun extends Model
{
    public function transaksis(){
        return $this->hasMany('App\Transaksi', 'kode_akuns_id');
    }
    public function parent()
    {
        return $this->belongsTo('App\KodeAkun', 'parent');
    }
    public function akun_coas(){
        return $this->hasMany('App\KodeAkunCoa', 'kode_akuns_id');
    }
    public function child()
    {
        return $this->hasMany('App\KodeAkun', 'parent');
    }
}
