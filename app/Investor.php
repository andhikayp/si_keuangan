<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investor extends Model
{
    public function proyeks(){
        return $this->belongsTo('App\proyek', 'proyeks_id');
    }
    public function users(){
        return $this->belongsTo('App\User', 'users_id');
    }
    public function transaksis(){
        return $this->hasMany('App\Transaksi', 'investors_id');
    }
    public function penginput(){
        return $this->belongsTo('App\User', 'penginput_id');
    }
    public function penyetujui(){
        return $this->belongsTo('App\User', 'penyetujui_id');
    }
}
