<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTenggat extends Mailable
{
    use Queueable, SerializesModels;
    public $konfig;
    public $detail;
    public $bayar;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($konfig,$detail,$bayar)
    {
        //
        $this->konfig = $konfig;
        $this->detail = $detail;
        $this->bayar = $bayar;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.tenggat')->from('jayaadam1234567@gmail.com','Jaya Adam')
        ->subject($this->konfig->subjek_tenggat);
    }
}
