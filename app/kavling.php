<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kavling extends Model
{
    //
    public function proyeks(){
        return $this->belongsTo('App\proyek', 'proyeks_id');
    }
    public function pemesanans(){
        return $this->hasMany('App\pemesanan','kavlings_id');
    }
}
