<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coa extends Model
{
    //
    public function akun_coas(){
        return $this->hasMany('App\AkunCoa', 'coas_id');
    }
}
