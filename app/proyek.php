<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proyek extends Model
{
    //
    // protected $table = 'proyeks';
    // protected $primaryKey = 'id';
    // public $timestamps = true;
    // public $incrementing = true;

    // protected $fillable = array(
    //    'name', 'alamat', 'jml_kavling', 'foto'
    // );
    // // public function mahasiswa_matakuliah(){
    // //     return $this->hasMany('App\Mahasiswa_Matakuliah');
    // // }
    public function kavlings(){
        return $this->hasMany('App\kavling', 'proyeks_id');
    }
    public function pemesanans(){
        return $this->hasMany('App\pemesanan','proyeks_id');
    }
    public function investors(){
        return $this->hasMany('App\Investor', 'proyeks_id');
    }
    public function pembatalans(){
        return $this->hasMany('App\Pembatalan', 'proyeks_id');
    }
    public function transaksis(){
        return $this->hasMany('App\Transaksi', 'proyeks_id');
    }
    public function jurnals(){
        return $this->hasMany('App\Jurnal', 'proyeks_id');
    }
}
