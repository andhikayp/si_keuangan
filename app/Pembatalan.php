<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembatalan extends Model
{
    public function proyeks(){
        return $this->belongsTo('App\proyek', 'proyeks_id');
    }
    public function pemesanans(){
        return $this->belongsTo('App\pemesanan', 'pemesanans_id');
    }
    public function transaksis(){
        return $this->hasMany('App\Transaksi', 'pembatalans_id');
    }
    public function penginput(){
        return $this->belongsTo('App\User', 'penginput_id');
    }
    public function penyetujui(){
        return $this->belongsTo('App\User', 'penyetujui_id');
    }
}
