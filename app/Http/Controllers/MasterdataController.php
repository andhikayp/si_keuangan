<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kavling;
use App\KodeAkun;
use App\proyek;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MasterdataController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function kodeakun()
    {
        $pengeluaran = KodeAkun::where('tipe','=',PENGELUARAN)->whereNull('parent')->orderBy('id','ASC')->get();
        $penerimaan = KodeAkun::where('tipe','=',PENERIMAAN)->whereNull('parent')->orderBy('id','ASC')->get();
        $akun = KodeAkun::all();
        return view('master_data.kodeakun',compact('pengeluaran','penerimaan','akun'));
    }

    public function datakavling()
    {
        $tampil = kavling::where('proyeks_id', '=', session('id_proyek'))->get();
        return view('master_data.datakavling', ['tampil'=>$tampil]); 
    }

    public function tambah_kavling(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'No_kavling'=>'required|unique:kavlings,name',
            'Lebar'=>'required|numeric',
            // 'Lebar_belakang'=>'required|numeric',
            // 'Panjang_kiri'=>'required|numeric',
            'Panjang'=>'required|numeric',
            'Luas'=>'required|numeric',
            'Harga'=>'required|numeric',
            'Diskon'=>'required|numeric',
            'hpp'=>'required|numeric',
        ]);
        // var_dump($r);return;

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        $proyek = proyek::find(session('id_proyek'));
        $kavling = kavling::where('proyeks_id', '=', session('id_proyek'))->count();
        if ($proyek->jmlh_kavling < $kavling+1) {
            $proyek->jmlh_kavling = $proyek->jmlh_kavling+1;
            $proyek->save();
        }
        if($r->input('Harga')<=$r->input('Diskon')) return Redirect::back()->withErrors(['message1'=>'Diskon harus kurang dari harga asli!']); 
        $kavling = new kavling;
        $kavling->proyeks_id = session('id_proyek');
        $kavling->name = $r->input('No_kavling');
        // $kavling->depan = $r->input('Lebar_depan');
        // $kavling->belakang = $r->input('Lebar_belakang');
        // $kavling->kiri = $r->input('Panjang_kiri');
        // $kavling->kanan = $r->input('Panjang_kanan');
        $kavling->panjang = $r->input('Panjang');
        $kavling->lebar = $r->input('Lebar');
        $kavling->luas = $r->input('Luas');
        $kavling->harga = $r->input('Harga');
        $kavling->diskon = $r->input('Diskon');
        $kavling->hpp = $r->input('hpp');
        $kavling->status = KAVLING_AVAILABLE;
        $kavling->save();
        return Redirect::back()->with('message','Data kavling berhasil ditambahkan');  
    }
    public function edit_harga(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'id'=>'required',
            'Harga'=>'required|numeric',
            'Diskon'=>'required|numeric',
            'hpp'=>'required|numeric',
        ]);
        // var_dump($r);return;

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        if($r->input('Harga')<=$r->input('Diskon')) return Redirect::back()->withErrors(['message1'=>'Diskon harus kurang dari harga asli!']); 
        $post = kavling::find($r->input('id'));
        $post->name = $r->input('No_kavling');
        // $post->depan = $r->input('Lebar_depan');
        // $post->belakang = $r->input('Lebar_belakang');
        // $post->kiri = $r->input('Panjang_kiri');
        // $post->kanan = $r->input('Panjang_kanan');
        $post->panjang = $r->input('Panjang');
        $post->lebar = $r->input('Lebar');
        $post->luas = $r->input('Luas');
        $post->harga = $r->input('Harga');
        $post->diskon = $r->input('Diskon');
        $post->hpp = $r->input('hpp');
        $post->save();
        return back()->with('message','Data kavling berhasil diedit');  
    }

    public function tambah_kode(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'jenis'=>'required',
            'kode-akun'=>'required',
            'nama-akun'=>'required',
        ]);
        // var_dump($r);return;

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        $akun = new KodeAkun;
        $akun->tipe = $r->input('jenis')-1;
        if($r->input('jenis')==1){
            if(!is_null(KodeAkun::where('kode_akun','=','A'.$r->input('kode-akun'))->first())) return Redirect::back()->withErrors(['message1'=>'Kode Akun telah dipakai!']); 
            $akun->kode_akun = 'A'.$r->input('kode-akun');
        }
        else if($r->input('jenis')==2){
            if(!is_null(KodeAkun::where('kode_akun','=','B'.$r->input('kode-akun'))->first())) return Redirect::back()->withErrors(['message1'=>'Kode Akun telah dipakai!']); 
            $akun->kode_akun = 'B'.$r->input('kode-akun');
        }
        $akun->nama_akun = $r->input('nama-akun');
        if($r->input('parent')!="new")$akun->parent = $r->input('parent');
        $akun->save();
        return back()->with('message','Kode Akun berhasil ditambahkan!');
    }

    public function hapus_kavling(Request $r){
        $kavling = kavling::find($r->input('kavling'));
        if($kavling->status == KAVLING_AVAILABLE)$kavling->forceDelete();
        else return Redirect::back()->withErrors(['message1'=>'Kavling digunakan! Tidak bisa dihapus!']); 
        return back()->with('message','Kavling Berhasil di hapus');
    }
}
