<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\pemesanan;
use App\Transaksi;
use Illuminate\Support\Facades\Auth;
use App\kavling;
use App\User;
use App\KodeAkun;
use App\Investor;
use App\Pembatalan;
use App\Jurnal;
use App\AkunCoa;
use App\KodeAkunCoa;
use Illuminate\Support\Facades\Redirect;

class ApprovalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function kuitansi()
    {
        $in = Transaksi::where([['proyeks_id','=',session('id_proyek')],['jenis_transaksi','=',PENERIMAAN]])
        ->orderBy('status','ASC')->orderBy('tgl_transaksi','DESC')->get();
        $out = Transaksi::where([['proyeks_id','=',session('id_proyek')],['jenis_transaksi','=',PENGELUARAN]])
        ->orderBy('status','ASC')->orderBy('tgl_transaksi','DESC')->get();
        // $investor_in = Transaksi::where('jenis_transaksi','=',PENERIMAAN)
        // ->whereNotNull('investors_id')
        // ->orderBy('status','ASC')->orderBy('tgl_transaksi','DESC')->get();
        // $investor_out = Transaksi::where('jenis_transaksi','=',PENGELUARAN)
        // ->whereNotNull('investors_id')
        // ->orderBy('status','ASC')->orderBy('tgl_transaksi','DESC')->get();
        return view('approval.kuitansi',compact('in','out'));
    }

    public function surat()
    {
        // $daftar = pemesanan::where('proyeks_id','=',session('id_proyek'))->whereNotNull('ppjb')->get();
        $daftar = pemesanan::where('proyeks_id','=',session('id_proyek'))->where('ppjb', null)->get();
        return view('approval.surat',compact('daftar'));
    }

    public function pembatalan()
    {
        $batalPesans = Pembatalan::where('proyeks_id','=',session('id_proyek'))->where('jenis_pembatalan','=',BATAL_PESAN)->get();
        $batalBelis = Pembatalan::where('proyeks_id','=',session('id_proyek'))->where('jenis_pembatalan','=',BATAL_BELI)->get();
        return view('approval.pembatalan',compact('batalPesans','batalBelis'));
    }

    public function terimapembatalan(Request $request)
    {
        $batal = Pembatalan::find($request->input('id-batal'));
        $batal->status = DISETUJUI;
        $batal->penyetujui_id = auth()->user()->id;
        $batal->save();

        kavling::where('id',$request->input('id-kavling'))->update(['status'=>KAVLING_AVAILABLE]);
        return back();
    }

    public function tolakpembatalan(Request $request)
    {
        $batal = Pembatalan::find($request->input('id-batal'));
        $batal->status = DITOLAK;
        $batal->penyetujui_id = auth()->user()->id;
        $batal->save();

        pemesanan::where('id',$request->input('id-pesan'))->update(['status_pembatalan'=>TIDAK_BATAL]);
        return back();
    }

    public function batalpembatalan(Request $request)
    {
        $batal = Pembatalan::find($request->input('id-batal'));
        if ($batal->status == DISETUJUI) {
            $batal->status = PENDING;
            $batal->penyetujui_id = null;
            $batal->save();

            kavling::where('id',$request->input('id-kavling'))->update(['status'=>KAVLING_BOOKING]);
            return back();

        } elseif ($batal->status == DITOLAK) {
            $batal->status = PENDING;
            $batal->penyetujui_id = null;
            $batal->save();

            $pesan = pemesanan::find($request->input('id-pesan'));
            if ($pesan->status >= DP1) {
                $pesan->status_pembatalan = BATAL_BELI;
            } elseif ($pesan->status <= UTJ) {
                $pesan->status_pembatalan = BATAL_PESAN;
            }
            $pesan->save();
            return back();
        }
    }

    public function terimappjb(Request $r)
    {
        $pesan = pemesanan::find($r->input('ppjb'));
        // var_dump($r->input('ppjb'));return;
        $pesan->status_ppjb = DISETUJUI;
        $pesan->penyetujui_id = Auth::id();
        $pesan->save();
        return back();
    }

    public function tolakppjb(Request $r)
    {
        $pesan = pemesanan::find($r->input('ppjb'));
        $pesan->status_ppjb = DITOLAK;
        $pesan->penyetujui_id = Auth::id();
        $pesan->save();
        return back();
    }

    public function batalppjb(Request $r)
    {
        $pesan = pemesanan::find($r->input('ppjb'));
        $pesan->status_ppjb = PENDING;
        $pesan->penyetujui_id = Auth::id();
        $pesan->save();
        return back();
    }

    public function terimakwt(Request $r)
    {
        $pesan = Transaksi::find($r->input('kwt'));
        if($pesan->jenis_transaksi==PENERIMAAN){
            if(!is_null($pesan->pemesanans_id)){
                $pesanan = pemesanan::find($pesan->pemesanans_id);
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();
                if($kode_akun->kode_akun=='A1'){
                    $pesanan->status = UTJ;
                    $pesan->status = DISETUJUI;
                    $pesan->penyetujui_id = Auth::id();

                    $jurnal1 = new Jurnal;
                    $jurnal1->tanggal = \Carbon\Carbon::now();
                    $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                    $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                    $jurnal1->debit = $pesan->jumlah;
                    $jurnal1->transaksis_id = $pesan->id;
                    $jurnal1->proyeks_id = session('id_proyek');
                    $jurnal1->save();

                    $jurnal2 = new Jurnal;
                    $jurnal2->tanggal = \Carbon\Carbon::now();
                    $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                    $jurnal2->kredit = $pesan->jumlah;
                    $jurnal2->transaksis_id = $pesan->id;
                    $jurnal2->proyeks_id = session('id_proyek');
                    $jurnal2->save();

                    $pesan->save();
                }else if($kode_akun->kode_akun=='A2'){ ////////////////////OOOOOOOOOOO
                    $jumlah = Transaksi::where(array(['pemesanans_id','=',$pesan->pemesanans_id],['kode_akuns_id','=',$kode_akun->id],['status','=',DISETUJUI]))->count();
                    if($jumlah > 0){
                        if($pesanan->status_ppjb != DISETUJUI) return Redirect::back()->withErrors(['message1'=>'PPJB belum disetujui belum bisa membayar DP']);
                    }
                    $pesanan->status = UTJ+$jumlah+1;
                    $pesan->status = DISETUJUI;
                    $pesan->penyetujui_id = Auth::id();

                    if($jumlah == 0 || $jumlah == 1){ // DP 1 dan 2
                        $jurnal1 = new Jurnal;
                        $jurnal1->tanggal = \Carbon\Carbon::now();
                        $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                        $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                        $jurnal1->debit = $pesan->jumlah;
                        $jurnal1->transaksis_id = $pesan->id;
                        $jurnal1->proyeks_id = session('id_proyek');
                        $jurnal1->save();

                        $jurnal2 = new Jurnal;
                        $jurnal2->tanggal = \Carbon\Carbon::now();
                        $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                        $jurnal2->kredit = $pesan->jumlah;
                        $jurnal2->transaksis_id = $pesan->id;
                        $jurnal2->proyeks_id = session('id_proyek');
                        $jurnal2->save();
                    }
                    else if($jumlah == 2){ // DP 3
                        // Kas
                        $jurnal1 = new Jurnal;
                        $jurnal1->tanggal = \Carbon\Carbon::now();
                        $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                        $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                        $jurnal1->debit = $pesan->jumlah;
                        $jurnal1->transaksis_id = $pesan->id;
                        $jurnal1->proyeks_id = session('id_proyek');
                        $jurnal1->save();

                        // Pendapatan dibayar di muka
                        $jurnal3 = new Jurnal;
                        $jurnal3->tanggal = \Carbon\Carbon::now();
                        $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                        $jurnal3->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal3->kode_akun_coas_id = $kodeAkunCoas[2]->id;
                        $jurnal3->debit = Transaksi::where(array(['pemesanans_id','=',$pesan->pemesanans_id],['kode_akuns_id','=',$kode_akun->id],['status','=',DISETUJUI]))->sum('jumlah');
                        //$jurnal3->debit += Transaksi::where(array(['pemesanans_id','=',$pesan->pemesanans_id],['kode_akuns_id','=','A1'],['status','=',DISETUJUI]))->sum('jumlah');
                        $jurnal3->transaksis_id = $pesan->id;
                        $jurnal3->proyeks_id = session('id_proyek');
                        $jurnal3->save();

                        // Piutang
                        $jurnal4 = new Jurnal;
                        $jurnal4->tanggal = \Carbon\Carbon::now();
                        $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                        $jurnal4->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal4->kode_akun_coas_id = $kodeAkunCoas[3]->id;
                        $piutang = pemesanan::find($pesan->pemesanans_id);
                        $jurnal4->debit = $piutang->angsur * $piutang->angsur_jml;
                        $jurnal4->transaksis_id = $pesan->id;
                        $jurnal4->proyeks_id = session('id_proyek');
                        $jurnal4->save();

                        // Diskon
                        $jurnal2 = new Jurnal;
                        $jurnal2->tanggal = \Carbon\Carbon::now();
                        $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal2->kode_akun_coas_id = $kodeAkunCoas[4]->id;
                        $jurnal2->debit = $piutang->kavlings->diskon;
                        $jurnal2->transaksis_id = $pesan->id;
                        $jurnal2->proyeks_id = session('id_proyek');
                        $jurnal2->save();

                        // HPP
                        $jurnal5 = new Jurnal;
                        $jurnal5->tanggal = \Carbon\Carbon::now();
                        $jurnal5->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal5->kode_akun_coas_id = $kodeAkunCoas[5]->id;
                        $jurnal5->debit = $piutang->kavlings->hpp;
                        $jurnal5->transaksis_id = $pesan->id;
                        $jurnal5->proyeks_id = session('id_proyek');
                        $jurnal5->save();

                        // Persediaan
                        $jurnal6 = new Jurnal;
                        $jurnal6->tanggal = \Carbon\Carbon::now();
                        $jurnal6->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal6->kode_akun_coas_id = $kodeAkunCoas[6]->id;
                        $jurnal6->kredit = $piutang->kavlings->hpp;
                        $jurnal6->transaksis_id = $pesan->id;
                        $jurnal6->proyeks_id = session('id_proyek');
                        $jurnal6->save();

                        // Pendapatan
                        $jurnal7 = new Jurnal;
                        $jurnal7->tanggal = \Carbon\Carbon::now();
                        $jurnal7->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                        $jurnal7->kode_akun_coas_id = $kodeAkunCoas[7]->id;
                        $jurnal7->kredit = $jurnal4->debit + $jurnal1->debit + $jurnal3->debit;
                        $jurnal7->transaksis_id = $pesan->id;
                        $jurnal7->proyeks_id = session('id_proyek');
                        $jurnal7->save();
                    }

                    $pesan->save();
                }else if($kode_akun->kode_akun=='A3' && $pesanan->status_ppjb==DISETUJUI){
                    $angsur = Transaksi::where(array(
                        ['pemesanans_id','=',$pesanan->id],
                        ['kode_akuns_id','=',$kode_akun->id],
                        ['status','=',DISETUJUI]
                        ))->count();
                    if($angsur+1 == $pesanan->angsur_jml) $pesanan->status = Lunas;
                    else if($angsur+1 < $pesanan->angsur_jml) $pesanan->status = Angsur;
                    $pesan->status = DISETUJUI;
                    $pesan->penyetujui_id = Auth::id();

                    $jurnal1 = new Jurnal;
                    $jurnal1->tanggal = \Carbon\Carbon::now();
                    $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                    $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                    $jurnal1->debit = $pesan->jumlah;
                    $jurnal1->transaksis_id = $pesan->id;
                    $jurnal1->proyeks_id = session('id_proyek');
                    $jurnal1->save();

                    $jurnal2 = new Jurnal;
                    $jurnal2->tanggal = \Carbon\Carbon::now();
                    $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                    $jurnal2->kredit = $pesan->jumlah;
                    $jurnal2->transaksis_id = $pesan->id;
                    $jurnal2->proyeks_id = session('id_proyek');
                    $jurnal2->save();

                    $pesan->save();
                }
                $pesanan->save();
            }
            else if(!is_null($pesan->investors_id)){
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();

                $investor = Investor::find($pesan->investors_id);
                $investor->penyetujui_id = Auth::id();
                $pesan->status = DISETUJUI;
                $pesan->penyetujui_id = Auth::id();

                $jurnal1 = new Jurnal;
                $jurnal1->tanggal = \Carbon\Carbon::now();
                $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                $jurnal1->debit = $pesan->jumlah;
                $jurnal1->transaksis_id = $pesan->id;
                $jurnal1->proyeks_id = session('id_proyek');
                $jurnal1->save();

                $jurnal2 = new Jurnal;
                $jurnal2->tanggal = \Carbon\Carbon::now();
                $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                $jurnal2->kredit = $pesan->jumlah;
                $jurnal2->transaksis_id = $pesan->id;
                $jurnal2->proyeks_id = session('id_proyek');
                $jurnal2->save();

                $investor->save();
                $pesan->save();
            }
        }
        else if($pesan->jenis_transaksi==PENGELUARAN){
            if ($pesan->kode_akuns->kode_akun == 'B5.2'){
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();

                $fee_marketing = pemesanan::find($pesan->pemesanans_id);
                if ($fee_marketing->fee_marketing == null) {
                    return Redirect::back()->withErrors(['message1'=>'Harap Menentukan Fee Marketing terlebih dahulu!']);
                }
                if ($fee_marketing->fee_marketing < ($pesan->jumlah+$fee_marketing->fee_diambil)) {
                    return Redirect::back()->withErrors(['message1'=>'Fee Marketing melebihi jumlah yang ditentukan!']);
                }
                else {
                    $fee_marketing->fee_diambil = $fee_marketing->fee_diambil + $pesan->jumlah;

                    $jurnal1 = new Jurnal;
                    $jurnal1->tanggal = \Carbon\Carbon::now();
                    $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                    $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                    $jurnal1->kredit = $pesan->jumlah;
                    $jurnal1->transaksis_id = $pesan->id;
                    $jurnal1->proyeks_id = session('id_proyek');
                    $jurnal1->save();

                    $jurnal2 = new Jurnal;
                    $jurnal2->tanggal = \Carbon\Carbon::now();
                    $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                    $jurnal2->debit = $pesan->jumlah;
                    $jurnal2->transaksis_id = $pesan->id;
                    $jurnal2->proyeks_id = session('id_proyek');
                    $jurnal2->save();

                    $fee_marketing->save();
                }
            }
            if ($pesan->kode_akuns->kode_akun == 'B12.1'){ ////////////////OOOOOOOOOOOOOOOOOOOO
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();

                // Kas
                $jurnal1 = new Jurnal;
                $jurnal1->tanggal = \Carbon\Carbon::now();
                $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                $jurnal1->kredit = $pesan->jumlah;
                $jurnal1->transaksis_id = $pesan->id;
                $jurnal1->proyeks_id = session('id_proyek');
                $jurnal1->save();

                // Piutang
                $jurnal2 = new Jurnal;
                $jurnal2->tanggal = \Carbon\Carbon::now();
                $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                $piutang = pemesanan::find($pesan->pemesanans_id);
                $kodeAkunAngsuran = KodeAkun::where('kode_akun', 'A3')->pluck('id')->first();
                $angsuran = Transaksi::where(array(
                    ['pemesanans_id','=',$pesan->pemesanans_id],
                    ['kode_akuns_id','=',$kodeAkunAngsuran],
                    ['status','=',DISETUJUI]
                    ))->sum('jumlah');
                $jurnal2->kredit = ($piutang->angsur * $piutang->angsur_jml) - $angsuran;
                $jurnal2->transaksis_id = $pesan->id;
                $jurnal2->proyeks_id = session('id_proyek');
                $jurnal2->save();

                // Persediaan
                $jurnal3 = new Jurnal;
                $jurnal3->tanggal = \Carbon\Carbon::now();
                $jurnal3->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal3->kode_akun_coas_id = $kodeAkunCoas[2]->id;
                $jurnal3->debit = ($piutang->angsur * $piutang->angsur_jml) - $angsuran; // Gak Tau
                $jurnal3->transaksis_id = $pesan->id;
                $jurnal3->proyeks_id = session('id_proyek');
                $jurnal3->save();

                // Kerugian Pembatalan
                $jurnal4 = new Jurnal;
                $jurnal4->tanggal = \Carbon\Carbon::now();
                $jurnal4->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal4->kode_akun_coas_id = $kodeAkunCoas[3]->id;
                $jurnal4->debit = ($piutang->angsur * $piutang->angsur_jml) - $angsuran; // Gak Tau
                $jurnal4->transaksis_id = $pesan->id;
                $jurnal4->proyeks_id = session('id_proyek');
                $jurnal4->save();
                /*$batal = Pembatalan::find($pesan->pembatalans_id)->pluck('jumlah_dikembalikan')->first();
                if($batal == $pesan->jumlah){
                    $jurnal1 = new Jurnal;
                    $jurnal1->tanggal = \Carbon\Carbon::now();
                    $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                    $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal1->jenis_akun = 'Kerugian akibat pembatalan';
                    $jurnal1->nomor_akun = '6-12001';
                    $jurnal1->debit = $pesan->jumlah;
                    $jurnal1->transaksis_id = $pesan->id;
                    $jurnal1->proyeks_id = session('id_proyek');
                    $jurnal1->save();

                    $jurnal2 = new Jurnal;
                    $jurnal2->tanggal = \Carbon\Carbon::now();
                    $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal2->jenis_akun = 'Kas';
                    $jurnal2->nomor_akun = '1-10001';
                    $jurnal2->kredit = $pesan->jumlah;
                    $jurnal2->transaksis_id = $pesan->id;
                    $jurnal2->proyeks_id = session('id_proyek');
                    $jurnal2->save();
                }
                if($batal < $pesan->jumlah){
                    $jurnal1 = new Jurnal;
                    $jurnal1->tanggal = \Carbon\Carbon::now();
                    $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                    $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal1->jenis_akun = 'Kerugian akibat pembatalan';
                    $jurnal1->nomor_akun = '6-12001';
                    $jurnal1->debit = $pesan->jumlah;
                    $jurnal1->transaksis_id = $pesan->id;
                    $jurnal1->proyeks_id = session('id_proyek');
                    $jurnal1->save();

                    $jurnal2 = new Jurnal;
                    $jurnal2->tanggal = \Carbon\Carbon::now();
                    $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    $jurnal2->jenis_akun = 'Hutang Usaha';
                    $jurnal2->nomor_akun = '2-10101';
                    $jurnal2->kredit = $pesan->jumlah;
                    $jurnal2->transaksis_id = $pesan->id;
                    $jurnal2->proyeks_id = session('id_proyek');
                    $jurnal2->save();
                }*/
            }
            if($pesan->kode_akuns->nama_akun == 'B11.1'){
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();

                $jurnal1 = new Jurnal;
                $jurnal1->tanggal = \Carbon\Carbon::now();
                $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                $jurnal1->kredit = $pesan->jumlah;
                $jurnal1->transaksis_id = $pesan->id;
                $jurnal1->proyeks_id = session('id_proyek');
                $jurnal1->save();

                $jurnal2 = new Jurnal;
                $jurnal2->tanggal = \Carbon\Carbon::now();
                $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                $jurnal2->debit = $pesan->jumlah;
                $jurnal2->transaksis_id = $pesan->id;
                $jurnal2->proyeks_id = session('id_proyek');
                $jurnal2->save();
            }
            if($pesan->kode_akuns->nama_akun == 'B11.2'){
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();

                $jurnal1 = new Jurnal;
                $jurnal1->tanggal = \Carbon\Carbon::now();
                $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                $jurnal1->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal1->kode_akun_coas_id = $kodeAkunCoas[0]->id;
                $jurnal1->kredit = $pesan->jumlah;
                $jurnal1->transaksis_id = $pesan->id;
                $jurnal1->proyeks_id = session('id_proyek');
                $jurnal1->save();

                $jurnal2 = new Jurnal;
                $jurnal2->tanggal = \Carbon\Carbon::now();
                $jurnal2->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                $jurnal2->kode_akun_coas_id = $kodeAkunCoas[1]->id;
                $jurnal2->debit = $pesan->jumlah;
                $jurnal2->transaksis_id = $pesan->id;
                $jurnal2->proyeks_id = session('id_proyek');
                $jurnal2->save();
            }
            else{
                $kode_akun = KodeAkun::find($pesan->kode_akuns_id);
                $kodeAkunCoas = KodeAkunCoa::where('kode_akuns_id', $kode_akun->id)->get();
                //return $kodeAkunCoas;

                //$daftarjurnal = KodeAkunCoa::where('kode_akuns_id','=',$pesan->kode_akuns->id)->get();
                // var_dump($pesan->kode_akuns->id,$daftarjurnal);return;
                $nomor = Jurnal::where('tanggal','=',\Carbon\Carbon::now()->toDateString())->count() + 1;
                foreach($kodeAkunCoas as $jurnal){
                    $new = new Jurnal;
                    $new->tanggal = \Carbon\Carbon::now();
                    $new->nomor_jurnal = 'J'.\Carbon\Carbon::now()->format('dmy').str_pad($nomor++, 4, '0', STR_PAD_LEFT);
                    if(!is_null($jurnal->debit)){
                        // var_dump($pesan->kode_akuns->id,$daftarjurnal);return;
                        $new->kode_akun_coas_id = $jurnal->id;
                        $new->debit = $pesan->jumlah;
                    }
                    if(!is_null($jurnal->kredit)){
                        $new->kode_akun_coas_id = $jurnal->id;
                        $new->kredit = $pesan->jumlah;
                    }
                    $new->transaksis_id = $pesan->id;
                    $new->proyeks_id = session('id_proyek');
                    $new->save();
                }
            }
            $pesan->status = DISETUJUI;
            $pesan->penyetujui_id = Auth::id();
            $pesan->save();
        }
        return Redirect::back()->with('message','Transaksi diterima!');
    }

    public function tolakkwt(Request $r)
    {
        $pesan = Transaksi::find($r->input('kwt'));
        $pesan->status = DITOLAK;
        $pesan->penyetujui_id = Auth::id();
        $pesan->save();
        return Redirect::back()->withErrors(['message1'=>'Transaksi ditolak!']);
    }

    public function batalkwt(Request $r)
    {
        $pesan = Transaksi::find($r->input('kwt'));
        $recent = $pesan->status;
        $jurnal = Jurnal::where('transaksis_id','=',$pesan->id);
        $jurnal->forceDelete();

        if($pesan->jenis_transaksi==PENERIMAAN){
            $pesan->status = PENDING;
            if(!is_null($pesan->pemesanans_id)){
                $pesanan = pemesanan::find($pesan->pemesanans_id);
                if($recent == DISETUJUI) $pesanan->status = $pesanan->status - 1;
                $pesanan->save();
            }
            else if(!is_null($pesan->investors_id)){
                $investor = Investor::find($pesan->investors_id);
                $investor->penyetujui_id = Auth::id();
                $investor->save();
            }
            $pesan->penyetujui_id = Auth::id();
            $pesan->save();
        }
        else if($pesan->jenis_transaksi==PENGELUARAN){
            $pesan->status = PENDING;
            if ($pesan->kode_akuns->kode_akun == 'B5.2'){
                $fee_marketing = pemesanan::find($pesan->pemesanans_id);
                if ($fee_marketing->fee_marketing == null) {
                    return Redirect::back()->withErrors(['message1'=>'Harap Menentukan Fee Marketing terlebih dahulu!']);
                }
                if ($fee_marketing->fee_marketing < ($pesan->jumlah+$fee_marketing->fee_diambil)) {
                    return Redirect::back()->withErrors(['message1'=>'Fee Marketing melebihi jumlah yang ditentukan!']);
                }
                else {
                    $fee_marketing->fee_diambil = $fee_marketing->fee_diambil - $pesan->jumlah;
                    $fee_marketing->save();
                }
            }
            $pesan->penyetujui_id = Auth::id();
            $pesan->save();
        }
        return Redirect::back()->withErrors(['message1'=>'Transaksi dibatalkan!']);
    }
}
