<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\proyek;
use App\kavling;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use File;

class SiteplanController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        // return "wkwk";
        $proyek = proyek::find(session('id_proyek'));
        $data = kavling::where('proyeks_id', '=', session('id_proyek'))->get();
        return view('site_plan.site', ['tampil'=>$data]);
    }

    public function tambah_img (Request $r)
    {
        $proyek = proyek::find(session('id_proyek'));
        if ($r->file('gambar')) {
            $file = $r->file('gambar');
            $extension = $file->getClientOriginalExtension();
            $filename =  'SITEPLAN_'.session('id_proyek').'_'.str_replace(' ', '_', $proyek->name).'.'.$extension;
            $file->move('siteplan', $filename);
            $proyek->foto = $filename;
            // $proyek->foto = $r->file('gambar')->store('foto');
            Session::put('foto',$proyek->foto);
            $proyek->save();    
        }
        return redirect('site-plan');
    }
}
