<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Investor;
use App\User;
use App\Transaksi;
use Spatie\Permission\Models\Role;

class InvestorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        if (auth()->user()->getRoleNames() == '["Investor"]') {
            $dataInvestors = Investor::where('proyeks_id','=',session('id_proyek'))
                                ->where('users_id', auth()->user()->id)
                                ->orderBy('tgl_perjanjian','DESC')
                                ->get();
        }
        else {
            $dataInvestors = Investor::where('proyeks_id','=',session('id_proyek'))
                            ->orderBy('created_at','DESC')
                            ->get();
        }
        return view('investor/index', compact('dataInvestors'));
    }

    public function detail($id)
    {
        $dataInvestors = Investor::find($id);
        if (auth()->user()->id != $dataInvestors->users->id && auth()->user()->getRoleNames() == '["Investor"]') {
            return redirect('/investor')->with('error', 'Unauthorized Page');
        }
        $dataUsers = User::find($dataInvestors->users_id);
        $dataTerimaTransaksis = Transaksi::where('investors_id','=',$id)
                                    ->where('jenis_transaksi','=',PENERIMAAN)
                                    //->where('status','=',DISETUJUI)
                                    ->orderBy('created_at', 'DESC')->get();
        $dataBayarTransaksis = Transaksi::where('investors_id','=',$id)
                                    ->where('jenis_transaksi','=',PENGELUARAN)
                                    //->where('status','=',DISETUJUI)
                                    ->orderBy('created_at', 'DESC')->get();
        return view('investor/detail', compact('dataInvestors', 'dataUsers', 'dataTerimaTransaksis', 'dataBayarTransaksis'));
    }

    public function form_setor_modal()
    {
        $dataInvestors = User::role('Investor')->get();
        return view('investor/form_setor_modal', compact('dataInvestors'));
    }

    /*public function form_bayar_investor()
    {
        $dataInvestors = User::where('role','=',INVESTOR_ROLE)->get();
        return view('investor/form_bayar_investor', compact('dataInvestors'));
    }*/

    public function setor_modal(Request $request)
    {
        $investor = new Investor;
        $investor->proyeks_id = session('id_proyek');
        $investor->users_id = $request->input('nama-investor');
        $namaInvestor = User::where('id', $request->input('nama-investor'))->pluck('name');
        $investor->tgl_perjanjian = $request->input('tanggal-perjanjian');
        $investor->jumlah_setor_modal = $request->input('jumlah-perjanjian');
        if($request->file('bukti-perjanjian')) {
            $investor->bukti_perjanjian = $request->file('bukti-perjanjian')->store('perjanjian');
        } else {
            $investor->bukti_perjanjian = null;
        }
        $investor->penginput_id = auth()->user()->id;
        $investor->penyetujui_id = null;
        //$investor->status = PENDING;
        $investor->created_at = now();
        $investor->updated_at = now();
        $investor->save();

        return redirect('/investor')->with('message','Penyetoran Modal oleh Investor '.$namaInvestor[0].' berhasil ditambah');
    }

    public function bukti_perjanjian(Request $request)
    {
        $bukti = Investor::find($request->input('id-investor'));
        if ($request->file('bukti_perjanjian')) {
            $bukti->bukti_perjanjian = $request->file('bukti_perjanjian')->store('perjanjian');
        }
        $bukti->save();
        return back()->with('message', 'Bukti perjanjian berhasil ditambahkan');
    }

    public function lihat_perjanjian($perjanjian)
    {
        return response()->file('storage/perjanjian/'.$perjanjian);
    }

    /*public function bayar_investor(Request $request)
    {
        $bayar = new Transaksi;
        $bayar->proyeks_id = session('id_proyek');
        $bayar->kode_akuns_id = $request->input('kode-akun');
        $bayar->investors_id = $request->input('nama-investor');
        $bayar->pemesanans_id = null;
        $bayar->pembatalans_id = null;
        $bayar->jenis_transaksi = PENGELUARAN;
        $bayar->tgl_transaksi = $request->input('tanggal-bayar');
        $bayar->jumlah = $request->input('jumlah-bayar');
        $bayar->deskripsi = null;
        if($request->file('bukti-bayar')) {
            $bayar->bukti = $request->file('bukti-bayar')->store('foto');
        }
        $bayar->penginput_id = auth()->user()->id;
        $bayar->penyetujui_id = null;
        $bayar->status = PENDING;
        $bayar->created_at = now();
        $bayar->updated_at = now();
        $bayar->save();

        return redirect('/pengeluaran')->with('message','Pembayaran ke Investor berhasil ditambah');
    }*/
}
