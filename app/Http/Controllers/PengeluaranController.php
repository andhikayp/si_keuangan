<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KodeAkun;
use App\Transaksi;
use App\pemesanan;
use App\Pembatalan;
use App\Investor;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class PengeluaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        $keluar = Transaksi::where(array(['proyeks_id', '=', session('id_proyek')],['jenis_transaksi','=',PENGELUARAN],))
            ->orderByRaw('status ASC')
            ->orderByRaw('created_at DESC')
            ->get();
        return view('transaksi.pengeluaran', ['keluar'=>$keluar]);

    }
    public function form()
    {
        $daftar = pemesanan::where([['proyeks_id','=',session('id_proyek')]])->get();
        $batal = Pembatalan::where([['proyeks_id','=',session('id_proyek')],['jenis_pembatalan', BATAL_BELI],['status', DISETUJUI]])->get();
        $investor = Investor::where([['proyeks_id','=',session('id_proyek')]])->get();
        $tampil = KodeAkun::where('tipe','=',PENGELUARAN)->whereNull('parent')->orderBy('id','ASC')->get();
        // var_dump($keluar); return;
        
        return view('transaksi.form_pengeluaran', compact('daftar','batal','investor','tampil'));
       
    }
    /*public function detail()
    {
        return view('transaksi.detail_pengeluaran');
    }*/

    public function form_post(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'tanggal' => 'required',
            // 'investor' => 'required',
            // 'kav' => 'required|numeric',
            'kode-akun' => 'required',
            'nominal' => 'required|numeric',
            // 'bukti' => 'image',
            //'nama-pengeluaran' => 'required',
        ]);
        // var_dump($r);return;

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }

        /*if ($r->kode == 28) {
            // $fee_marketing = pemesanan::find($r->input('no-fee'));
            // if ($fee_marketing->fee_marketing == null) {
            //     return Redirect::back()->withErrors(['message1'=>'Harap Menentukan Fee Marketing terlebih dahulu!']); 
            // }
            // if ($fee_marketing->fee_marketing < ($r->nominal+$fee_marketing->fee_diambil)) {
            //     return Redirect::back()->withErrors(['message1'=>'Fee Marketing melebihi jumlah yang ditentukan!']); 
            // }
            // else {
            //     $fee_marketing->fee_diambil = $fee_marketing->fee_diambil + $r->nominal;
            //     $fee_marketing->save();
            // }            
        }*/
        $post = new Transaksi;
        $post->kode_akuns_id = $r->input('kode-akun'); 
        if($r->input('tipe-pengeluaran') == 1){
            $pembatalanId = $r->input('no-pembatalan');
            $pembatalanData = Pembatalan::find($pembatalanId);
            $sudahKembali = Transaksi::where('pembatalans_id', $pembatalanId)->where('status', DISETUJUI)->sum('jumlah');
            if ($r->input('nominal')+$sudahKembali > $pembatalanData->jumlah_dikembalikan) {
                return Redirect::back()->withErrors(['message1'=>'Jumlah nominal melebihi perjanjian! mohon cek kembali!']);
            }
            $post->pembatalans_id = $pembatalanId;
            $namaKavling = Pembatalan::find($pembatalanId)->pemesanans->kavlings->name;
            $post->deskripsi = 'Kavling: '.$namaKavling;
        }
        if ($r->input('tipe-pengeluaran') == 2){
            $investorId = $r->input('no-investor');
            $post->investors_id = $investorId;
            $namaInvestor = Investor::find($investorId)->users->name;
            $post->deskripsi = 'Investor: '.$namaInvestor;
        }
        if ($r->input('tipe-pengeluaran') == 3){
            // echo "string"; return;
            $fee_marketing = kodeakun::where('kode_akun', '=', "B5.2")->first();
            $post->kode_akuns_id = $fee_marketing->id;

            $pemesananId = $r->input('no-fee');
            $post->pemesanans_id = $pemesananId;
            $dataPesan = pemesanan::find($pemesananId);
            $namaMarketing = $dataPesan->nama_marketing;
            $post->deskripsi = 'Marketing: '.$namaMarketing;
            if ($dataPesan->fee_marketing == null) {
                return Redirect::back()->withErrors(['message1'=>'Harap Menentukan Fee Marketing terlebih dahulu!']); 
            }
            // if ($fee_marketing->fee_marketing < ($r->nominal+$fee_marketing->fee_diambil)) {
            //     return Redirect::back()->withErrors(['message1'=>'Fee Marketing melebihi jumlah yang ditentukan!']); 
            // }
            // else {
            //     $fee_marketing->fee_diambil = $fee_marketing->fee_diambil + $r->nominal;
            //     $fee_marketing->save();
            // }  
        }
        if ($r->input('tipe-pengeluaran') == 4){
            $post->deskripsi = $r->input('nama-pengeluaran');
        }
        $post->proyeks_id = session('id_proyek');
        $post->jenis_transaksi = 1;
        $post->tgl_transaksi = $r->input('tanggal');
        $post->jumlah = $r->input('nominal');
        if ($r->file('bukti')) {
            $post->bukti = $r->file('bukti')->store('foto');   
        }
        $post->penginput_id = Auth::id();
        $post->penyetujui_id = null;
        $post->status = PENDING;
        $post->save();
        return redirect('/pengeluaran')->with('message','Transaksi berhasil ditambahkan!');
    }
}
