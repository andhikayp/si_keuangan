<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\konfigurasi;

class KonfigurasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function email()
    {
        $konfigurasi = konfigurasi::all()->first();
        // var_dump($konfigurasi);return;
        return view('konfigurasi.email',compact('konfigurasi'));
    }
    public function sms()
    {
        return view('konfigurasi.sms');
    }
    public function surat()
    {
        return view('konfigurasi.surat');
    }

    public function konfigtenggat(Request $r)
    {
        $konfigurasi = konfigurasi::all()->first();
        if($konfigurasi == null){
            $konfigurasi = new konfigurasi;
        }
        $konfigurasi->email = $r->input('email');
        $konfigurasi->subjek_tenggat = $r->input('subjek');
        $konfigurasi->isi_tenggat = $r->input('isi');
        $konfigurasi->save();
        return view('konfigurasi.email',compact('konfigurasi'));
    }
    public function konfigperingatan(Request $r)
    {
        $konfigurasi = konfigurasi::all()->first();
        if($konfigurasi == null){
            $konfigurasi = new konfigurasi;
        }
        $konfigurasi->email = $r->input('email');
        $konfigurasi->subjek_peringatan = $r->input('subjek');
        $konfigurasi->isi_peringatan = $r->input('isi');
        $konfigurasi->save();
        return view('konfigurasi.email',compact('konfigurasi'));
    }
}
