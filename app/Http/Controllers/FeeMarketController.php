<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kavling;
use App\pemesanan;
use App\Transaksi;
use App\KodeAkun;
use DB;
use Illuminate\Support\Facades\Redirect;
use Carbon;

class FeeMarketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        $fee_marketing = DB::table('pemesanans')
            ->join('kavlings', 'pemesanans.kavlings_id', '=', 'kavlings.id')
            // ->join('users', 'pemesanans.nama_marketing', '=', 'users.id')
            ->select('kavlings.name', 'pemesanans.id', 'pemesanans.nama_marketing')
            ->where('pemesanans.proyeks_id', '=', session('id_proyek'))
            ->where('status_fee_marketing', '=', 0)
            ->get();
        $pemesanan = pemesanan::where('proyeks_id','=',session('id_proyek'))
        	->where('status_fee_marketing', '=', 1)
        	->get();
        return view('fee_marketing.index', compact('fee_marketing','pemesanan'));
    }
    public function history($id)
    {
        $marketing = pemesanan::find($id);
        $kode_akun = KodeAkun::where('kode_akun','=','B5.2')
            ->pluck('id')->first();
        $transaksi = Transaksi::where('pemesanans_id', '=', $id)
            ->where('kode_akuns_id', '=', $kode_akun)
            ->where('status', '=', 1)
            ->get();
        return view('fee_marketing.detailfee', compact('marketing','transaksi'));
    }

    public function tambah_fee(Request $r)
    {
        $fee_marketing = pemesanan::find($r->nomor_kavling);
        $fee_marketing->fee_marketing = $r->input('total_fee');
        $fee_marketing->status_fee_marketing = 1;
        $fee_marketing->fee_diambil = 0;
        $fee_marketing->save();
        return Redirect::back()->with('message','Jumlah Fee Marketing untuk user  berhasil diinput');  
    }

    public function bayar_fee(Request $r)
    {
        $mytime = Carbon\Carbon::now();
        $fee_marketing = pemesanan::find($r->id);
        $namaMarketing = $fee_marketing->nama_marketing;
        if ($fee_marketing->fee_marketing == null) {
            return Redirect::back()->withErrors(['message1'=>'Harap Menentukan Fee Marketing terlebih dahulu!']); 
        }
        // if ($fee_marketing->fee_marketing < ($r->fee + $fee_marketing->fee_diambil)) {
        //     return Redirect::back()->withErrors(['message1'=>'Fee Marketing melebihi jumlah yang ditentukan!']); 
        // }
        // else {
        //     $fee_marketing->fee_diambil = $fee_marketing->fee_diambil + $r->fee;
        //     $fee_marketing->save();
        // }   
        $kodeakun = kodeakun::where('kode_akun', '=', "B5.2")->first();
        $post = new Transaksi;
        $post->deskripsi = 'Marketing: '.$namaMarketing;
        $post->kode_akuns_id = $kodeakun->id; 
        $post->proyeks_id = session('id_proyek');
        $post->pemesanans_id = $r->input('id');            
        $post->jenis_transaksi = 1;
        $post->jumlah = $r->input('fee');
        $post->tgl_transaksi =  $mytime; 
        if ($r->file('bukti')) {
            $post->bukti = $r->file('bukti')->store('foto');   
        }
        $post->penginput_id = 1;
        $post->penyetujui_id = null;
        $post->status = PENDING;
        $post->save();
        return Redirect::back()->with('message','Input pengeluaran Fee Marketing berhasil!'); 
    }
}
