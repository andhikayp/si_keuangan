<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\proyek;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\KodeAkun;
use App\Transaksi;
use App\pemesanan;
use App\Pembatalan;
use Carbon;

class KeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function penerimaan()
    {
        $penerimaan = transaksi::select('pemesanans_id')
            ->where('proyeks_id', '=', session('id_proyek'))
            ->where('jenis_transaksi', '=', 0)
            ->where('status', '=', 1)
            ->where('pemesanans_id', '!=', null)
            ->distinct()
            ->get();
        return view('keuangan/penerimaan/index', compact('penerimaan'));
    }

    public function detail_penerimaan($id){
        $transaksi_all = transaksi::where('kode_akuns_id','!=', null)
            ->where('jenis_transaksi','=', 0)
            ->where('pemesanans_id','=', $id)
            ->where('status', '=', 1)
            ->get();
        return view('keuangan/penerimaan/detail', compact('id', 'transaksi_all'));
    }
    public function pengeluaran(){
        $pengeluaran = transaksi::select('kode_akuns_id')
            ->where('proyeks_id', '=', session('id_proyek'))
            ->where('jenis_transaksi', '=', 1)
            ->where('status', '=', 1)
            ->distinct()
            ->orderByRaw('kode_akuns_id')->get();
        return view('keuangan/pengeluaran/index', compact('pengeluaran'));
    }
    public function detail_pengeluaran($id){
        $pengeluaran = transaksi::where('kode_akuns_id','=',$id)
            ->where('proyeks_id', '=', session('id_proyek'))
            ->where('status', '=', 1)
            ->orderByRaw('tgl_transaksi')->get();
        return view('keuangan/pengeluaran/detail', compact('pengeluaran', 'id'));
    }
    public function laba_rugi(){
        return view('keuangan/laba_rugi/index');
    }
    public function neraca(){
        return view('keuangan/neraca/index');
    }
    public function arus_kas(){
        return view('keuangan/arus_kas/index');
    }
}
