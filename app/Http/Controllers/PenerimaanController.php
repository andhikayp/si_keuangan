<?php

namespace App\Http\Controllers;

use App\KodeAkun;
use App\Transaksi;
use App\pemesanan;
use App\Investor;
use App\kavling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class PenerimaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        $daftar = Transaksi::where(array(
            ['jenis_transaksi','=',PENERIMAAN],
            ['proyeks_id','=',session('id_proyek')]))
            ->orderByRaw('status ASC')
            ->orderByRaw('created_at DESC')
            ->get();
        return view('transaksi.penerimaan',compact('daftar'));
    }
    public function form()
    {
        $daftar = pemesanan::where([['proyeks_id','=',session('id_proyek')],['status','!=',Lunas]])->get();
        $akun = KodeAkun::where('tipe','=',PENERIMAAN)->whereNull('parent')->orderBy('id','ASC')->get();
        $investor = Investor::where('proyeks_id','=',session('id_proyek'))->get();
        return view('transaksi.form_penerimaan',compact('daftar','akun','investor'));
    }
    /*public function detail()
    {
        return view('transaksi.detail_penerimaan');
    }*/

    public function form_post(Request $r)
    {
        // return $r;
        // return $r->input('nominal') == 'null' ? 'sama' : 'tidak';
        $validator = Validator::make($r->all(), [
            'tanggal' => 'required',
            // 'investor' => 'required',
            // 'kav' => 'required|numeric',
            'kode-akun' => 'required',
            // 'nominal' => 'required|numeric',
            'bukti' => 'image',
            // 'keterangan' => 'alpha_num',
        ]);

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }

        $post = new Transaksi;
        if($r->input('tipe-penerimaan')==1){
            $pesan = pemesanan::find($r->input('no_pemesanan'));
            $kode = KodeAkun::find($r->input('kode-akun'));
            // return $kode;
            if($kode->kode_akun == "A1"){
                if($pesan->status >= UTJ) return Redirect::back()->withErrors(['message1'=>'UTJ telah dibayarkan, mohon tunggu konfirmasi Komisaris!']);
                if($r->input('nominal') != $pesan->utj && $r->input('nominal') != 0) return Redirect::back()->withErrors(['message1'=>'Jumlah nominal tidak sesuai, mohon cek kembali!']);
            }
            if($kode->kode_akun == "A2"){
                $dpke = Transaksi::where(array(['pemesanans_id','=',$r->input('no_pemesanan')],['status','!=',DITOLAK],['kode_akuns_id','=',$kode->id]))->count();
                if($dpke == 0){
                    if($pesan->status < UTJ) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar DP1 karena belum ada konfirmasi UTJ']);
                    if($pesan->status >= DP1) return Redirect::back()->withErrors(['message1'=>'DP1 telah dibayarkan, mohon tunggu konfirmasi Komisaris!']);
                    if($r->input('nominal') != $pesan->dp1) return Redirect::back()->withErrors(['message1'=>'Jumlah nominal tidak sesuai, mohon cek kembali!']);
                }
                if($dpke == 1){
                    if($pesan->status >= DP2) return Redirect::back()->withErrors(['message1'=>'DP2 telah dibayarkan, mohon tunggu konfirmasi Komisaris!']);
                    if($pesan->status < DP1) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar DP2 karena belum ada konfirmasi DP1']);
                    if($pesan->status_ppjb != DISETUJUI) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar DP2 karena PPJB belum disetujui']);
                    if($r->input('nominal') != $pesan->dp2) return Redirect::back()->withErrors(['message1'=>'Jumlah nominal tidak sesuai, mohon cek kembali!']);
                }
                if($dpke == 2){
                    if($pesan->status >= DP3) return Redirect::back()->withErrors(['message1'=>'DP3 telah dibayarkan, mohon tunggu konfirmasi Komisaris!']);
                    if($pesan->status < DP2) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar DP3 karena belum ada konfirmasi DP2']);
                    if($pesan->status_ppjb != DISETUJUI) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar DP2 karena PPJB belum disetujui']);
                    if($r->input('nominal') != $pesan->dp3) return Redirect::back()->withErrors(['message1'=>'Jumlah nominal tidak sesuai, mohon cek kembali!']);
                }
            }
            if($kode->kode_akun == "A3"){
                $angsur = Transaksi::where(array(
                    ['pemesanans_id','=',$pesan->id],
                    ['status','=',DISETUJUI],
                    ['kode_akuns_id','=',KodeAkun::where('kode_akun','=','A3')->pluck('id')->first()])
                    )->orderBy('tgl_transaksi','ASC')
                    ->get();
                if($angsur->count()>=$pesan->angsur_jml || $pesan->status == Lunas) return Redirect::back()->withErrors(['message1'=>'Angsuran telah dilunasi']);
                if($pesan->status < DP3) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar Angsuran karena belum ada konfirmasi DP3']);
                if($pesan->status_ppjb != DISETUJUI) return Redirect::back()->withErrors(['message1'=>'Belum bisa membayar Angsuran karena PPJB belum disetujui']);
                if($r->input('nominal') != $pesan->angsur) return Redirect::back()->withErrors(['message1'=>'Jumlah nominal tidak sesuai, mohon cek kembali!']);
            }
            $post->pemesanans_id =$r->input('no_pemesanan');
        }
        else if($r->input('tipe-penerimaan')==2){
            $investor = Investor::find($r->input('investor'));
            $transaksi = Transaksi::where('investors_id','=',$r->input('investor'))
                        ->where('jenis_transaksi', PENERIMAAN)
                        ->where('status','=',DISETUJUI)
                        ->sum('jumlah');
            if($r->input('nominal')+$transaksi > $investor->jumlah_setor_modal) return Redirect::back()->withErrors(['message1'=>'Jumlah nominal melebihi perjanjian! mohon cek kembali!']);
            $post->investors_id = $r->input('investor');
        }
        $post->kode_akuns_id = $r->input('kode-akun');
        $post->pembatalans_id = NULL;
        $post->proyeks_id = session('id_proyek');
        $post->jenis_transaksi = 0;
        $post->tgl_transaksi = $r->input('tanggal');
        $post->jumlah = $r->input('nominal');
        $post->deskripsi = $r->input('keterangan');
        if ($r->file('bukti')) {
            $post->bukti = $r->file('bukti')->store('foto');
        }
        $post->penginput_id = Auth::id();
        $post->penyetujui_id = null;
        $post->status = PENDING;
        $post->save();
        return Redirect::back()->with('message','Transaksi berhasil ditambahkan!');
    }
}
