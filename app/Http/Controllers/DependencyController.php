<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DependencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDataPemesan($kavling)
    {
        $pemesan = DB::table('pemesanans')
                        ->select('buyer', 'alamat', 'email', 'no_ktp', 'hp_pribadi', 'hp_kerabat', 'utj', 'dp1', 'dp2', 'dp3', 'dp4', 'angsur')
                        ->where('kavlings_id', $kavling)
                        ->where('status_pembatalan', TIDAK_BATAL)
                        ->get();
        return json_encode($pemesan[0]);
    }

    public function getDataInvestor($id)
    {
        $investor = DB::table('users')
                        ->select('name', 'alamat', 'email', 'telp')
                        ->where('id', $id)
                        ->get();
        return json_encode($investor[0]);
    }

    public function getDataTagihan($id)
    {
        $kodeAkun = DB::table('kode_akuns')->where('id', $id)->get();
        return json_encode($kodeAkun[0]);
    }
}
