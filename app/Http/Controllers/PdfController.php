<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\pemesanan;
use App\Transaksi;
use App\KodeAkun;
use App\kavling;
use App\proyek;
use App\user;
use Carbon;
use Illuminate\Support\Facades\Auth;

class PdfController extends Controller
{
    public function kwitansi($id)
    {
        $transaksi = transaksi::find($id);
        $kode_akun = kodeakun::find($transaksi->kode_akuns_id);
        $pemesanan = pemesanan::find($transaksi->pemesanans_id);
        $kavling = kavling::find($pemesanan->kavlings_id);
        $proyek =  proyek::find($pemesanan->proyeks_id);
        $terbilang = $this->terbilang($transaksi->jumlah);
		$hasil_rupiah = "Rp " . number_format($transaksi->jumlah,2,',','.');
    	$html = '
    			<style>
					td {
						height: 25px;
					}
				</style>
				<table width="100%" style="font-size: 9pt; color: #000000;page-break-inside:avoid; margin-top: 10px">
					<tr>
						<td width="50%;" style="text-decoration: underline; font-weight: bold; font-size: 14pt;">ADAM JAYA PROPERTY</td>
						<td></td>
						<td>No: '.\Carbon\Carbon::today()->format('y').'/'.$proyek->id.'/'.$kavling->id.'/'.$pemesanan->id.'/'.$kode_akun->id.'/'.$transaksi->id.'</td>
					</tr>
	                <tr>
	                	<td width="13%;" height="10px"></td>
	                    <td width="70%" style="text-align:center; text-decoration: underline; font-size: 16pt; font-weight: bold;" height="10px">KWITANSI</td>
	                </tr>
	                <tr>
	                	<td width="13%;"></td>
	                	<td style="text-align:center;">TANDA TERIMA</td>
	                </tr>
	            </table><br>
	            <!-- <hr style="color:black;"/> -->
	            <table style="font-size: 12pt" >
	            	<tr>
	            		<td width="50%" height: 30px;>TELAH TERIMA DARI</td>
	            		<td></td>
	            	</tr>
	            	<tr>
	            		<td width="20%">Bapak/Ibu/Sdr</td>
	            		<td>: '.$pemesanan->buyer.'</td>
	            	</tr>
	            	<tr>
	            		<td>Alamat</td>
	            		<td>: '.$pemesanan->alamat.'</td>
	            	</tr>
	            	<tr>
	            		<td width="20%">Untuk membayar</td>
	            		<td>: '.$kode_akun->nama_akun.'</td>
	            	</tr>
	            	<tr>
	            		<td>Ukuran tanah</td>
	            		<td>: '.$kavling->luas.' m<sup>2</sup></td>
	            	</tr>
	            	<tr>
	            		<td>Lokasi</td>
	            		<td>: '.$proyek->alamat.'</td>
	            	</tr>
	            	<tr>
	            		<td>Jumlah uang</td>
	            		<td>: '.$hasil_rupiah.'</td>
	            	</tr>
	            	<tr>
	            		<td>Terbilang</td>
	            		<td>: '.$terbilang.' rupiah</td>
	            	</tr>
	            </table>
	            <br><br>
	            <table>
	            	<tr>
	            		<td width="50%" style="text-align:center;">
	            			<br>Mengetahui / Menyetujui <br>Pembeli <br><br><br><br>('.$pemesanan->buyer.')
	            		</td>
	            		<td width="50%" style="text-align:center;">
	            			Surabaya, '.\Carbon\Carbon::today()->locale('id')->settings(['formatFunction' => 'isoFormat'])->format('LL').'<br>ADAM JAYA PROPERTY
	            			<br><br><br><br>('.Auth::user()->name.')
	            		</td>
	            	</tr>
	            </table>
	        ';
        PDF::SetTitle('Kwitansi'.$kode_akun->nama_akun);
        PDF::SetAutoPageBreak(TRUE, 0);
        PDF::AddPage('L', 'A5');
        PDF::writeHTML($html, true, false, true, false, '');
        $path = storage_path() .'/app/kwitansi/kwitansi_'.$pemesanan->id.'.pdf';
        PDF::Output($path,'F');
        return response()->file($path);
    }

    public function ppjb(Request $r)
    {
    	$pihak_pertama = user::find($r->Nomor_kavling);
    	$id = $r->id;
        $pemesanan = pemesanan::find($id);
        $kavling = kavling::find($pemesanan->kavlings_id);
        $proyek =  proyek::find($pemesanan->proyeks_id);
        $terbilang = $this->terbilang($kavling->harga);
		$hasil_rupiah = "Rp " . number_format($kavling->harga,2,',','.');

		$dp_total = $pemesanan->dp1 + $pemesanan->dp2 + $pemesanan->dp3 + $pemesanan->dp4;
		$hasil_rupiah_dp_total = "Rp " . number_format($dp_total,2,',','.');
    	$html = '
    			<style>
					td {
						height: 25px;
						vertical-align: bottom;
					}
					.bold{
						font-weight: bold;
					}
					.center{
						text-align: center;
					}
					div {
						line-height: 1.6;
					}
				</style>
				<div class="bold center">
					PERJANJIAN  JUAL BELI TANAH KAVLING <br>Nomor: '.$proyek->id.'/PPJB/Kav/'.$kavling->name.'/'.\Carbon\Carbon::today()->format('Y').'
				</div>
				<div>
					<div>
						Pada hari '.\Carbon\Carbon::today()->locale('id')->settings(['formatFunction' => 'translatedFormat'])->format('l').', tanggal '.\Carbon\Carbon::today()->locale('id')->settings(['formatFunction' => 'isoFormat'])->format('LL').' kami yang bertanda tangan dibawah ini,
					</div>
					<table style="padding-top: 5px;">
						<tr>
							<td width="5%"></td>
							<td width="15%">Nama</td>
							<td width="78%">: '.$pihak_pertama->name.'</td>
						</tr>
						<tr>
							<td></td>
							<td>Alamat</td>
							<td>: '.$pihak_pertama->alamat.'</td>
						</tr>
						<tr>
							<td></td>
							<td>Nomor KTP</td>
							<td>: '.$pihak_pertama->no_ktp.'</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">Selanjutnya dalam perjanjian ini disebut <b><i>Pihak Pertama</i></b>.</td>
						</tr>
						<tr>
							<td></td>
							<td>Nama</td>
							<td>: '.$pemesanan->buyer.'</td>
						</tr>
						<tr>
							<td></td>
							<td>Alamat</td>
							<td>: '.$pemesanan->alamat.'</td>
						</tr>
						<tr>
							<td></td>
							<td>Nomor KTP</td>
							<td>: '.$pemesanan->no_ktp.'</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">Selanjutnya dalam perjanjian ini disebut <b><i>Pihak Kedua</i></b>.</td>
						</tr>
					</table>
					<div >
						Kedua belah pihak sepakat mengikat perjanjian jual beli tanah kavling dengan ketentuan sebagai berikut. <br>
						<div class="center bold">
							Pasal 1 <br>Obyek Jual Beli <br>
						</div>
						Obyek jual beli tanah kavling berada di '.$proyek->alamat.' dengan:
						<div style="text-indent: 27px;">
							1. Nomor Kavling	: '.$kavling->name.'<br>
							2. Luas Tanah	: '.$kavling->luas.' m²
						</div>

						<p class="center bold">Pasal 2 <br>Harga dan Sistem Pembayaran</p>
						<ol>
							<li>
								Harga jual disepakati sebesar '.$hasil_rupiah.',- ('.$terbilang.' rupiah)
							</li>
							<li>
								Sistem pembayaran disepakati dengan sistem kredit sebagai berikut :
								<ul type="disc">
									<li>
										DP sebesar '.$hasil_rupiah_dp_total.',- , Dibayar pada saat perjanjian ini ditanda tangani Rp. '.number_format($pemesanan->dp1,2,',','.').', dan sisa DP Rp. '.number_format($pemesanan->dp2+$pemesanan->dp3+$pemesanan->dp4,2,',','.').',-  Akan dibayar pada tanggal yang telah disepakati bersama.
									</li>
									<li>
										Angsuran tiap bulan sebesar Rp. '.number_format($pemesanan->angsur,2,',','.').',- selama '.$pemesanan->angsur_jml.' bulan ('.$pemesanan->angsur_jml.' Kali) yang dibayarkan setiap tanggal '.$pemesanan->tgl_tempo.' terhitung mulai tanggal '.Carbon::parse($pemesanan->tgl_angsur1)->locale('id')->settings(['formatFunction' => 'isoFormat'])->format('LL').' s/d '.Carbon::parse($pemesanan->tgl_angsur2)->locale('id')->settings(['formatFunction' => 'isoFormat'])->format('LL').'.
									</li>
								</ul>
							</li>
							<li>
								Harga tersebut sudah termasuk biaya Splitzing SHM.
							</li>
							<li>
								Biaya biaya lain yang mungkin timbul;
								<ul type="disc">
									<li>
										Biaya PBB terhitung sejak PJB ini ditanda tangani hingga surat-surat diserahkan yang untuk sementara akan ditalangi oleh Pihak Pertama.
									</li>
									<li>
										Biaya balik nama ke pembeli serta pajak beli (BPHTB) Jika ada.
									</li>
								</ul>
							</li>
						</ol>
						<p class="center bold">
							Pasal 3 <br>Penyerahan Obyek Jual Beli
						</p>
						<ol>
							<li>
								Obyek jual beli yang berupa tanah diserahkan pada saat PJB ini ditanda-tangani sehingga pembeli  telah memiliki hak sepenuhnya.
							</li>
							<li>
								Sertifikat Hak Milik atas nama Pihak Pertama akan diserahkan kepada Pihak Kedua dan dapat dilakukan Balik Nama  apabila pembayaran telah lunas dan proses splitzing SHM telah selesai.
							</li>
						</ol>
						<p class="center bold">
							Pasal 4 <br>Sangsi Keterlambatan
						</p>
						<ol>
							<li>
								Kedua belah pihak sepakat bahwa, keterlambatan bayar selama sekurang-kurangnya 100 hari dari jatuh tempo pembayaran, maka dianggap telah gagal bayar.
							</li>
							<li>
								Apabila pihak kedua gagal bayar, maka, pihak pertama berhak membantu menjualkan dengan harga minimal sama dengan harga beli dari pihak pertama dikurangi biaya marketing.
							</li>
							<li>
								Hasil penjualan, akan diberikan kepada pihak pertama minimal sebesar dengan biaya yang sudah dibayarkan dikurangin dengan biaya marketing sebagaimana ayat dua.
							</li>
						</ol>
						<p class="center bold">
							Pasal 5 <br>Teknis Pembayaran
						</p>
						<ol>
							<li>
								Pembayaran dilakukan dengan cara  transfer ke Rekening BANK BNI SYARIAH atas nama CV. ADAM JAYA Nomor Rekening :6868000089  Cabang Syariah Diponegoro, Surabaya.
							</li>
							<li>
								Bukti transfer difoto dan dikirim ke WA/SMS dengan nomor 081231777172 selambat-lambatnya 24 jam setelah melakukan transfer untuk mendapatkan konfirmasi.
							</li>
							<li>
								Bukti transfer dibawa ke kantor proyek guna ditukar dengan kwitansi dan untuk mendapatkan pengesahan pada Kartu Angsuran minimal sekali dalam 3 bulan.
							</li>
							<li>
								Tanpa menunjukkan bukti transfer asli, Pihak Kesatu tidak menjamin dapat memberikan kwitansi dan memberikan pengesahan pada Kartu Angsuran.
							</li>
						</ol>
						<p class="center bold">
							Pasal 6 <br>Penyelesaian Perselisihan
						</p>
						<ol>
							<li>
								Jika terjadi perselisihan, kedua belah pihak telah sepakat  akan diselesaikan dengan kekeluargaan atas dasar keadilan.
							</li>
							<li>
								Jika perselisihan tidak bisa diselesaikan secara kekeluargaan maka kedua belah pihak bersedia menyerahkan kepada pihak yang berwenang di dalam Negara Kesatuan Republik Indonesia.
							</li>
						</ol>
						<p class="center bold">
							Pasal 7 <br>Penutup
						</p>
						<ol>
							<li>
								Perjanjian ini dibuat rangkap 2 (dua) masing-masing asli dan bermaterai cukup dan dilakukan waarmarking  oleh Notaris untuk diserahkan kepada masing-masing pihak.
							</li>
							<li>
								Perjanjian ini dilampiri gambar siteplan yang menunjukkan posisi kavling. <br><br><br>
							</li>
						</ol>
						<table>
			            	<tr class="bold">
			            		<td width="50%" style="text-align:center;">
			            			<br><br>PIHAK PERTAMA <br><br><br><br>[ '.$pihak_pertama->name.' ]
			            		</td>
			            		<td width="50%" style="text-align:center;">
			            			Surabaya, '.\Carbon\Carbon::today()->locale('id')->settings(['formatFunction' => 'isoFormat'])->format('LL').'<br>PIHAK KEDUA <br><br><br><br>[ '.$pemesanan->buyer.' ]
			            		</td>
			            	</tr>
			            </table>
					</div>
				</div>
	        ';
        PDF::SetTitle($proyek->id.'/PPJB/Kav/'.$kavling->name);
        PDF::SetMargins(17, 20, 17, true);
        PDF::AddPage('P', 'A4');
        PDF::writeHTML($html, true, false, true, false, '');
        // PDF::Output($proyek->id.'/PPJB/Kav/'.$kavling->name.'.pdf');
        $path = storage_path() .'/app/ppjb/ppjb_'.$pemesanan->id.'.pdf';
        PDF::Output($path,'F');
        return response()->file($path);
    }

    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}
		return $temp;
	}

	function terbilang($nilai){
		if($nilai<0) {
			$hasil = "Minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}
		return $hasil;
	}
}
