<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KodeAkun;
use App\Transaksi;
use App\pemesanan;
use App\Pembatalan;

class ProgressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index(){
        return view('progress_penjualan.index');
    }
}
