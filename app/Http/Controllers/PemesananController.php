<?php

namespace App\Http\Controllers;

use App\kavling;
use App\pemesanan;
use App\Transaksi;
use App\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\KodeAkun;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class PemesananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        $daftar = pemesanan::where(array(
            ['proyeks_id','=',session('id_proyek')],
            ['status_pembatalan','=',TIDAK_BATAL]
        ))->get();
        // var_dump($daftar[0]->kavling);return;
        return view('pemesanan.pesan',compact('daftar'));
    }

    public function form()
    {
        $kavling = kavling::where(array(
            ['proyeks_id','=',session('id_proyek')],
            ['status','=',KAVLING_AVAILABLE]
        ))->get();
        //$marketing = user::where('role','=','Marketing')->get();
        return view('pemesanan.formpesan',compact('kavling'));
    }

    public function tambah_pesan(Request $r)
    {
        $pesan = new pemesanan;
        // dd($r->nama_marketing); return;
        if ($r->nama_marketing != null and $r->nama_marketing != "-" and $r->nama_marketing != " ") {
            $pesan->nama_marketing = $r->input('nama_marketing');
            $pesan->status_fee_marketing = 0;
        }
        $validator = Validator::make($r->all(), [
            'Nomor_kavling' => 'required',
            'Nama_pemesan' => 'required',
            'Alamat' => 'required',
            'Email' => 'required',
            // 'Ktp' => 'required|numeric|digits_between:16,16',
            // 'Hp_Pribadi' => 'required|numeric|digits_between:10,14',
            // 'Hp_Kerabat' => 'required|numeric|digits_between:10,14',
            'Ktp' => 'required|numeric',
            'Hp_Pribadi' => 'required|numeric',
            // 'Hp_Kerabat' => 'required|numeric',
            'Harga_PPJB' => 'required|numeric',
            // 'UTJ' => 'required|numeric',
            // 'Tanggal_UTJ' => 'required',
            'DP1' => 'required|numeric',
            'Tanggal_DP1' => 'required',
            // 'DP2' => 'required|numeric',
            // 'Tanggal_DP2' => 'required',
            // 'DP3' => 'required|numeric',
            // 'Tanggal_DP3' => 'required',
            'Jumlah_Angsuran' => 'required|numeric',
            'Angsuran' => 'required|numeric',
            'Tanggal_Jatuh_Tempo' => 'required',
            'Angsuran_pertama' => 'required',
            'Angsuran_terakhir' => 'required',
            'Tanggal_Pemesanan' => 'required',
        ]);
        // var_dump($r);return;

        if($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        // return $r;
        $pesan->proyeks_id = session('id_proyek');
        $pesan->kavlings_id = $r->input('Nomor_kavling');
        $pesan->buyer = $r->input('Nama_pemesan');
        $pesan->alamat = $r->input('Alamat');
        $pesan->email = $r->input('Email');
        $pesan->no_ktp = $r->input('Ktp');
        $pesan->hp_pribadi = $r->input('Hp_Pribadi');
        $pesan->hp_kerabat = $r->input('Hp_Kerabat');
        $pesan->harga_deal = $r->input('Harga_PPJB');
        $pesan->utj = $r->input('UTJ');
        $pesan->utj_tgl = $r->input('Tanggal_UTJ');
        $pesan->dp1 = $r->input('DP1');
        $pesan->dp1_tgl = $r->input('Tanggal_DP1');
        $pesan->dp2 = $r->input('DP2');
        $pesan->dp2_tgl = $r->input('Tanggal_DP2');
        $pesan->dp3 = $r->input('DP3');
        $pesan->dp3_tgl = $r->input('Tanggal_DP3');
        $pesan->angsur_jml = $r->input('Jumlah_Angsuran');
        $pesan->angsur = $r->input('Angsuran');
        $pesan->tgl_tempo = $r->input('Tanggal_Jatuh_Tempo');
        $pesan->tgl_angsur1 = $r->input('Angsuran_pertama');
        $pesan->tgl_angsur2 = $r->input('Angsuran_terakhir');
        $pesan->tgl_pesan = $r->input('Tanggal_Pemesanan');
        $pesan->penginput_id = Auth::id();
        $pesan->penyetujui_id = null;
        $pesan->marketing_id = null;
        $pesan->status = PENDING;
        $pesan->status_ppjb = NOPPJB;
        $pesan->status_pembatalan = TIDAK_BATAL;
        $pesan->save();

        $kavling = kavling::find($r->input('Nomor_kavling'));
        $kavling->status = KAVLING_BOOKING;
        $kavling->save();
        return redirect('pemesanan')->with('message','Pemesanan berhasil ditambahkan!');
    }

    public function detail($id)
    {
        $data = pemesanan::find($id);
        $user = user::orderBy('name', 'asc')->get();
        $transaksi = Transaksi::where(array(['pemesanans_id','=',$id],['status','=',DISETUJUI]))->orderBy('kode_akuns_id','ASC')->get();
        $utj = Transaksi::where(array(
            ['pemesanans_id','=',$id],
            ['status','=',DISETUJUI],
            ['kode_akuns_id','=',KodeAkun::where('kode_akun','=','A1')->pluck('id')->first()])
        )->first();
        $dp = Transaksi::where(array(
            ['pemesanans_id','=',$id],
            ['status','=',DISETUJUI],
            ['kode_akuns_id','=',KodeAkun::where('kode_akun','=','A2')->pluck('id')->first()])
        )->orderBy('tgl_transaksi','ASC')
        ->get();
        // var_dump($dp);return;
        $angsur = Transaksi::where(array(
            ['pemesanans_id','=',$id],
            ['kode_akuns_id','=',KodeAkun::where('kode_akun','=','A3')->pluck('id')->first()])
        )->orderBy('tgl_transaksi','ASC')
        ->get();
        return view('pemesanan.detailpesan',compact('data','transaksi','utj','dp','angsur','user'));
    }

    public function ppjb(Request $r)
    {
        $pesan = pemesanan::find($r->input('pemesanan'));
        if ($r->file('ppjb')) {
            $pesan->ppjb = $r->file('ppjb')->store('ppjb');
        }
        $pesan->save();
        return back();
    }

    public function lihat_ppjb($ppjb)
    {
        //return $ppjb;
        return response()->file('storage/ppjb/'.$ppjb);
    }
}
