<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Pembatalan;
use App\pemesanan;
use App\kavling;
use App\Transaksi;

class PembatalanBeliController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }

    public function index()
    {
        $dataBatals = Pembatalan::where('proyeks_id','=',session('id_proyek'))
                            ->where('jenis_pembatalan','=',BATAL_BELI)
                            ->get();
        return view('pembatalan/index_beli', compact('dataBatals'));
    }

    public function form_batal()
    {
        /*$dataKavlings = kavling::where(array(
            ['proyeks_id','=',session('id_proyek')],
            ['status','=',KAVLING_BOOKING]
        ))->get();*/
        $dataKavlings = pemesanan::select('kavlings_id')
                            ->where('proyeks_id','=',session('id_proyek'))
                            ->where('status_pembatalan','=',TIDAK_BATAL)
                            ->where('status','>=', DP1)
                            ->get();
        //return $dataKavlings;
        return view('pembatalan/form_pembatalan_beli', compact('dataKavlings'));
    }

    /*public function form_pengembalian()
    {
        //$dataKavlings = Pembatalan::select('')
        return view('pembatalan/form_pengembalian_uang');
    }*/

    public function detail($id)
    {
        $dataBatals = Pembatalan::find($id);
        $dataPesans = pemesanan::find($dataBatals->pemesanans_id);
        $dataTransaksis = Transaksi::where('pembatalans_id','=',$id)->orderBy('created_at', 'DESC')->get();
        return view('pembatalan/detail_pembatalan_beli', compact('dataBatals', 'dataPesans', 'dataTransaksis'));
    }

    public function tambah_batal(Request $request)
    {
        $batal = new Pembatalan;
        $batal->proyeks_id = session('id_proyek');
        $kavling = $request->input('nomor-kavling');
        $namaKavling = kavling::where('id', $kavling)->pluck('name');
        $pemesanansId = DB::table('pemesanans')
                            ->where('kavlings_id','=',$kavling)
                            ->where('status_pembatalan','=',TIDAK_BATAL)
                            ->pluck('id');
        $batal->pemesanans_id = $pemesanansId[0];
        $batal->jenis_pembatalan = BATAL_BELI;
        $batal->jumlah_dikembalikan = $request->input('jumlah-uang-kembali');
        $batal->tgl_pembatalan = $request->input('tanggal-batal-beli');
        $batal->alasan_pembatalan = $request->input('alasan-batal-beli');
        $batal->penginput_id = auth()->user()->id;
        $batal->penyetujui_id = null;
        $batal->status = PENDING;
        $batal->created_at = now();
        $batal->updated_at = now();
        $batal->save();

        pemesanan::where('id', $pemesanansId[0])->update(['status_pembatalan'=>BATAL_BELI]);
        return redirect('/pembatalan_beli')->with('message','Pembatalan Beli untuk nomor kavling '.$namaKavling[0].' berhasil ditambah');
    }
}
