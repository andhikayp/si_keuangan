<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use App\Pembatalan;
use App\pemesanan;
use App\kavling;

class PembatalanPesanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('proyek');
    }
    
    public function index()
    {
        $dataBatals = Pembatalan::where('proyeks_id','=',session('id_proyek'))
                            ->where('jenis_pembatalan','=',BATAL_PESAN)
                            ->get();
        return view('pembatalan/index_pesan', compact('dataBatals'));
    }

    public function form_batal()
    {
        /*$dataKavlings = kavling::where(array(
            ['proyeks_id','=',session('id_proyek')],
            ['status','=',KAVLING_BOOKING]
        ))->get();*/
        $dataKavlings = pemesanan::select('kavlings_id')
                            ->where('proyeks_id','=',session('id_proyek'))
                            ->where('status_pembatalan','=',TIDAK_BATAL)
                            ->where('status', '<=', UTJ)
                            ->get();
        //return $dataKavlings;
        return view('pembatalan/form_pembatalan_pesan', compact('dataKavlings'));
    }

    public function detail($id)
    {
        $dataBatals = Pembatalan::find($id);
        $dataPesans = pemesanan::find($dataBatals->pemesanans_id);
        return view('pembatalan/detail_pembatalan_pesan', compact('dataBatals', 'dataPesans'));
    }

    public function tambah_batal(Request $request)
    {
        $batal = new Pembatalan;
        $batal->proyeks_id = session('id_proyek');
        $kavling = $request->input('nomor-kavling');
        $namaKavling = kavling::where('id', $kavling)->pluck('name');
        $pemesanansId = DB::table('pemesanans')
                            ->where('kavlings_id','=',$kavling)
                            ->where('status_pembatalan','=',TIDAK_BATAL)
                            ->pluck('id');
        $batal->pemesanans_id = $pemesanansId[0];
        $batal->jenis_pembatalan = BATAL_PESAN;
        $batal->tgl_pembatalan = $request->input('tanggal-batal-pesan');
        $batal->alasan_pembatalan = $request->input('alasan-batal-pesan');
        $batal->penginput_id = auth()->user()->id;
        $batal->penyetujui_id = null;
        $batal->status = PENDING;
        $batal->created_at = now();
        $batal->updated_at = now();
        $batal->save();

        pemesanan::where('id', $pemesanansId[0])->update(['status_pembatalan'=>BATAL_PESAN]);
        return redirect('/pembatalan_pesan')->with('message','Pembatalan Pesan untuk nomor kavling '.$namaKavling[0].' berhasil ditambah');
    }
}
