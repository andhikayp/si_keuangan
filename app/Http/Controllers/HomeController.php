<?php

namespace App\Http\Controllers;

use App\kavling;
use Illuminate\Http\Request;
use App\proyek;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\KodeAkun;
use App\Transaksi;
use App\pemesanan;
use App\Pembatalan;
use Carbon;
use DB;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['login']);
        $this->middleware('proyek')->except(['pilih', 'chooseproyek', 'tambah_proyek', 'login']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    /*public function index($id)
    {
        $proyek=proyek::find($id);
        if ($proyek) {
            Session::put('id_proyek',$proyek->id);
            Session::put('proyek',$proyek->name);
            Session::put('alamat',$proyek->alamat);
            Session::put('jmlh_kavling',$proyek->alamat);   
        }
        return view('progress_penjualan.index');
    }*/

    public function dash()
    {
        // var_dump(session('id_proyek'));return;
        $seven = \Carbon\Carbon::today()->subDays(7);
        $bulan = \Carbon\Carbon::today()->subDays(30);
        $booking7 = pemesanan::where(array(['status_ppjb', '=', PENDING], ['tgl_pesan', '>', $seven], ['status_pembatalan', '=', TIDAK_BATAL], ['proyeks_id', '=', session('id_proyek')]))->count();
        $booking30 = pemesanan::where(array(['status_ppjb', '=', PENDING], ['tgl_pesan', '>', $bulan], ['status_pembatalan', '=', TIDAK_BATAL], ['proyeks_id', '=', session('id_proyek')]))->count();
        $penjualan7 = DB::table('pemesanans')
            ->select('tgl_pesan', DB::raw('count(*) as total'))
            ->where(array(['tgl_pesan', '>', $seven], ['proyeks_id', '=', session('id_proyek')], ['status_ppjb', '=', DISETUJUI], ['status_pembatalan', '=', TIDAK_BATAL]))
            ->groupBy('tgl_pesan', 'proyeks_id')
            ->orderBy('tgl_pesan', 'DESC')
            ->get();
        $penjualan30 = DB::table('pemesanans')
            ->select('tgl_pesan', DB::raw('count(*) as total'))
            ->where(array(['tgl_pesan', '>', $bulan], ['proyeks_id', '=', session('id_proyek')], ['status_ppjb', '=', DISETUJUI], ['status_pembatalan', '=', TIDAK_BATAL]))
            ->groupBy('tgl_pesan', 'proyeks_id')
            ->orderBy('tgl_pesan', 'DESC')
            ->get();
        $booking = pemesanan::where(array(['status_ppjb', '=', PENDING], ['status_pembatalan', '=', TIDAK_BATAL], ['proyeks_id', '=', session('id_proyek')]))->count();
        // var_dump($penjualan7);return;
        $penjualan = pemesanan::where(array(['status_ppjb', '=', DISETUJUI], ['status_pembatalan', '=', TIDAK_BATAL], ['proyeks_id', '=', session('id_proyek')]))->count();
        return view('progress_penjualan.index', compact('seven', 'bulan', 'booking7', 'booking30', 'penjualan7', 'penjualan30', 'booking', 'penjualan'));
    }

    public function pilih()
    {
        $proyek = proyek::all();
        return view('daftarproyek.index', compact('proyek'));
    }

    public function login(Request $r)
    {
        $user = $r->only('email', 'password');
        $findUser = User::where('email', '=', $r->input('email'))->first();
        if ($findUser) {
            if ($findUser->aktif == 1) {
                if (Auth::attempt(['email' => $user['email'], 'password' => $user['password']])) {
                    return redirect('pilih-proyek');
                } else {
                    return Redirect::back()->withErrors(['message1' => 'Email atau password anda salah!']);
                }
            } else {
                return Redirect::back()->withErrors(['message1' => 'Akun Telah Dinonaktifkan!']);
            }
        } else {
            return Redirect::back()->withErrors(['message1' => 'Email Belum Terdaftar!']);
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('login');
    }

    public function lihat_user()
    {
        // if (Session::has('id_proyek')) {
        //     echo Session::get('proyek'); return;
        // }
        if (auth()->user()->getRoleNames() != '["Komisaris"]' && auth()->user()->getRoleNames() != '["Manajer"]' && auth()->user()->getRoleNames() != '["Admin"]') {
            $user = User::find(auth()->user()->id);
            //return auth()->user()->getRoleNames();
            $signal = 0;
        } else {
            $user = user::all();
            $signal = 1;
        }
        $role = Role::orderBy('name', 'ASC')->get();

        return view('user.index', compact('user', 'role', 'signal'));
    }

    public function detail_user($id)
    {
        if (auth()->user()->id != $id && auth()->user()->getRoleNames() != '["Komisaris"]' && auth()->user()->getRoleNames() != '["Manajer"]' && auth()->user()->getRoleNames() != '["Admin"]') {
            return redirect('/user')->with('error', 'Unauthorized Page');
        }
        $user = user::find($id);
        return view('user.detail', compact('user'));
    }

    public function edit_user($id)
    {
        /*if (auth()->user()->id !== $id) {
            return redirect('/user/detail/'.$id)->with('error', 'Unauthorized Page');
        }*/
        $user = user::find($id);
        return view('user.edit', compact('user'));
    }

    public function save_edit_user(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'alamat' => 'required',
            'no_hp' => 'required|numeric|digits_between:10,14',
            'ktp' => 'required|numeric|digits_between:16,16',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        $user = user::find($r->id);
        $user->name = $r->input('name');
        $user->email = $r->input('email');
        $user->alamat = $r->input('alamat');
        $user->telp = $r->input('no_hp');
        $user->no_ktp = $r->input('ktp');
        if ($r->file('foto')) {
            $file = $r->file('foto');
            $extension = $file->getClientOriginalExtension();
            $filename =  'PROFIL_' . $r->id . '_' . trim($r->input('email')) . '.' . $extension;
            $file->move('foto', $filename);
            $user->avatar = $filename;
        }
        $user->save();
        return redirect('user/detail/' . $r->id)->with('message', 'Data diri ' . $user->name . ' berhasil diubah');
    }

    public function tambah_proyek(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'proyek' => 'required|unique:proyeks,name',
            'alamat_proyek' => 'required',
            'jumlah_kavling' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }

        DB::beginTransaction();
        try {
            // tambah proyek
            $post = new proyek;
            $post->name = $r->input('proyek');
            $post->alamat = $r->input('alamat_proyek');
            $post->jmlh_kavling = $r->input('jumlah_kavling');
            $post->save();

            // tambah kavling
            for ($i = 0; $i < $post->jmlh_kavling; $i++) {
                $kavling = new kavling;
                $kavling->proyeks_id = $post->id;
                $kavling->name = "default";
                // $kavling->depan = ;
                // $kavling->belakang = ;
                // $kavling->kiri = ;
                // $kavling->kanan = ;
                $kavling->lebar = 0;
                $kavling->panjang = 0;
                $kavling->luas = 0;
                $kavling->harga = 0;
                $kavling->diskon = 0;
                $kavling->hpp = 0;
                $kavling->status = KAVLING_AVAILABLE;
                $kavling->save();
            }
            DB::commit();
            return Redirect::back()->with('message', 'Proyek ' . $post->name . ' berhasil ditambahkan');
        } catch (\Throwable $th) {
            DB::rollback();
            return Redirect::back()->with('message-error', 'Proyek gagal ditambahkan');
        }
    }

    public function chooseproyek(Request $r)
    {
        $proyek = proyek::where('name', '=', $r->input('nama'))->first();
        if ($proyek) {
            Session::put('id_proyek', $proyek->id);
            Session::put('proyek', $proyek->name);
            Session::put('alamat', $proyek->alamat);
            Session::put('jmlh_kavling', $proyek->jmlh_kavling);
            Session::put('foto', $proyek->foto);
        }
        // var_dump(session('id_proyek'));return;
        return redirect('home');
    }

    public function tambah_user(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'user' => 'required',
            'no_hp' => 'required|numeric|digits_between:10,14',
            'ktp' => 'required|numeric|digits_between:16,16',
            'email' => 'required|unique:users,email',
            'password' => 'required|same:confirm_password',
            'confirm_password' => 'required',
            'role' => 'required',
            'foto' => 'mimes:jpeg,jpg,png|max:2048',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        $post = new user;
        $post->name = $r->input('user');
        $post->email = $r->input('email');
        $post->password = bcrypt($r->password);
        $post->telp = $r->input('no_hp');
        $post->no_ktp = $r->input('ktp');
        //$post->role= $r->input('role');
        $post->alamat = $r->input('alamat');
        $post->aktif = 1;
        if ($r->file('foto')) {
            $post->avatar = $r->file('foto')->store('foto');
        }
        $post->assignRole($r->role);
        $post->save();
        return Redirect::back()->with('message', 'User ' . $post->name . ' berhasil ditambahkan');
    }

    public function ubahpass(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'password_lama' => 'required',
            'password' => 'required|same:confirm_password',
            'confirm_password' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        if (!(Hash::check($r->input('password_lama'), Auth::user()->password))) {
            return redirect('/user/detail/' . $r->id)->with('error', 'Password lama salah');
        }
        if (strcmp($r->input('password_lama'), $r->input('password')) == 0) {
            return redirect('/user/detail/' . $r->id)->with("error", "Password baru sama dengan password lama, silakan pilih password yang berbeda");
        }
        if (strcmp($r->input('password'), $r->input('confirm_password')) != 0) {
            return redirect('/user/detail/' . $r->id)->with('error', 'Password baru tidak sama dengan confirm password');
        }
        $user = user::find($r->id);
        $user->password = bcrypt($r->input('password'));
        $user->save();
        return Redirect::back()->with('message', 'Password dari user ' . $user->name . ' berhasil diubah');
    }

    public function manage_user(Request $request)
    {
        $idUser = $request->input('id-user');
        $user = user::find($idUser);
        if ($user->aktif == 1) {
            $user->aktif = 0;
            $user->save();
            return redirect('/user/detail/' . $idUser)->with('message', 'User ' . $user->name . ' berhasil dinonaktifkan');
        } elseif ($user->aktif == 0) {
            $user->aktif = 1;
            $user->save();
            return redirect('/user/detail/' . $idUser)->with('message', 'User ' . $user->name . ' berhasil diaktifkan');
        }
    }

    public function resetpass(Request $request)
    {
        $idUser = $request->input('id-user');
        $user = User::find($idUser);
        if (auth()->user()->getRoleNames() == '["Admin"]' && ($user->getRoleNames() == '["Komisaris"]' || $user->getRoleNames() == '["Manajer"]')) {
            return redirect('/user/detail' . $idUser)->with('error', 'Unauthorized Page');
        } elseif (auth()->user()->getRoleNames() == '["Manajer"]' && $user->getRoleNames() == '["Komisaris"]') {
            return redirect('/user/detail' . $idUser)->with('error', 'Unauthorized Page');
        } elseif (auth()->user()->id != $idUser && auth()->user()->getRoleNames() == $user->getRoleNames()) {
            return redirect('/user/detail' . $idUser)->with('error', 'Unauthorized Page');
        }
        $user->password = bcrypt($user->email);
        $user->save();
        return redirect('/user/detail/' . $idUser)->with('message', 'Password user ' . $user->name . ' berhasil direset');
    }

    public function rolePermission(Request $request)
    {
        $role = $request->get('role');

        $permissions = null;
        $hasPermission = null;

        $roles = Role::all()->pluck('name');

        if (!empty($role)) {
            $getRole = Role::findByName($role);

            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();

            $permissions = Permission::all()->pluck('name');
        }
        return view('user.role_permission', compact('roles', 'permissions', 'hasPermission'));
    }

    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        Permission::firstOrCreate([
            'name' => $request->name
        ]);
        return redirect()->back();
    }

    public function setRolePermission(Request $request, $role)
    {
        $role = Role::findByName($role);

        //syncPermission untuk menghapus semua permission yg dimiliki role tersebut
        //kemudian di-assign kembali sehingga tidak terjadi duplicate data
        $role->syncPermissions($request->permission);
        return redirect()->back()->with(['message' => 'Permission to Role Saved!']);
    }

    public function roles(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all()->pluck('name');
        return view('user.roles', compact('user', 'roles'));
    }

    public function setRole(Request $request, $id)
    {
        $this->validate($request, [
            'role' => 'required'
        ]);

        $user = User::findOrFail($id);
        //menggunakan syncRoles agar terlebih dahulu menghapus semua role yang dimiliki
        //kemudian di-set kembali agar tidak terjadi duplicate
        $user->syncRoles($request->role);
        return redirect()->back()->with(['message' => 'Role Sudah Di Set']);
    }
}
