<?php

namespace App\Http\Middleware;

use Closure;

class ProyekCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('id_proyek')){
            return redirect('/pilih-proyek');
        }
        return $next($request);
    }
}
