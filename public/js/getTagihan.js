$(document).ready(function () {
    $('#nomor-kavling').change(function () {
        var kavlingsId = $(this).val();
        if(kavlingsId){
            $.ajax({
                type: "GET",
                url: "/pemesan/get/"+kavlingsId,
                dataType: "json",
                success: function (response) {
                    var kodeAkun;
                    var jsonObj = response;
                    $('#kode-akun').change(function () {
                        $('#nominal').empty();
                        var idAkun = $(this).val();
                        $.ajax({
                            type: "GET",
                            url: "/kode_akun/get/"+idAkun,
                            dataType: "json",
                            success: function (response) {
                                var jsonObj2 = response;
                                kodeAkun = jsonObj2.kode_akun;
                                if (kodeAkun == 'A1') {
                                    $('#nominal').attr('value', jsonObj.utj);
                                    $('#nominal').attr('readonly', 'readonly');
                                }
                                if (kodeAkun == 'A3') {
                                    $('#nominal').attr('value', jsonObj.angsur);
                                    $('#nominal').attr('readonly', 'readonly');
                                }
                            }
                        });
                    });
                }
            });
        } else {
            $('#nominal').empty();
        }
    });
});
