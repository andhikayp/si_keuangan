$(document).ready(function () {
    $('#nomor-kavling').change(function () { 
        var kavlingsId = $(this).val();
        if(kavlingsId){
            $.ajax({
                type: "GET",
                url: "/pemesan/get/"+kavlingsId,
                dataType: "json",
                success: function (response) {
                    $('#nama-pemesan').empty();
                    $('#alamat-pemesan').empty();
                    $('#email-pemesan').empty();
                    $('#ktp-pemesan').empty();
                    $('#hp-pribadi-pemesan').empty();
                    $('#hp-kerabat-pemesan').empty();

                    var jsonObj = response;
                    $('#nama-pemesan').attr('placeholder', jsonObj.buyer);
                    $('#alamat-pemesan').attr('placeholder', jsonObj.alamat);
                    $('#email-pemesan').attr('placeholder', jsonObj.email);
                    $('#ktp-pemesan').attr('placeholder', jsonObj.no_ktp);
                    $('#hp-pribadi-pemesan').attr('placeholder', jsonObj.hp_pribadi);
                    $('#hp-kerabat-pemesan').attr('placeholder', jsonObj.hp_kerabat);
                }
            });
        } else {
            $('#nama-pemesan').empty();
            $('#alamat-pemesan').empty();
            $('#email-pemesan').empty();
            $('#ktp-pemesan').empty();
            $('#hp-pribadi-pemesan').empty();
            $('#hp-kerabat-pemesan').empty();
        }
    });
});