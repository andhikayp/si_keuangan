$(document).ready(function () {
    $('#nama-investor').change(function () { 
        var investorsId = $(this).val();
        if(investorsId){
            $.ajax({
                type: "GET",
                url: "/investor/get/"+investorsId,
                dataType: "json",
                success: function (response) {
                    $('#alamat-investor').empty();
                    $('#email-investor').empty();
                    $('#hp-investor').empty();

                    var jsonObj = response;
                    $('#alamat-investor').attr('placeholder', jsonObj.alamat);
                    $('#email-investor').attr('placeholder', jsonObj.email);
                    $('#hp-investor').attr('placeholder', jsonObj.telp);
                }
            });
        } else {
            $('#alamat-investor').empty();
            $('#email-investor').empty();
            $('#hp-investor').empty();
        }
    });
});