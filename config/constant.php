<?php
define('KOMISARIS_ROLE', 1);
define('MANAJER_ROLE', 2);
define('ADMIN_ROLE', 3);
define('MARKETING_ROLE', 4);
define('INVESTOR_ROLE', 5);

define('AKTIF', 1);
define('NON_AKTIF', 0);

define('PENDING', 0);
define('DISETUJUI', 1);
define('DITOLAK', 2);
define('DALAM_PROSES', 3);

define('KAVLING_AVAILABLE', 0);
define('KAVLING_BOOKING', 1);
define('KAVLING_SOLD_OUT', 2);

define('TIDAK_BATAL', 0);
define('BATAL_PESAN', 1);
define('BATAL_BELI', 2);

define('UTJ', 1);
define('DP1', 2);
define('DP2', 3);
define('DP3', 4);
define('Angsur', 5);
define('Lunas', 6);

define('NOPPJB', 0);
define('PPJB_DITERIMA', 1);
define('PPJB_DITOLAK', 2);

define('PENERIMAAN',0);
define('PENGELUARAN',1);