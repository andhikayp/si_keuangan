@extends('layouts.base.app')
@section('title', ' Site Plan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Site Plan</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Site Plan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        @php
        $foto = App\proyek::find(session('id_proyek'))
        @endphp
        @if (auth()->user()->can('create-siteplan'))
            @if(is_null($foto->foto))
                <a href="" data-toggle="modal" data-target="#modal-top2">
                    <button type="button" class="btn btn-sm btn-warning" style="margin-bottom:20px">
                        <i class="fa fa-plus mr-2"></i>Tambah Gambar Site Plan
                    </button>
                </a><br>
            @else
                <a href="" data-toggle="modal" data-target="#modal-top2">
                    <button type="button" class="btn btn-sm btn-warning" style="margin-bottom:20px">
                        <i class="fa fa-plus mr-2"></i>Ubah Gambar Site Plan
                    </button>
                </a><br>
            @endif
        @endif
        @if(!is_null($foto->foto))
            <img src="{{asset('/siteplan/'.$foto->foto)}}" class="img-fluid img-responsive rounded mx-auto d-block" style="max-width:100%;height:auto; margin-bottom:20px;">
        @endif
        <br>
        <div class="table-responsive" >                            
            <table id="table-pesan" class="table table-bordered table-vcenter">
                <thead>
                    <tr style="background-color :lightgreen ; color :black">
                        <th class="text-center">No</th>
                        <th class="text-center">Kavling</th>
                        <th class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1
                    @endphp
                    @foreach ($tampil as $ta)
                    <tr>
                        <td class="text-center">{{$no++}}</td>
                        <td class="text-center">{{$ta->name}}</td>
                        <td class="text-center">
                            @if($ta->status == KAVLING_AVAILABLE)
                                <button style="width: 80px;" type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#av{{$ta->id}}">Available</button>
                                
                            @elseif($ta->status == KAVLING_BOOKING)
                                <button style="width: 80px;" type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#sold{{$ta->id}}">Booked</button>
                                
                            @else    
                                <button style="width: 80px;" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-top" disabled>Sold Out</button>
                            @endif
                        </td>
                    </tr>
                    <!-- Top Modal -->
                    <!-- END Top Modal -->
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
    <div class="block-content">
        <div class="col-12 col-md-12 col-xl-12" style="margin-buttom : 20px;">
                <h4 style="margin-bottom:15px;">Kalkulator Angsuran</h4>
            </div>
        <div class="row" style="margin-bottom:40px">
            <div class="col-xs-6 col-md-4 col-xl-4">
                <div class="form-group">
                    <label class="col-12" for="nomor-kavling">Nomor Kavling</label>
                    <div class="col-12">
                        <select id="nomor-kavling" name="nomor-kavling" class="form-control">
                            <option value=""></option>
                            @foreach ($tampil as $item)
                                @if($item->status == KAVLING_AVAILABLE)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-4 col-xl-4">
                <div class="form-group">
                    <label class="col-12" for="harga-kavling">Harga Kavling</label>
                    <div class="col-12">
                        <input type="number" class="form-control" id="harga-kavling" name="harga-kavling" placeholder="Harga" disabled>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-4 col-xl-4">
                <div class="form-group">
                    <label class="col-12" for="dp">DP</label>
                    <div class="col-12">
                        <input type="number" class="form-control" id="dp" name="dp" placeholder="DP">
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-4 col-xl-4">
                <div class="form-group">
                    <label class="col-12" for="kali-kavling">Berapa kali Angsur</label>
                    <div class="col-12">
                        <select id="kali-angsur" name="kali-angsur" class="form-control">
                            <option value=""></option>
                            <option value="1">12 Bulan</option>
                            <option value="2">24 Bulan</option>
                            <option value="3">36 Bulan</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-4 col-xl-4">
                <div class="form-group">
                    <label class="col-12" for="angsur">Angsuran perbulan</label>
                    <div class="col-12">
                        <input type="text" class="form-control" id="angsur" name="angsur" placeholder="Angsuran" disabled>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-md-4 col-xl-4">
                <div class="form-group">
                    <label class="col-12" for="total">Total Harga Kredit</label>
                    <div class="col-12">
                        <input type="text" class="form-control" id="total" name="total" placeholder="Total Harga Kredit" disabled>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>
@foreach($tampil as $ta)
@if($ta->status == KAVLING_AVAILABLE)
<div class="modal fade" id="av{{$ta->id}}" tabindex="-1" role="dialog" aria-labelledby="av{{$ta->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Detail Kavling {{$ta->name}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                    <table class="table">
                        <tr><td>Lebar</td><td>:</td><td>{{$ta->lebar}} m</td></tr>
                        <tr><td>Panjang</td><td>:</td><td>{{$ta->panjang}} m</td></tr>
                        <!-- <tr><td>Panjang Kiri</td><td>:</td><td>{{$ta->kiri}} m</td></tr>
                        <tr><td>Panjang Kanan</td><td>:</td><td>{{$ta->kanan}} m</td></tr> -->
                        <tr><td>Luas</td><td>:</td><td>{{$ta->luas}} m<sup>2</sup></td></tr>
                        <tr><td>Harga</td><td>:</td><td>Rp <span class="harga-tiap-kavling">{{number_format($ta->harga - $ta->diskon,2,',','.')}}</span></td></tr>
                        {{-- <tr><td>Diskon</td><td>:</td><td>Rp <span class="diskon-tiap-kavling">{{number_format($ta->diskon,2,',','.')}}</span></td></tr> --}}
                    </table>
                    <input hidden style="display:none" id="item-kavling-harga{{$ta->id}}" value="{{$ta->harga}}">
                    <input hidden style="display:none" id="item-kavling-diskon{{$ta->id}}" value="{{$ta->diskon}}">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                    <i class="fa fa-check"></i> Oke
                </button>
            </div>
        </div>
    </div>
</div>
@elseif($ta->status == KAVLING_BOOKING)
@php
    // $isi = App\pemesanan::where('status_pembatalan','=',TIDAK_BATAL)
    //                     ->where('kavlings_id','=',$ta->id)
    //                     ->first();
    $isi = App\pemesanan::where('kavlings_id','=',$ta->id)
    ->latest('created_at')->first();;
@endphp
<div class="modal fade" id="sold{{$ta->id}}" tabindex="-1" role="dialog" aria-labelledby="sold{{$ta->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                <h3 class="block-title">Detail Kavling {{$ta->name}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-6 col-md-6 col-s-6">
                            <p style="margin-bottom:3px">Nama Proyek</p>
                            <h5>{{$isi->proyeks->name}}</h5>
                        </div>
                        <div class="col-6 col-md-6 col-s-6">
                            <p style="margin-bottom:3px">Nama Pemesan</p>
                            <h5>{{$isi->buyer}}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-md-6 col-s-6">
                            <p style="margin-bottom:3px">Alamat</p>
                            <h5>{{$isi->alamat}}</h5>
                        </div>
                        <div class="col-6 col-md-6 col-s-6">
                            <br>
                            <a href="{{url('pemesanan/detail/'.$isi->id)}}">Detail</a>
                        </div>
                    </div>
                    <div class="table-responsive">
                    <table class="table table-hover table-vcenter">
                        <tr>
                            <td> </td>
                            <td>UTJ</td>
                            <td>Rp  </td><td>{{number_format($isi->utj,2,',','.')}}</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td>DP1</td>
                            <td>Rp  </td><td>{{number_format($isi->dp1,2,',','.')}}</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td>DP2</td>
                            <td>Rp  </td><td>{{number_format($isi->dp2,2,',','.')}}</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td>DP3</td>
                            <td>Rp  </td><td>{{number_format($isi->dp3,2,',','.')}}</td>
                        </tr>
                        <tr>
                            <td> </td>
                            <td>Angsuran</td>
                            <td> </td>
                            <td>{{$isi->angsur_jml}} X Rp {{number_format($isi->angsur,2,',','.')}}</td><td></td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                    <i class="fa fa-check"></i> Oke
                </button>
            </div>
        </div>
    </div>
</div>
@endif
@endforeach

{{-- Modal tambah gambar --}}
<div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <form method="POST" action="{{ url('/tambah_gambar') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                @if(is_null(session('foto')))
                    <h3 class="block-title" style="text-align: center">Tambah Gambar</h3>
                @else
                    <h3 class="block-title" style="text-align: center">Ubah Gambar</h3>
                @endif
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-group row">
                        <label class="col-12" for="no_kavling">Pilih Gambar yang akan di upload</label>
                        <div class="col-md-12">
                            <input type="file" id="example-file-input" name="gambar">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-alt-success">
                    <i class="fa fa-check"></i> Submit
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('#table-pesan').DataTable({
            "autoWidth": true,
            "ordering": true,
        });
        $("#nomor-kavling").select2({
            placeholder: "Nomor Kavling",
            width: '100%'
        });
        $("#kali-angsur").select2({
            placeholder: "Lama Mengangsur",
            width: '100%'
        });
        $('#nomor-kavling').change(function () {
            var value = $(this).val();
            $('#harga-kavling').val($('#item-kavling-harga'+value).val()-$('#item-kavling-diskon'+value).val());
        });
        $('#kali-angsur').change(function () {
            var kaliAngsur = $('#kali-angsur').val();
            var hargaSudahDP = $('#harga-kavling').val() - $('#dp').val();
            var perbulan = hargaSudahDP * (1 + (kaliAngsur * 0.1));
            var total = parseFloat(perbulan) + parseFloat($('#dp').val());
            if(kaliAngsur == 1){
                perbulan = perbulan / 12;
            }
            if(kaliAngsur == 2){
                perbulan = perbulan / 24;
            }
            if(kaliAngsur == 3){
                perbulan = perbulan / 36;
            }
            //var total = parseFloat((perbulan*12)) + parseFloat($('#dp').val());
            $('#angsur').val(perbulan.toLocaleString('en', {maximumFractionDigits : 2}));
            $('#total').val(total.toLocaleString('en', {maximumFractionDigits : 2}));
        });
    });
</script>
@endsection