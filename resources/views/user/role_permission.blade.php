@extends('layouts.base.app')
@section('title', 'Manajemen Permission')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Manajemen Permission</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Manajemen Permission</h3>
    </div>
    <div class="block-content block-content-full">
        {{--<a href="javascript:void(0)" disabled class="btn btn-sm btn-warning mb-3" data-toggle="modal" data-target="#modal-top2"><i class="fa fa-plus mr-2"></i>Tambah Permission</a>--}}
        <form action="{{ route('users.roles_permission') }}" method="GET" enctype="multipart/form-data">
            <div class="form-group row">
                <label for="" class="col-12">Roles</label>
                <div class="input-group col-md-9">
                    <select name="role" class="form-control">
                        @foreach ($roles as $value)
                            <option value="{{ $value }}" {{ request()->get('role') == $value ? 'selected':'' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-danger">Check!</button>
                    </span>
                </div>
                <div class="col-md-9">
                    <p>Pastikan klik tombol "Check!" setelah memilih role.</p>
                </div>
            </div>
        </form>

        {{-- jika $permission tidak bernilai kosong --}}
        @if (!empty($permissions))
            <form action="{{ route('users.setRolePermission', request()->get('role')) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1" data-toggle="tab">Permissions</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                @php $no = 1; @endphp
                                @foreach ($permissions as $key => $row)
                                    <input type="checkbox" 
                                        name="permission[]" 
                                        class="minimal-red" 
                                        value="{{ $row }}"
                                        {{--  CHECK, JIKA PERMISSION TERSEBUT SUDAH DI SET, MAKA CHECKED --}}
                                        {{ in_array($row, $hasPermission) ? 'checked':'' }}
                                        > {{ $row }} <br>
                                    @if ($no++%4 == 0)
                                    <br>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                
                <br>
                <button class="btn btn-primary btn-sm">
                    <i class="fa fa-send"></i> Set Permission
                </button>
            </form>
        @endif

        <!-- MODAL -->
        <div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
                <form method="POST" action="{{ route('users.add_permission') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Tambah Permission</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                        <i class="si si-close"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="form-group row">
                                    <label class="col-12" for="name">Nama Permission</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nama Permission.." required> 
                                    </div>
                                </div>           
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-alt-success">
                                <i class="fa fa-check"></i> Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection