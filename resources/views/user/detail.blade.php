@extends('layouts.base.app')
@section('title', 'Detail User')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/user')}}">User</a>
    <span class="breadcrumb-item active">Detail</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail User</h3>
        {{-- <div class="block-options">
            <a href="javascript:void(0)" disabled data-toggle="modal" data-target="#modal-top2"><button type="button" class="btn-block-option" data-toggle="tooltip" title="Ubah Password"><i class="si si-wrench"></i></button></a>
        </div> --}}
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <h2 class="content-heading">Foto <small>Profil</small></h2>
                <div class="row items-push js-gallery img-fluid-100">
                    <div class="col-md animated fadeIn" style="text-align : center;">
                        <a class="" href="{{ asset('/foto/'.$user->avatar) }}" target="_blank">
                            <img class="img-fluid text" style="max-height:500px; width:auto;"src="{{ asset('/foto/'.$user->avatar) }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama</p>
                        <h5>{{$user->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>{{$user->email}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat</p>
                        <h5>{{$user->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP</p>
                        <h5>{{$user->telp}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">KTP</p>
                        <h5>{{$user->no_ktp}}</h5>
                    </div>
                </div>
                @php 
                    $userRoles = $user->getRoleNames();
                @endphp
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Role</p>
                        <h5>
                            @if (is_array($userRoles) || is_object($userRoles))
                                @foreach ($userRoles as $userRole)
                                    <label for="" class="badge badge-warning">{{ $userRole }}</label>
                                @endforeach
                            @endif
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-6"></div>
            @if (auth()->user()->id == $user->id || auth()->user()->getRoleNames() == '["Komisaris"]' || auth()->user()->getRoleNames() == '["Manajer"]' || auth()->user()->getRoleNames() == '["Admin"]')
                <button type="submit" class="col-md-2 btn bg-pulse text-white" data-toggle="modal" data-target="#modal-top2">Ubah Password</button>&nbsp
            @endif
            @if (auth()->user()->can('create-user'))
                <button type="submit" class="col-md-2 btn bg-warning text-white"><a href="{{url('/user/edit/'.$user->id)}}" class="text-white">Edit Profil</a></button>&nbsp
                <form action="{{ action('HomeController@manage_user') }}" method="post" enctype="multipart/form-data" id="ubah-status">
                    {{ csrf_field() }}
                    <input type="hidden" name="id-user" value="{{$user->id}}">
                    <button type="submit" class="col-md-12 btn bg-earth text-white" id="yakin">
                        @if($user->aktif == 1)
                            Nonaktifkan
                        @elseif($user->aktif == 0)
                            Aktifkan
                        @endif
                    </button>
                </form>
            @endif
        </div>
        <br><br><br><br>
        @if (auth()->user()->can('create-user'))
            <hr style="border-width:3px">
            <div class="row mb-3">
                <div class="col-3"></div>
                <div class="col-6 text-center">
                    <form action="{{ action('HomeController@resetpass') }}" method="post" enctype="multipart/form-data" id="reset-pass">
                        {{ csrf_field() }}
                        <input type="hidden" name="id-user" value="{{$user->id}}">
                        <button type="submit" class="col-md-12 btn btn-primary text-white" id="yakin2">Reset Password ke Default</button>
                    </form>
                    <p>Password default adalah sama seperti alamat email.</p>
                </div>
                <div class="col-3"></div>
            </div>
        @endif
    </div>
    <!-- END DIV BLOCK -->
    <!-- Top Modal -->
    <div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
            <form method="POST" action="{{ url('/user/ubahpass/') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input value="{{$user->id}}" type="text" class="form-control" id="id" name="id" hidden>
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Ubah Password</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <label class="col-12" for="email-input">Nama User</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="{{$user->name}}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="email-input">Email</label>
                                <div class="col-md-12">
                                    <input type="email" class="form-control" id="email-input" name="email-input" value="{{$user->email}}" placeholder="Email" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Password Lama</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="password_lama" name="password_lama" placeholder="Password Lama" required> 
                                </div>
                            </div>           
                            <div class="form-group row">
                                <label class="col-12" for="alamat-input">Password Baru</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="alamat-input">Konfirmasi Password Baru</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Konfirmasi Password" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-alt-success">
                            <i class="fa fa-check"></i> Submit
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <!-- END Top Modal -->
</div>
@endsection
@section('moreJS')
<script src="jquery-1.11.2.min.js"></script>
<script src="select2.min.js"></script>
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select"
        });
        $("#lokasi").select2({
            placeholder: "Please Select"
        });
        $('#yakin').click(function(){
            event.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan melanjutkan?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, submit!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#ubah-status').submit();
                }
            })
        });
        $('#yakin2').click(function(){
            event.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan melanjutkan?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, submit!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#reset-pass').submit();
                }
            })
        });
    });
</script>
<script>
    jQuery(function () {
        Codebase.helpers('magnific-popup');
    });
</script>
@endsection