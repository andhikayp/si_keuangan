@extends('layouts.base.app')
@section('title', 'Edit User')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/user')}}">User</a>
    <a class="breadcrumb-item" href="{{url('/user/detail/'.$user->id)}}">Detail</a>
    <span class="breadcrumb-item active">Edit</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Edit User</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <h2 class="content-heading">Foto <small>Profil</small></h2>
                <div class="row items-push js-gallery img-fluid-100">
                    <div class="col-md animated fadeIn" style="text-align:center;">
                        <a class="" href="{{ asset('/foto/'.$user->avatar) }}" target="_blank">
                            <img class="img-fluid text" style="max-height:500px; width:auto;" src="{{ asset('/foto/'.$user->avatar) }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ url('/user/edit_user/') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input value="{{$user->id}}" type="text" class="form-control" id="id" name="id" hidden>
            <div class="col-md-6">
                {{-- <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama</p>
                        <h5>{{$user->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>{{$user->email}}</h5>
                    </div>
                </div> --}}
                <div class="form-group row">
                    <div class="col-12">
                        <div class="form-material form-material-primary floating input-group">
                            <input value="{{$user->name}}" type="text" class="form-control" id="name" name="name" required>
                            <label for="no_hp">Nama</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="form-material form-material-primary floating input-group">
                            <input value="{{$user->email}}" type="text" class="form-control" id="email" name="email" required>
                            <label for="no_hp">Email</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="form-material form-material-primary floating input-group">
                            <input value="{{$user->telp}}" type="text" class="form-control" id="no_hp" name="no_hp" required>
                            <label for="no_hp">No HP</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="form-material form-material-primary floating input-group">
                            <input value="{{$user->alamat}}" type="text" class="form-control" id="alamat" name="alamat" required>
                            <label for="alamat">Alamat</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12">
                        <div class="form-material form-material-primary floating input-group">
                            <input value="{{$user->no_ktp}}" type="text" class="form-control" id="ktp" name="ktp" required>
                            <label for="ktp">No KTP</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="deskripsi-setor">Upload Foto</label>
                    <div class="col-md-9">
                        <input type="file" name="foto" id="foto">
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row mb-3">
            <div class="col-3"></div>
                <button type="button" class="col-3 btn bg-pulse text-white"><a href="{{url('/user/detail/'.auth()->user()->id)}}" class="text-white">Batalkan</a></button>&nbsp
                <button type="submit" class="col-3 btn bg-earth text-white">Simpan</button>
            <div class="col-3"></div>
        </div>
    </form>
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script src="jquery-1.11.2.min.js"></script>
<script src="select2.min.js"></script>
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select"
        });
        $("#lokasi").select2({
            placeholder: "Please Select"
        });
    });
</script>
<script>
    jQuery(function () {
        Codebase.helpers('magnific-popup');
    });
</script>
@endsection