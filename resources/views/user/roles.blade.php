@extends('layouts.base.app')
@section('title', 'Set Role')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('user')}}">Manajemen User</a>
    <span class="breadcrumb-item active">Set Role</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Set Role</h3>
    </div>
    <div class="block-content block-content-full">
        <form action="{{ route('users.set_role', $user->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <td>:</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>:</td>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>Role</th>
                            <td>:</td>
                            <td>
                                @foreach ($roles as $row)
                                <input type="radio" name="role" 
                                    {{ $user->hasRole($row) ? 'checked':'' }}
                                    value="{{ $row }}"> {{  $row }} <br>
                                @endforeach
                            </td>
                        </tr>
                    </thead>
                </table>
            </div>
            <button type="submit" class="btn btn-primary btn-sm">
                <i class="fa fa-send"></i> Set Role
            </button>
        </form>
    </div>
</div>
@endsection