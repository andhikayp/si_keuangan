@extends('layouts.base.app')
@section('title', ' Manajemen User')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="#">Dashboard</a>
    <span class="breadcrumb-item active">Manajemen User</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Manajemen User</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        @if (auth()->user()->can('create-user'))
            <a href="javascript:void(0)" disabled class="btn btn-sm btn-warning mb-3" data-toggle="modal" data-target="#modal-top2"><i class="fa fa-plus mr-2"></i>Tambah User</a>
        @endif
        <div class="table-responsive">                            
            <table id="table-pesan" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">No HP</th>
                        <th class="text-center">Role</th>
                        <th class="text-center" style="width: 15%;">Status</th>
                        <th class="text-center" style="width: 15%;">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @if ($signal == 0)
                        <tr>
                            <td class="text-center">
                                {{$no}}
                                @php 
                                    $no++;
                                    $userRoles = $user->getRoleNames(); 
                                @endphp
                            </td>
                            <td class="font-w600">{{$user->name}}</td>
                            <td>{{$user->telp}}</td>
                            <td class="text-center">
                                @if (is_array($userRoles) || is_object($userRoles))
                                    @foreach ($userRoles as $userRole)
                                        <label for="" class="badge badge-warning">{{ $userRole }}</label>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center">
                            @if($user->aktif == 1)
                                <span class="badge badge-success"><i class="fa fa-check"></i> Aktif</span>                            
                            @else
                                <span class="badge badge-danger"><i class="fa fa-times"></i> Nonaktif</span>
                            @endif
                                {{-- <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span> --}}
                            </td>
                            <td class="text-center">
                                <a href="{{url('/user/detail/'.$user->id)}}">
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Lihat Detail">
                                        <i class="fa fa-user"></i> Detail
                                    </button>
                                </a>
                                @if (auth()->user()->getRoleNames() == '["Komisaris"]' || auth()->user()->can('manage-rolepermission'))
                                    <a href="{{ route('users.roles', $user->id) }}">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="Ubah Role">
                                            <i class="fa fa-user-secret"></i>
                                        </button>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @else
                        @foreach($user as $users)
                        <tr>
                            <td class="text-center">
                                {{$no}}
                                @php 
                                    $no++;
                                    $userRoles = $users->getRoleNames(); 
                                @endphp
                            </td>
                            <td class="font-w600">{{$users->name}}</td>
                            <td>{{$users->telp}}</td>
                            <td class="text-center">
                                @if (is_array($userRoles) || is_object($userRoles))
                                    @foreach ($userRoles as $userRole)
                                        <label for="" class="badge badge-warning">{{ $userRole }}</label>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center">
                            @if($users->aktif == 1)
                                <span class="badge badge-success"><i class="fa fa-check"></i> Aktif</span>                            
                            @else
                                <span class="badge badge-danger"><i class="fa fa-times"></i> Nonaktif</span>
                            @endif
                                {{-- <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span> --}}
                            </td>
                            <td class="text-center">
                                <a href="{{url('/user/detail/'.$users->id)}}">
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Lihat Detail">
                                        <i class="fa fa-user"></i> Detail
                                    </button>
                                </a>
                                @if (auth()->user()->getRoleNames() == '["Komisaris"]' || auth()->user()->can('manage-rolepermission'))
                                    <a href="{{ route('users.roles', $users->id) }}">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="tooltip" title="Ubah Role">
                                            <i class="fa fa-user-secret"></i>
                                        </button>
                                    </a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>

        <div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
            <form method="POST" action="{{ url('/tambah_user/') }}" enctype="multipart/form-data" id="add-user">
            {{ csrf_field() }}
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Tambah User</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Nama</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="user" name="user" placeholder="Nama" required> 
                                </div>
                            </div>           
                            <div class="form-group row">
                                <label class="col-12" for="email-input">Email</label>
                                <div class="col-md-12">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">No HP</label>
                                <div class="col-md-12">
                                    <input type="number" class="form-control" id="no_hp" name="no_hp" placeholder="No HP" required> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Alamat</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Alamat" required> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">No KTP</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="ktp" name="ktp" placeholder="KTP" required> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="alamat-input">Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="alamat-input">Konfirmasi Password</label>
                                <div class="col-md-12">
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12">Role</label>
                                <div class="col-md-12">
                                    <select id="role" name="role" class="form-control" required>
                                        <option value="" selected disabled>Pilih Role</option>
                                        @foreach ($role as $row)
                                            <option value="{{ $row->name }}">{{ $row->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Foto Profil</label>
                                <div class="col-md-12">
                                    <input type="file" class="form-control" id="foto" name="foto"> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-alt-success" id="yakin">
                            <i class="fa fa-check"></i> Submit
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
    {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script> --}}
    <script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#table-pesan').DataTable({
                "autoWidth": true,
                "ordering": false,
            });
            $('#yakin').click(function(){
                event.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Apakah anda yakin akan menambah User? Mohon cek ulang data apabila belum yakin",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, submit!',
                    cancelButtonText: 'Tidak'
                    }).then((result) => {
                    if (result.value) {
                        $('#add-user').submit();
                    }
                })
            });
        });
    </script>
@endsection