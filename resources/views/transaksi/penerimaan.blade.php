@extends('layouts.base.app')
@section('title', 'Penerimaan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Penerimaan</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Transaksi Penerimaan</h3>
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-transaksi'))
            <a href="{{url('/penerimaan/form')}}">
            <button type="button" class="btn btn-sm btn-warning">
                <i class="fa fa-plus mr-2"></i>Tambah Penerimaan
            </button></a>
            <br><br> 
        @endif
        <div class="table-responsive">                            
            <table id="table-pesan" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th class="d-sm-table-cell">Income</th>
                        <th class="d-sm-table-cell">Kode Akun</th>
                        <th class="d-sm-table-cell">Nominal</th>
                        <th class="d-sm-table-cell">Oleh</th>
                        <th class="d-sm-table-cell">Tgl</th>
                        <th class="d-sm-table-cell">Sts</th>
                        <th style="width: 10%;">opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $no = 1    
                    @endphp
                    @foreach ($daftar as $item)    
                    <tr>
                        <td class="text-center">{{$no++}}</td>
                        <td>
                            @if($item->pemesanans)
                                {{$item->pemesanans->kavlings->name}} {{$item->deskripsi}}
                            @endif
                            @if($item->investors)
                                {{$item->investors->users->name}} {{$item->deskripsi}}
                            @endif
                        </td>
                        <td>{{substr($item->kode_akuns->kode_akun,1)}}. {{$item->kode_akuns->nama_akun}}</td>
                        <td>Rp&nbsp;{{number_format($item->jumlah,2,',','.')}}</td>
                        <td>{{$item->penginput->name}}</td>
                        <td>{{ date('d-m-Y', strtotime($item->tgl_transaksi)) }}</td>
                        <td>
                            @if($item->status==PENDING)
                            <span class="badge badge-warning">Pending</span>
                            @elseif($item->status==DISETUJUI)
                            <span class="badge badge-success">Acc</span>
                            @elseif($item->status==DITOLAK)
                            <span class="badge badge-danger">Tolak</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{asset('/storage/'.$item->bukti)}}"><button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Detail Pesanan">
                                <i class="fa fa-book"></i> Detail
                            </button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
</div>
@endsection
@section('moreJS')
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script> --}}
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('#table-pesan').DataTable({
                    "autoWidth": true,
                    "ordering": false,
                });
            });
        </script>
        @endsection