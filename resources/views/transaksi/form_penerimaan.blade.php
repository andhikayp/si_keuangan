@extends('layouts.base.app')
@section('title', ' Form Penerimaan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
@php
function recursive($kode){
    foreach($kode->child as $anak){
        if(!is_null($anak)){
            echo('<option value="'.$anak->id.'">'.substr($anak->kode_akun,1).' &emsp;'.$anak->nama_akun.'</option>');
                recursive($anak);
        }
    }
}
@endphp
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/penerimaan')}}">Penerimaan</a>
    <span class="breadcrumb-item active">Tambah Penerimaan</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Penerimaan</h3>

    </div>
    <div class="block-content">
        <form action="{{url('/tambah_transaksi')}}" method="post" enctype="multipart/form-data" id="add_trans">
            {{csrf_field()}}
            <div class="form-group row">
                <label class="col-12" for="nama-proyek-input">Tanggal</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="nama-proyek-input" name="tanggal" >
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="nama-proyek-input">Tipe Penerimaan</label>
                <div class="col-md-9">
                    <select id="tipe-penerimaan" name="tipe-penerimaan" class="form-control">
                        <option value="0"></option>
                        <option value="1">Penerimaan Kavling</option>
                        <option value="2">Investor Masuk</option>
                    </select>
                </div>
            </div>
            <div id="internal" style="display:none">
                <div class="form-group row" id="kavling-group">
                    <label class="col-12" for="kavling">Kavling</label>
                    <div class="col-md-9">
                        <select id="nomor-kavling" name="no_pemesanan" class="form-control">
                            <option value=""></option>
                            @foreach($daftar as $data)
                            <option value="{{$data->id}}">{{$data->kavlings->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="investor-group">
                    <label class="col-12" for="investor">Nama Investor | Total Setor | Tgl</label>
                    <div class="col-md-9">
                        <select id="investor" name="investor" class="form-control">
                            <option value=""></option>
                            @foreach($investor as $data)
                                @if(App\Transaksi::where('investors_id','=',$data->id)
                                            ->where('status','=',DISETUJUI)
                                            ->sum('jumlah') < $data->jumlah_setor_modal)
                                <option value="{{$data->id}}">{{$data->users->name}} | {{number_format($data->jumlah_setor_modal,0,',','.')}} | {{date('d-m-Y', strtotime($data->tgl_perjanjian))}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="kode-akun">Kode Akun</label>
                    <div class="col-md-9">
                        <select id="kode-akun" name="kode-akun" class="form-control">
                            <option value=""></option>
                            @foreach($akun as $a)
                                <option value="{{$a->id}}">{{substr($a->kode_akun,1)}} &emsp; {{$a->nama_akun}}</option>
                                @php
                                    recursive($a);
                                @endphp
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="nominal">Nominal</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Nominal.." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="example-file-input">Input Bukti</label>
                    <div class="col-9">
                        <input type="file" id="example-file-input" name="bukti" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="example-textarea-input">Keterangan</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="keterangan" id="example-textarea-input" placeholder="Keterangan..">
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-alt-primary" id="yakin">Submit</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
    $(document).ready(function () {
        $("#internal").hide();
        $("#kode-akun").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#investor").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#nominal").on('keyup', function(evt){
            var 	number_string = $(this).val().replace(/[^,\d]/g, '').toString(),
            split	= number_string.split(','),
            sisa 	= split[0].length % 3,
            rupiah 	= split[0].substr(0, sisa),
            ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            $(this).val(rupiah) ;
        });
        $("#tipe-penerimaan").change(function(){
            if($("#tipe-penerimaan").val()==1){
                $("#kavling-group").show();
                $("#investor-group").hide();
                $('#internal').show();
            }
            else if($("#tipe-penerimaan").val()==2){
                $("#investor-group").show();
                $("#kavling-group").hide();
                $('#internal').show();
            }
            else if($("#tipe-penerimaan").val()==0){
                $("#investor-group").hide();
                $("#kavling-group").hide();
                $('#internal').hide();
            }
        });
        $('#yakin').click(function(){
            event.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menambah Transaksi? Mohon cek ulang data apabila belum yakin",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, submit!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    var harga = $("#nominal").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    if(isNaN(harga)){
                        harga = 0
                    }
                    $("#nominal").val(harga);
                    $('#add_trans').submit();
                }
                })
        });
        if($("#tipe-penerimaan").val()==1){
            $("#kavling-group").show();
            $("#investor-group").hide();
            $('#internal').show();
        }
        else if($("#tipe-penerimaan").val()==2){
            $("#investor-group").show();
            $("#kavling-group").hide();
            $('#internal').show();
        }
    });
</script>
<script src="{{ asset('js/getTagihan.js') }}"></script>
@endsection
