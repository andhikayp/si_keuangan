@extends('layouts.base.app')
@section('title', 'Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Pengeluaran</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }} </li>
        @endforeach
        </ul>  
    </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Transaksi Pengeluaran</h3>
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-transaksi'))
            <a href="{{url('/pengeluaran/form')}}">
            <button type="button" class="btn btn-sm btn-warning">
                <i class="fa fa-plus mr-2"></i>Tambah Pengeluaran
            </button></a>
            <br><br>
        @endif           
        <div class="table-responsive">                            
            <table id="table-pesan" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th>Pengeluaran</th>
                        <th>Kode Akun</th>
                        <th>Nominal</th>
                        <th>Oleh</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th style="width: 10%;">Option</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1
                    @endphp
                    @foreach ($keluar as $ke) 
                    <tr>
                        <td class="text-center">{{$no++}}</td>
                        <td>{{$ke->deskripsi}}</td>
                        <td>{{substr($ke->kode_akuns->kode_akun,1)}}. {{$ke->kode_akuns->nama_akun}}</td>
                        <td>Rp&nbsp;{{number_format($ke->jumlah,2,',','.')}}</td>
                        <td>{{$ke->penginput->name}}</td>
                        <td>{{ date('d-m-Y', strtotime($ke->tgl_transaksi)) }}</td>
                        <td>
                            @if($ke->status==PENDING)
                            <span class="badge badge-warning">Pending</span>
                            @elseif($ke->status==DISETUJUI)
                            <span class="badge badge-success">Acc</span>
                            @elseif($ke->status==DITOLAK)
                            <span class="badge badge-danger">Tolak</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{asset('storage/'.$ke->bukti)}}"><button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Detail Bukti">
                                <i class="fa fa-book"></i> Bukti
                            </button></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
</div>
@endsection
@section('moreJS')
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script> --}}
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('#table-pesan').DataTable({
                    "autoWidth": true,
                    "ordering": false,
                });
            });
        </script>
        @endsection