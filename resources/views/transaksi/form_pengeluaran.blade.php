@extends('layouts.base.app')
@section('title', ' Form Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
@php
function recursive($kode){
    foreach($kode->child as $anak){
        if(!is_null($anak)){
            echo('<option value="'.$anak->id.'">'.substr($anak->kode_akun,1).' &emsp;'.$anak->nama_akun.'</option>');
                recursive($anak);
        }
    }
}
@endphp
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pengeluaran')}}">Pengeluaran</a>
    <span class="breadcrumb-item active">Tambah Pengeluaran</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }} </li>
        @endforeach
        </ul>  
    </div>
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Pengeluaran</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <form action="{{ url('/tambah_pengeluaran') }}" method="post" enctype="multipart/form-data" id="add_trans">
            {{csrf_field()}}
            <div class="form-group row">
                <label class="col-12" for="tanggal">Tanggal</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="tanggal" name="tanggal" placeholder="Nama pengeluaran.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tipe-pengeluaran">Tipe Pengeluaran</label>
                <div class="col-md-9">
                    <select id="tipe-pengeluaran" name="tipe-pengeluaran" class="form-control">
                        <option value="0" disabled selected hidden>Tipe Pembayaran</option>
                        <option value="1">Pembayaran Pembatalan</option>
                        <option value="2">Investor Keluar</option>
                        <option value="3">Fee Marketing</option>
                        <option value="4">Pembayaran Lain-lain</option>
                    </select>
                </div>
            </div>
            <div id="internal" style="display:none">
                <div class="form-group row" id="pembatalan">
                    <label class="col-12" for="no-pembatalan">Kavling</label>
                    <div class="col-md-9">
                        <select id="no-pembatalan" name="no-pembatalan" class="form-control">
                            <option value=""></option>
                            @foreach($batal as $data)
                            <option value="{{$data->id}}">{{$data->pemesanans->kavlings->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="investor">
                    <label class="col-12" for="no-investor">Nama Investor | Total Setor | Tgl</label>
                    <div class="col-md-9">
                        <select id="no-investor" name="no-investor" class="form-control">
                            <option value=""></option>
                            @foreach($investor as $data)
                            <option value="{{$data->id}}">{{$data->users->name}} | {{number_format($data->jumlah_setor_modal,0,',','.')}} | {{date('d-m-Y', strtotime($data->tgl_perjanjian))}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="fee-marketing">
                    <label class="col-12" for="no-fee">Marketing</label>
                    <div class="col-md-9">
                        <select id="no-fee" name="no-fee" class="form-control">
                            <option value=""></option>
                            <option value="0">Lain-Lain</option>
                            @foreach($daftar as $data)
                            <option value="{{$data->id}}">{{$data->nama_marketing}} - {{$data->kavlings->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row" id="nama-keluar">
                    <label class="col-12" for="nama-proyek-input">Nama Pengeluaran</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="nama-proyek-input" name="nama-pengeluaran" placeholder="Nama pengeluaran.." required>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12">Kode Akun</label>
                    <div class="col-md-9">
                        <select id="kode-akun" name="kode-akun" class="form-control">
                            <option value=""></option>
                            @foreach ($tampil as $ta)
                            {{-- @if(!is_null($ta->parent)) --}}
                            <option value="{{$ta->id}}" style="background-color: white">{{substr($ta->kode_akun,1)}} &emsp; {{$ta->nama_akun}}</option>
                            @php
                                recursive($ta);
                            @endphp
                            {{-- @else
                            <option disabled style="background-color:grey;color:black;">{{substr($ta->kode_akun,1)}} &emsp; {{$ta->nama_akun}}</option> --}}
                            {{-- @endif --}}
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="nama-pemesan-input">Nominal</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Nominal.." required> 
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12" for="example-file-input">Input Bukti</label>
                    <div class="col-12">
                        <input type="file" id="example-file-input" name="bukti" required>
                    </div>
                </div>
                <br>
                <div class="form-group row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-alt-primary" id="yakin">Submit</button>
                    </div>
                </div>
            </div>
        </form>
        
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
$(document).ready(function () {
    $("#internal").hide();
    $("#pembatal").hide();
    $("#investor").hide();
    $('#fee-marketing').hide();
    $('#nama-keluar').hide();
    $("#kode-akun").select2({
        placeholder: "Please Select",
            width: '100%',
            style: 'resolve'
    });
    $("#nominal").on('keyup', function(evt){
        var 	number_string = $(this).val().replace(/[^,\d]/g, '').toString(),
        split	= number_string.split(','),
        sisa 	= split[0].length % 3,
        rupiah 	= split[0].substr(0, sisa),
        ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        $(this).val(rupiah) ;
    });
    $("#no-pembatalan").select2({
        placeholder: "Please Select",
            width: '100%'
    });
    $("#no-fee").select2({
        placeholder: "Please Select",
            width: '100%'
    });
    $("#no-investor").select2({
        placeholder: "Please Select",
            width: '100%'
    });
    $("#tipe-pengeluaran").change(function(){
        $("#internal").show();
        if($("#tipe-pengeluaran").val()==1){
            $("#pembatalan").show();
            $("#investor").hide();
            $('#fee-marketing').hide();
            $('#nama-keluar').hide();
        }
        else if($("#tipe-pengeluaran").val()==2){
            $("#pembatalan").hide();
            $("#investor").show();
            $('#fee-marketing').hide();
            $('#nama-keluar').hide();
        }
        else if($("#tipe-pengeluaran").val()==3){
            $("#pembatalan").hide();
            $("#investor").hide();
            $('#fee-marketing').show();
            $('#nama-keluar').hide();
        }
        else if($("#tipe-pengeluaran").val()==4){
            $("#pembatalan").hide();
            $("#investor").hide();
            $('#fee-marketing').hide();
            $('#nama-keluar').show();
        }
        else if($("#tipe-pengeluaran").val()==0){
            $("#internal").hide();
        }
    });
    $('#yakin').click(function(){
        event.preventDefault();
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Apakah anda yakin akan menambah Transaksi? Mohon cek ulang data apabila belum yakin",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, submit!',
            cancelButtonText: 'Tidak'
            }).then((result) => {
            if (result.value) {
                var harga = $("#nominal").val().toString().replace(/[^,\d]/g, '');
                harga = parseFloat(harga.replace(/\,/g,'.'));
                $("#nominal").val(harga);
                $('#add_trans').submit();
            }
        })
    });
});
</script>
@endsection