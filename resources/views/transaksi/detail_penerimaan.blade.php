@extends('layouts.base.app')
@section('title', ' Form Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan')}}">Pemesanan</a>
    <span class="breadcrumb-item active">Detail</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail Pemesanan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Proyek</p>
                        <h5>Proyek Ciputra</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Lokasi</p>
                        <h5>Jakarta</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>A-1</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Luas</p>
                        <h5>100 m<sup>2</sup></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>natalia@gmail.com</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pemesan</p>
                        <h5>Natalia</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat</p>
                        <h5>Jl. Kertajaya 69</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor KTP</p>
                        <h5>358011308870001</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Pribadi</p>
                        <h5>087666555444</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Kerabat</p>
                        <h5>084333222111</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Harga Deal</p>
                        <h5>Rp 100.000.000,-</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="dp-angsuran table-responsive col-sm-12">
                        <table class="table table-hover table-vcenter">
                            <tr>
                                <td> </td>
                                <td>UTJ</td>
                                <td>Rp  </td><td>10.000.000</td>
                                <td>Tanggal  </td><td>15 Agustus 2019</td>
                                <td><span class="badge badge-success">Telah dibayar</span></td>
                                <td>
                                    <a href="{{asset('img/bukti-exm.jpg')}}">
                                    <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail Pembayaran">
                                        <i class="fa fa-book"></i> Bukti
                                    </button>
                                    </a>
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" title="Terima Pembayaran">
                                        <i class="fa fa-check"></i> Terima
                                    </button>
                                    <button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Tolak Pembayaran">
                                        <i class="fa fa-times"></i> Tolak
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>DP1</td>
                                <td>Rp  </td><td>10.000.000</td>
                                <td>Tanggal  </td><td>15 Oktober 2019</td>
                                <td><span class="badge badge-warning">Belum dibayar</span></td>
                                <td>
                                <form class="form-inline" action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
                                    <input type="file" class="form-control-inline input-sm" id="utj" name="utj">
                                    <button type="submit" class="btn btn-sm btn-alt-primary">Submit</button>
                                </form>
                                </td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>DP2</td>
                                <td>Rp  </td><td>10.000.000</td>
                                <td>Tanggal  </td><td>15 Desember 2019</td>
                                <td><span class="badge badge-warning">Belum dibayar</span></td>
                                <td>
                                <!-- form -->
                                </td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>DP3</td>
                                <td>Rp  </td><td>10.000.000</td>
                                <td>Tanggal  </td><td>15 Februari 2020</td>
                                <td><span class="badge badge-warning">Belum dibayar</span></td>
                                <td>
                                <!-- form -->
                                </td>
                            </tr>
                            <tr>
                                <td> </td>
                                <td>Angsuran</td>
                                <td> </td>
                                <td>12 X Rp 5.000.000,-</td>
                                <td></td>
                                <td>
                                    <a href="{{url('angsuran/history')}}">
                                        <button type="button" class="btn btn-sm btn-primary">
                                            <i class="fa fa-book"></i> History Angsuran
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jatuh Tempo Setiap Tanggal</p>
                        <h5>15 Agustus</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Angsuran Pertama Tanggal</p>
                        <h5>15 Agustus</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Angsuran Terkahir Tanggal</p>
                        <h5>15 Agustus</h5>
                    </div>
                </div>    
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
    <!-- Top Modal -->
    <div class="modal fade" id="modal-top" tabindex="-1" role="dialog" aria-labelledby="modal-top" aria-hidden="true">
        <div class="modal-dialog modal-dialog-top" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Bukti Pembayaran UTJ</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <img src="{{asset('img/bukti-exm.jpg')}}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                        <i class="fa fa-check"></i> Perfect
                    </button>
                </div>
            </div>
        </div>
    </div>
        <!-- END Top Modal -->
</div>
@endsection
@section('moreJS')
<script src="jquery-1.11.2.min.js"></script>
<script src="select2.min.js"></script>
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#lokasi").select2({
            placeholder: "Please Select",
                width: '100%'
        });
    });
</script>
@endsection