@extends('layouts.base.app')
@section('title', ' Fee Marketing')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="#">Dashboard</a>
    <span class="breadcrumb-item active">Fee Marketing</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Fee Marketing</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
    <div class="block-content">
        @if (auth()->user()->can('create-feemarketing'))
            <a href="javascript:void(0)" disabled class="btn btn-sm btn-warning mb-3" data-toggle="modal" data-target="#modal-top2"><i class="fa fa-plus mr-2"></i>Tambah Data</a>
        @endif
        <div class="table-responsive">                            
            <table id="table-pesan" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center d-sm-table-cell">No</th>
                        <th class="d-sm-table-cell" style="width:7%">Kavling</th>
                        <th class="d-sm-table-cell">Nama Pemesan</th>
                        <th class="d-sm-table-cell">Marketing</th>
                        <th class="d-sm-table-cell">Total Fee</th>
                        <th class="d-sm-table-cell" style="width: 15%;">Diambil</th>
                        <th class="d-sm-table-cell" style="width: 15%;">Sisa</th>
                        <th class="d-sm-table-cell text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($pemesanan as $pemesanans)
                    <tr>
                        <td class="text-center">
                            {{$no}}
                            @php 
                                $no++;
                            @endphp
                        </td>
                        <td class="font-w600">
                            @php 
                                $kavling=App\kavling::find($pemesanans->kavlings_id);
                            @endphp
                            {{ $kavling->name }}
                        </td>
                        <td class="">{{ $pemesanans->buyer }}</td>
                        <td class="">
                            {{-- @php 
                                $nama_marketing=App\user::find($pemesanans->nama_marketing);
                            @endphp --}}
                            {{ $pemesanans->nama_marketing }}
                        </td>
                        <td class="">
                            Rp {{ number_format($pemesanans->fee_marketing,2,',','.') }}
                        </td>
                        <td>
                            Rp {{ number_format($pemesanans->fee_diambil,2,',','.') }}
                        </td>
                        <td>
                            @php
                                $sisa = $pemesanans->fee_marketing - $pemesanans->fee_diambil;
                            @endphp
                            Rp {{ number_format($sisa,2,',','.') }}
                        </td>
                        <td class="d-sm-table-cell text-center">
                            @if($sisa == 0)
                                <span class="btn btn-sm btn-success mb-2"><i class="fa fa-check"></i> Lunas</span>
                            @endif
                            <a href="{{url('fee-marketing/history/'.$pemesanans->id)}}" disabled class="btn btn-sm btn-secondary mb-2"><i class="si si-book-open mr-2"></i>History</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>         
    </div>

    <div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
            <form method="POST" action="{{ url('/tambah_fee') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Tambah Data</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <label class="col-12" for="nomor_kavling">Nomor Kavling - Nama Marketing</label>
                                <div class="col-md-12">
                                    <select id="nomor_kavling" name="nomor_kavling" class="form-control">
                                        <option value=""></option>
                                        @foreach($fee_marketing as $fee_marketings)
                                            <option value="{{$fee_marketings->id}}">
                                                {{$fee_marketings->name}} - {{$fee_marketings->nama_marketing}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Nama Pemesan</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="nama-pemesan-input" name="nama-pemesan-input" placeholder="Nama Pemesan" required> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Alamat</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="nama-pemesan-input" name="nama-pemesan-input" placeholder="Alamat" required> 
                                </div>
                            </div>           
                            <div class="form-group row">
                                <label class="col-12" for="nama-pemesan-input">Nama Marketing</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control" id="nama-pemesan-input" name="nama-pemesan-input" placeholder="Nama Marketing" required> 
                                </div>
                            </div> --}}
                            <div class="form-group row">
                                <label class="col-12" for="total_fee">Total Fee yang Diperoleh</label>
                                <div class="col-md-12">
                                    <input type="number" class="form-control" id="total_fee" name="total_fee" placeholder="Total Fee" required> 
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label class="col-12" for="example-file-input">Input Bukti</label>
                                <div class="col-md-12">
                                    <input type="file" id="bukti" name="bukti" required>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-alt-success">
                            <i class="fa fa-check"></i> Submit
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    </div>
</div>

@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('#table-pesan').DataTable({
                    "autoWidth": true,
                    "ordering": true,
                });
            });
            $("#nomor_kavling").select2({
                placeholder: "Pilih Nomor Kavling",
                width: '100%'
            });
        </script>
        @endsection