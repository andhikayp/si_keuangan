@extends('layouts.base.app')
@section('title', ' History Fee Marketing')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/fee-marketing')}}">Fee Marketing</a>
    <span class="breadcrumb-item active">History</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">History Fee Marketing</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    @php
        $detail_kavling = App\kavling::find($marketing->kavlings_id);
        $penginput = App\user::find($marketing->penginput_id);
        $sisa = $marketing->fee_marketing - $marketing->fee_diambil;
    @endphp
    <div class="block-content">
        @if($sisa != 0)
            <a href="javascript:void(0)" disabled class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#modal-top3"><i class="fa fa-money mr-2"></i>Bayar Marketing</a>
        @endif
        @if($sisa == 0)
                <span class="btn btn-sm btn-success mb-3"><i class="fa fa-check"></i> Lunas</span>
        @endif
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>{{ $detail_kavling->name }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pemesan</p>
                        <h5>{{ $marketing->buyer }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Tanggal Pemesanan</p>
                        <h5>{{ $marketing->tgl_pesan }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                        <div class="col-md-12">
                            <p style="margin-bottom:3px">Nama Marketing</p>
                            <h5>{{ $marketing->nama_marketing }}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p style="margin-bottom:3px">Diinput Oleh</p>
                            <h5>{{ $penginput->name }}</h5>
                        </div>
                    </div>
                    {{--<div class="row">
                        <div class="col-md-12">
                            <p style="margin-bottom:3px">Disetujui Oleh</p>
                            <h5>{{ $marketing->penyetujui_id }}</h5>
                        </div>
                    </div>
                 <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat Marketing</p>
                        <h5>{{ $detail_marketing->alamat }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Marketing</p>
                        <h5>{{ $detail_marketing->telp }}</h5>
                    </div>
                </div> --}}
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <p style="margin-bottom:3px">Total Fee</p>
                        <h5>
                            Rp. {{ number_format($marketing->fee_marketing,2,',','.') }}
                        </h5>
                    </div>
                    <div class="col-md-4">
                        <p style="margin-bottom:3px">Diambil</p>
                        <h5>
                            Rp. {{ number_format($marketing->fee_diambil,2,',','.') }}
                        </h5>
                    </div>
                    <div class="col-md-4">
                        <p style="margin-bottom:3px">Sisa</p>
                        <h5>
                            Rp. {{ number_format($sisa,2,',','.') }}
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="dp-angsuran table-responsive col-sm-12">
                        <table class="table table-striped table-vcenter">
                            <thead>
                                <th class="text-center d-sm-table-cell" style="width:20px"><b>No</b></th>
                                <th class="d-sm-table-cell"><b>Pembayar</b></th>
                                <th class="d-sm-table-cell"><b>Disetujui Oleh</b></th>
                                <th class="d-sm-table-cell"><b>Tanggal</b></th>
                                <th class="d-sm-table-cell text-right"><b>Nominal</b></th>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($transaksi as $transaksis)
                                <tr>
                                    <td class="text-center d-sm-table-cell">
                                        {{$no}}
                                        @php 
                                            $no++;
                                        @endphp
                                    </td>
                                    <td class="d-sm-table-cell">{{ $transaksis->penginput->name }}</td>
                                    <td class="d-sm-table-cell">{{ $transaksis->penginput->name }}</td>
                                    <td class="d-sm-table-cell">{{ $transaksis->tgl_transaksi }}</td>
                                    <td class="d-sm-table-cell text-right">
                                        Rp. {{ number_format($transaksis->jumlah,2,',','.') }}
                                    </td>
                                </tr>
                                @endforeach
                                <tr style="background-color: yellow">
                                    <td class="text-center d-sm-table-cell"></td>
                                    <td class="d-sm-table-cell"><b>Total</b></td>
                                    <td class="d-sm-table-cell"></td>
                                    <td class="d-sm-table-cell"></td>
                                    <td class="d-sm-table-cell text-right">
                                        <b>Rp. {{ number_format($marketing->fee_diambil,2,',','.') }}</b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
    <!-- Top Modal -->
    <div class="modal fade" id="modal-top3" tabindex="-1" role="dialog" aria-labelledby="modal-top3" aria-hidden="true">
        <div class="modal-dialog modal-dialog-top" role="document">
        <form method="POST" action="{{ url('/bayar_fee/') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
            <input type="text" class="form-control" id="id" name="id" value="{{$marketing->id}}" hidden> 
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Bayar Fee Marketing</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            <label class="col-12">Nomor Kavling - Nama Marketing</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="lol" name="lol" value="{{ $detail_kavling->name}} - {{ $marketing->nama_marketing }}" disabled required> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="nama-pemesan-input">Nominal</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="fee" name="fee" placeholder="Uang yang diberikan" required> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-file-input">Input Bukti</label>
                            <div class="col-md-12">
                                <input type="file" id="bukti" name="bukti" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-alt-success">
                        <i class="fa fa-check"></i> Submit
                    </button>
                </div>
            </div>
        </form>
        </div>
    </div>
    <!-- END Top Modal -->
</div>
@endsection