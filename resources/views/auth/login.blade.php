@extends('layouts.auth.app')
@section('title', 'Masuk')

@section('content')
<!-- Page Content -->
<div class="bg-image" style="background-image: url('{{ asset('img/tanah-kavling.jpg') }}');">
    <div class="row mx-0 bg-black-op">
        <div class="hero-static col-md-6 col-xl-8 d-none d-md-flex align-items-md-end">
            <div class="p-30 invisible" data-toggle="appear">
                <p class="font-size-h3 font-w600 text-white">
                    Selamat Datang di Sistem Informasi Developer ADAM JAYA PROPERTY
                </p>
                <p class="font-italic text-white-op">
                    Copyright &copy; SID ADAM JAYA PROPERTY <span class="js-year-copy"></span>
                </p>
            </div>
        </div>
        <div class="hero-static col-md-6 col-xl-4 d-flex align-items-center bg-white invisible" data-toggle="appear" data-class="animated fadeInRight">
            <div class="content content-full">
                <div class="px-30 py-10 text-center">
                    <img src="{{asset('img/logo.png')}}" alt="" width="50%" height="100%"><br>
                    <a class="link-effect font-w700" href="index.html">
                        <span class="font-size-xl text-primary-dark">SID</span> <span class="font-size-xl"> ADAM JAYA PROPERTY</span>
                    </a>
                </div>
                <!-- Header -->
                <div class="px-30 py-10">
                    
                    {{-- <h1 class="h3 font-w700 mt-30 mb-10">Selamat Datang di SID ADAM JAYA</h1> --}}
                    <h2 class="h5 font-w400 text-muted mb-0">Masuk terlebih dahulu</h2>
                </div>
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                          {{ $error }}
                        </div>
                    @endforeach  
                @endif
                <form method="POST" action="{{ url('/login') }}" class="js-validation-signin px-30">
                    @csrf
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-material form-material-primary floating">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                <label for="email">{{ __('Alamat Email') }}</label>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material form-material-primary floating">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                <label for="password">{{ __('Password') }}</label>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div> --}}
                    <div class="form-group">
                            <button type="submit" class="btn btn-sm btn-hero btn-alt-primary">
                                <i class="si si-login mr-10"></i>{{ __('Login') }}
                            </button>

                            {{-- @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif --}}
                    </div>
                </form>
                <!-- END Sign In Form -->
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection