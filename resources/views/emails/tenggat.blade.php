<style>
    td {
        height: 25px;
        vertical-align: bottom;
    }
    .bold{
        font-weight: bold;
    }
    .center{
        text-align: center;
    }
    div {
        line-height: 1.6;
    }
</style>
<div class="bold center">
    <strong>SURAT PEMBAYARAN MEMASUKI TENGGAT</strong>
</div>
<div>
<p>Kepada Yth. <strong>{{ $detail->buyer }}</strong>,<br>
{{$detail->email}},<br>
{{$detail->alamat}},<br>
{{$detail->hp_pribadi}},</p>
<br>
<p>{!!str_replace('"','',nl2br(e($konfig->isi_tenggat)))!!}</p>
<table border="0">
    <tr>
        @if($bayar == 'DP2')
        <td>DP ke-2</td><td>:</td>
        <td>Rp {{number_format($detail->dp2,2,',','.')}}</td>
    </tr>
    <tr>
        <td>Tanggal</td><td>:</td>
        <td>{{\Carbon\Carbon::parse($detail->dp2_tgl)->format('l, d F Y')}}</td>
        @elseif($bayar == 'DP3')
        <td>DP ke-3</td><td>:</td>
        <td>Rp {{number_format($detail->dp3,2,',','.')}}</td>
    </tr>
    <tr>
        <td>Tanggal</td><td>:</td>
        <td>{{\Carbon\Carbon::parse($detail->dp3_tgl)->format('l, d F Y')}}</td>
        @elseif($bayar == 'Angsuran')
        <td>Angsuran</td><td>:</td>
        <td>Rp {{number_format($detail->angsur,2,',','.')}}</td>
    </tr>
    <tr>
        <td>Tanggal</td><td>:</td>
        <td>{{\Carbon\Carbon::tomorrow()->format('l, d F Y')}}</td>
        @endif
    </tr>
</table>

<p>Terimakasih,</p>
<p>Adam Jaya</p>
<p>{{$konfig->email}}</p>
</div>