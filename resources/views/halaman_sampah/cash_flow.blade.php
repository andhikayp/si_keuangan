{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Cashflow')

@section('content_header')
@stop

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header text-center">
        <h2>Laporan Arus Kas</h2>
      </div>

      <div class="box-body table-responsive">
        <div class="col-md-offset-2 col-md-8">
            <table class="table">
                <tbody>
                    <tr>
                        <th colspan="1"></th>
                        <td class="text-right">Saldo</td>
                    </tr>

                    <?php
                        $total_operasional = 0;
                        $total_investasi = 0;
                        $total_pendanaan = 0;
                        $kas_awal = 0;
                        $kas_akhir = 0;
                        $revaluasi_bank = 0;
                    ?>

                    <!-- Operational -->
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9">Transaksi Operasional</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Net Income</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['penerimaan_pelanggan'], $total_operasional+=$operating_activities['penerimaan_pelanggan'] }}</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Depreciation Expense</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['depreciation_expenses'], $total_operasional+=$operating_activities['depreciation_expenses'] }}</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Loss on sale of equipment</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['loss_on_sale_of_equipment'], $total_operasional+= $operating_activities['loss_on_sale_of_equipment']}} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Decrease in account receivable</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['decrease_in_account_receivable'], $total_operasional+=$operating_activities['decrease_in_account_receivable'] }} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in Inventory</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['increase_in_inventory'][0]->total_credit-$operating_activities['increase_in_inventory'][0]->total_debit, $total_operasional+= $operating_activities['increase_in_inventory'][0]->total_credit-$operating_activities['increase_in_inventory'][0]->total_debit }} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in prepaid expenses</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['increase_in_prepaid_expenses'], $total_operasional+=$operating_activities['increase_in_prepaid_expenses'] }} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in accounts payable</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['increase_in_account_payable'], $total_operasional+=$operating_activities['increase_in_account_payable']}} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in income taxes payable</td>
                            <th colspan="1" class="text-right">Rp {{ $operating_activities['decrease_in_income_taxes_payable'], $total_operasional+=$operating_activities['decrease_in_income_taxes_payable'] }} </th>
                        </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Transaksi Operasional</td>
                        <th colspan="1" style="background-color: #d9d9d9" class="text-right">Rp {{ $total_operasional }}</th>
                    </tr>
                    <!-- End Operational -->

                    <!-- Investment -->
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9">Transaksi Investasi</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Purchase of building</td>
                            <th colspan="1" class="text-right">Rp 0 </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Sale of building</td>
                            <th colspan="1" class="text-right">Rp 0 </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Purchase of equipment</td>
                            <th colspan="1" class="text-right">Rp {{ $investing_activities['purchase_of_equipment'][0]->total_debit, $total_investasi -=  $investing_activities['purchase_of_equipment'][0]->total_debit}} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Sale of equipment</td>
                            <th colspan="1" class="text-right">Rp 0 </th>
                        </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Transaksi Investasi</td>
                        <th colspan="1" style="background-color: #d9d9d9" class="text-right">Rp {{ $total_investasi }}</th>
                    </tr>
                    <!-- end Investment -->

                    <!-- Financing -->

                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9" >Transaksi Pendanaan</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase of Capital</td>
                            <th colspan="1" class="text-right">Rp {{ $financing_activities['increase_of_capital'][0]->total_credit, $total_pendanaan +=  $financing_activities['increase_of_capital'][0]->total_credit}} </th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Capital Withdrawal</td>
                            <th colspan="1" class="text-right">Rp {{ $financing_activities['capital_withdrawal'], $total_pendanaan += $financing_activities['capital_withdrawal'] }} </th>
                        </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Transaksi Pendanaan</td>
                        <th colspan="1" style="background-color: #d9d9d9" class="text-right">Rp {{$total_pendanaan}} </th>
                    </tr>
                    <!-- End Financing -->

                    <!-- Konklusi-->

                    <?php $kas_akhir += $total_operasional + $total_investasi + $total_pendanaan; ?>

                    <br>

                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Kenaikan (penurunan) Kas</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp {{ $kas_akhir - $kas_awal }}
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Total revaluasi bank</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp {{ $revaluasi_bank }}
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Saldo Awal Kas</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp {{ $kas_awal }}
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Saldo Akhir Kas</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp {{ $kas_akhir }}
                        </th>
                    </tr>
                    <!-- end Konklusi -->

                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
  console.log('Hi!');
</script>
@stop