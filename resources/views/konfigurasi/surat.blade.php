@extends('layouts.base.app')
@section('title', ' Konfigurasi Surat')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Konfigurasi Surat</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Konfigurasi Surat</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
    <div class="block">

    <div class="block">
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#btabs-alt-static-home">Tunggakan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#btabs-alt-static-profile">Peringatan</a>
            </li>
        </ul>
        <div class="block-content tab-content">
            <br> 
            <div class="tab-pane active" id="btabs-alt-static-home" role="tabpanel">
                <h6 class="font-w400" >
                    <form action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
                        <div class="form-group row">
                            <label class="col-12" for="example-text-input-valid">Kota Tempat Surat dibuat</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid" placeholder="Tempat..">
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-12" for="example-text-input-valid">Tanggal</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-disabled-input">Perihal</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="example-disabled-input" name="example-disabled-input" placeholder="Pemberitahuan Tunggakan" disabled>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-12" for="example-text-input-valid">Lampiran</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-disabled-input">Tanggal jatuh Tempo</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" id="example-disabled-input" name="example-disabled-input" placeholder="Otomatis" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-12" for="example-textarea-input">Isi Surat</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="6" placeholder="Isi.."></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </h6>
                
            </div>
            <div class="tab-pane" id="btabs-alt-static-profile" role="tabpanel">
                <h6 class="font-w400" >
                    <form action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
                        <div class="form-group row">
                            <label class="col-12" for="example-text-input-valid">Kota Tempat Surat dibuat</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid" placeholder="Tempat..">
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-12" for="example-text-input-valid">Tanggal</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-disabled-input">Perihal</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="example-disabled-input" name="example-disabled-input" placeholder="Pemberitahuan Jatuh Tempo" disabled>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-12" for="example-text-input-valid">Lampiran</label>
                            <div class="col-md-9">
                                <input type="number" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="example-disabled-input">Tanggal jatuh Tempo</label>
                            <div class="col-md-9">
                                <input type="date" class="form-control" id="example-disabled-input" name="example-disabled-input" placeholder="Otomatis" disabled>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-12" for="example-textarea-input">Isi Surat</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="6" placeholder="Isi.."></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </h6>
            </div>
            <div class="tab-pane" id="btabs-alt-static-settings" role="tabpanel">
                <h4 class="font-w400">Settings Content</h4>
                <p>...</p>
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#lokasi").select2({
            placeholder: "Please Select",
                width: '100%'
        });
    });
</script>
@endsection