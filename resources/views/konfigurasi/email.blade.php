@extends('layouts.base.app')
@section('title', ' Konfigurasi Email')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item active" >Konfigurasi Email</a>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Konfigurasi Email</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class="block">
            <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#tunggakan">Tunggakan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#peringatan">Peringatan</a>
                </li>
            </ul>
            <div class="block-content tab-content">
                <div class="tab-pane active" id="tunggakan" role="tabpanel">
                    <form action="{{url('/konfigtgt')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-12" for="email">Email Perusahaan</label>
                            <div class="col-md-9">
                                <input type="email" class="form-control" id="email-tgt" name="email" placeholder="Email..">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-12" for="subjek">Subjek</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="subjek-tgt" name="subjek" placeholder="Subjek..">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-12" for="isi">Isi Email</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="isi-tgt" name="isi" rows="6" placeholder="Isi.."></textarea>
                            </div>
                            {{-- {!!nl2br(e($konfigurasi->isi_tenggat))!!} --}}
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="peringatan" role="tabpanel">
                    <form action="{{url('/konfigpgt')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group row">
                            <label class="col-12" for="email">Email Perusahaan</label>
                            <div class="col-md-9">
                                <input type="email" class="form-control" id="email-pgt" name="email" placeholder="Email..">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-12" for="subjek">Subjek</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="subjek-pgt" name="subjek" placeholder="Subjek..">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-12" for="isi">Isi Email</label>
                            <div class="col-md-9">
                                <textarea class="form-control" id="isi-pgt" name="isi" rows="6" placeholder="Isi Peringatan.."></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-alt-primary">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#lokasi").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        @if(!is_null($konfigurasi))
        $("#email-pgt").val('{{$konfigurasi->email}}');
        $("#email-tgt").val('{{$konfigurasi->email}}');
        $("#isi-tgt").val("{{json_encode($konfigurasi->isi_tenggat)}}");
        $("#subjek-tgt").val('{{$konfigurasi->subjek_tenggat}}');
        $("#isi-pgt").val('{{json_encode($konfigurasi->isi_peringatan)}}');
        $("#subjek-pgt").val('{{$konfigurasi->subjek_peringatan}}');
        @endif
    });
</script>
@endsection