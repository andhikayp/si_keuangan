@extends('layouts.base.app')
@section('title', ' Form Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan')}}">Konfigurasi SMS</a>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Konfigurasi SMS</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
    <div class="block">
            <div class="block-content">
                <form action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
                    <div class="form-group row">
                        <label class="col-12" for="example-text-input-valid">Nomor Telfon Perusahaan</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control is-valid" id="example-text-input-valid" name="example-text-input-valid" placeholder="Nomor telfon..">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-12" for="example-textarea-input">Pesan</label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="example-textarea-input" name="example-textarea-input" rows="6" placeholder="Pesan.."></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <button type="submit" class="btn btn-alt-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
                width: '100%'
        });
        $("#lokasi").select2({
            placeholder: "Please Select",
                width: '100%'
        });
    });
</script>
@endsection