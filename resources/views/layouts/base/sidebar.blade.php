<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                    <span class="text-dual-primary-dark">R</span><span class="text-primary">D</span>
                </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout"
                    data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="link-effect font-w700" href="#">
                        <i class="fa fa-database text-primary"></i>
                        <span class="font-size-xl text-dual-primary-dark">   SID</span> <span class="font-size-xl text-primary">ADAM JAYA</span>
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src=""
                    alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="#">
                    <img class="img-avatar" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle"
                            href="javascript:void(0)">
                            <i class="si si-drop"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="{{url('/pilih-proyek')}}">
                            <i class="si si-logout mr-2"></i> Ganti Proyek
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">
            <ul class="nav-main">
                <li>
                    <a href="{{url('home')}}" class="">
                        <i class="fa fa-home"></i><span class="sidebar-mini-hide">Dashboard</span>
                    </a>
                </li>
                {{-- <li>
                    <a href="" class="">
                        <i class="fa fa-bar-chart "></i><span class="sidebar-mini-hide">Statistik</span>
                    </a>
                </li> --}}
                @if (auth()->user()->can('approval-kuitansi') || auth()->user()->can('approval-ppjb') || auth()->user()->can('approval-pembatalan'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Approval</span></li>
                    @if (auth()->user()->can('approval-kuitansi'))
                        <li>
                            <a href="{{url('approval_kuitansi')}}" class="">
                                <i class="fa fa-newspaper-o"></i><span class="sidebar-mini-hide">Kuitansi</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->can('approval-ppjb'))
                        <li>
                            <a href="{{url('approval_surat')}}" class="">
                                <i class="fa fa-file"></i><span class="sidebar-mini-hide">PPJB</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->can('approval-pembatalan'))
                        <li>
                            <a href="{{url('approval_pembatalan')}}" class="">
                                <i class="fa fa-close"></i><span class="sidebar-mini-hide">Pembatalan</span>
                            </a>
                        </li>
                    @endif
                @endif
                
                @if (auth()->user()->can('view-kodeakun') || auth()->user()->can('view-datakavling'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Master Data</span></li>
                    @if (auth()->user()->can('view-kodeakun'))
                        <li>
                            <a href="{{url('kode-akun')}}" class="">
                                <i class="fa fa-list-alt"></i><span class="sidebar-mini-hide">Kode Akun</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->can('view-datakavling'))
                        <li>
                            <a href="{{url('data-kavling')}}" class="">
                                <i class="fa fa-database"></i><span class="sidebar-mini-hide">Data Kavling</span>
                            </a>
                        </li>
                    @endif
                @endif
                
                @if (auth()->user()->can('view-transaksi') || auth()->user()->can('view-pembatalan') || auth()->user()->can('view-pemesanan'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Transaksi</span></li>
                    @if (auth()->user()->can('view-transaksi'))
                        <li>
                            <a href="{{ url('penerimaan') }}" class="">
                                <i class="fa fa-arrow-circle-o-down"></i><span class="sidebar-mini-hide">Penerimaan</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('pengeluaran') }}" class="">
                                <i class="fa fa-arrow-circle-o-up"></i><span class="sidebar-mini-hide">Pengeluaran</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->can('view-pemesanan'))
                        <li>
                            <a href="{{url('pemesanan')}}" class="">
                                <i class="fa fa-book"></i><span class="sidebar-mini-hide">Pemesanan</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->can('view-pembatalan'))
                        <li>
                            <a href="#" class="nav-submenu" data-toggle="nav-submenu">
                                <i class="fa fa-calendar-times-o"></i>
                                <span class="sidebar-mini-hide">Pembatalan</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="{{url('pembatalan_pesan')}}">Batal Pesan</a>
                                </li>
                                <li>
                                    <a href="{{url('pembatalan_beli')}}">Batal Beli</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                @endif
                
                @if (auth()->user()->can('view-laporan'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Laporan</span></li>
                    <li>
                        <a href="{{ url('keuangan/penerimaan') }}" class="">
                            <i class="fa fa-chevron-circle-right"></i><span class="sidebar-mini-hide">Penerimaan</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('keuangan/pengeluaran') }}" class="">
                            <i class="fa fa-chevron-circle-left"></i><span class="sidebar-mini-hide">Pengeluaran</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('keuangan/laba_rugi') }}" class="">
                            <i class="fa fa-line-chart"></i><span class="sidebar-mini-hide">Laba Rugi</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('keuangan/neraca') }}" class="">
                            <i class="fa fa-balance-scale"></i><span class="sidebar-mini-hide">Neraca</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('keuangan/arus_kas') }}" class="">
                            <i class="fa fa-money"></i><span class="sidebar-mini-hide">Arus Kas</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->can('view-feemarketing') || auth()->user()->can('view-siteplan'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Marketing</span></li>
                    @if (auth()->user()->can('view-feemarketing'))
                        <li>
                            <a href="{{ url('fee-marketing') }}" class="">
                                <i class="fa fa-tag"></i><span class="sidebar-mini-hide">Fee Marketing</span>
                            </a>
                        </li>
                    @endif
                    @if (auth()->user()->can('view-siteplan'))
                        <li>
                            <a href="{{ url('site-plan') }}" class="">
                                <i class="fa fa-map"></i><span class="sidebar-mini-hide">Site Plan</span>
                            </a>
                        </li>
                    @endif
                @endif
                
                @if (auth()->user()->can('view-investor'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Investor</span></li>                
                    <li>
                        <a href="{{ url('investor') }}" class="">
                            <i class="fa fa-handshake-o"></i><span class="sidebar-mini-hide">Investor</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->can('view-user'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">User</span></li>
                    <li>
                        <a href="{{url('user')}}" class="">
                            <i class="fa fa-user"></i><span class="sidebar-mini-hide">Manajemen User</span>
                        </a>
                    </li>
                @endif

                @if (auth()->user()->can('config-email'))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Konfigurasi</span></li>
                    <li>
                        <a href="{{ url('konfigurasi_email') }}" class="">
                            <i class="fa fa-at"></i><span class="sidebar-mini-hide">Email</span>
                        </a>
                    </li>  
                @endif

                @role('Komisaris')
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Roles and Permissions</span></li>
                    <li>
                        <a href="{{ route('role.index') }}" class="">
                            <i class="fa fa-circle-o"></i><span class="sidebar-mini-hide">Manajemen Role</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('users.roles_permission') }}" class="">
                            <i class="fa fa-circle-o"></i><span class="sidebar-mini-hide">Manajemen Permissions</span>
                        </a>
                    </li>
                @else
                    @if (auth()->user()->can('manage-rolepermission'))
                        <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Roles and Permissions</span></li>
                        <li>
                            <a href="{{ route('role.index') }}" class="">
                                <i class="fa fa-circle-o"></i><span class="sidebar-mini-hide">Manajemen Role</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.roles_permission') }}" class="">
                                <i class="fa fa-circle-o"></i><span class="sidebar-mini-hide">Manajemen Permissions</span>
                            </a>
                        </li>
                    @endif
                @endrole
                
                {{-- <li>
                        <a href="{{ url('konfigurasi_surat') }}" class="">
                            <i class="si si-envelope"></i><span class="sidebar-mini-hide">Surat</span>
                        </a>
                    </li> --}}
                {{--<li>
                        <a href="{{ url('konfigurasi_sms') }}" class="">
                            <i class="fa fa-mobile-phone"></i><span class="sidebar-mini-hide">SMS</span>
                        </a>
                    </li>--}}
                {{-- <li>
                    <a href="{{url('pemesanan')}}" class="">
                        <i class="fa fa-book"></i><span class="sidebar-mini-hide">Pemesanan</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="nav-submenu" data-toggle="nav-submenu">
                        <i class="fa fa-calendar-times-o"></i>
                        <span class="sidebar-mini-hide">Pembatalan</span>
                    </a>
                    <ul>
                        <li>
                            <a href="{{url('pembatalan_pesan')}}">Batal Pesan</a>
                        </li>
                        <li>
                            <a href="{{url('pembatalan_beli')}}">Batal Beli</a>
                        </li>
                    </ul>
                </li> --}}
                {{-- <li>
                    <a href="{{ url('progress') }}" class="">
                        <i class="fa fa-area-chart"></i><span class="sidebar-mini-hide">Progress Penjualan</span>
                    </a>
                </li> --}}
                {{-- <li>
                    <a href="" class="">
                        <i class="fa fa-wrench"></i><span class="sidebar-mini-hide">Menu</span>
                    </a>
                </li> --}}
                {{-- <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Laporan</span></li>
                <li>
                    <a href="" class="">
                        <i class="fa fa-download"></i><span class="sidebar-mini-hide">Menu</span>
                    </a>
                </li> --}}
            </ul>
        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
</nav>
<!-- END Sidebar -->