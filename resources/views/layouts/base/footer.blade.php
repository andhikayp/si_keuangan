<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        <div class="float-left">
            <a class="font-w600" href="#" target="_blank">SID ADAM JAYA PROPERTY </a> &copy; <span class="js-year-copy"></span>
        </div>
    </div>
</footer>