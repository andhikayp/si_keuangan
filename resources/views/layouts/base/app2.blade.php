
<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>ADAM JAYA | @yield('title')</title>

        <meta name="description" content="Adam Jaya - Aplikasi Keuangan Adam Jaya">
        <meta name="author" content="Adam Jaya Inc.">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Adam Jaya - Aplikasi Keuangan Adam Jaya">
        <meta property="og:site_name" content="Adam Jaya">
        <meta property="og:description" content="Adam Jaya - Aplikasi Keuangan Adam Jaya &amp; created by Adam Jaya Inc.">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{asset('img/logo.png')}}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/logo.png')}}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/logo.png')}}">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{ asset('codebase/src/assets/css/codebase.min.css')}}">
        <link rel="stylesheet" id="css-main" href="{{ asset('codebase/src/assets/css/themes/corporate.min.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    </head>
    <body>

        <div id="page-container" class="enable-page-overlay side-scroll page-header-classic main-content-boxed">

            @yield('sidebar')

            @yield('header')

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    @yield('content')
                </div>
                <!-- END Page Content -->

            </main>
            <!-- END Main Container -->

            @include('layouts.base.footer')
        </div>
        <!-- END Page Container -->
        <script src="{{ asset('codebase/src/assets/js/codebase.core.min.js')}}"></script>
        <script src="{{ asset('codebase/src/assets/js/codebase.app.min.js')}}"></script>
        <!-- Page JS Plugins -->
        <script src="{{ asset('codebase/src/assets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
        <!-- Page JS Code -->
        <script src="{{ asset('codebase/src/assets/js/pages/op_auth_signin.min.js')}}"></script>
        {{-- <script src="{{ asset('js/be_forms_validation.js')}}"></script>
        <script src="{{ asset('js/plugins/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset('js/codebase.core.min.js')}}"></script>
        <script src="{{ asset('js/codebase.app.min.js')}}"></script> --}}
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
        @yield('moreJS')
    </body>
</html>