<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>@yield('title')</title>

        <meta name="description" content="Adam Jaya - Aplikasi Keuangan Adam Jaya">
        <meta name="author" content="Adam Jaya Inc.">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Adam Jaya - Aplikasi Keuangan Adam Jaya">
        <meta property="og:site_name" content="Adam Jaya">
        <meta property="og:description" content="Adam Jaya - Aplikasi Keuangan Adam Jaya &amp; created by Adam Jaya Inc.">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="">
        <link rel="icon" type="image/png" sizes="192x192" href="">
        <link rel="apple-touch-icon" sizes="180x180" href="">
        <!-- END Icons -->

        <!-- Stylesheets -->

        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" id="css-main" href="{{ asset('codebase/src/assets/css/codebase.min.css') }}">
    </head>

    <body>
        <div id="page-container" class="main-content-boxed">

            <!-- Main Container -->
            <main id="main-container">

                @yield('content')
                
            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
        <script src="{{ asset('codebase/src/assets/js/codebase.core.min.js') }}"></script>
        <script src="{{ asset('codebase/src/assets/js/codebase.app.min.js') }}"></script>
        <!-- Page JS Plugins -->
        <script src="{{ asset('codebase/src/assets/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
        <!-- Page JS Code -->
        <script src="{{ asset('js/op_auth_signin.js') }}"></script>
    </body>
</html>