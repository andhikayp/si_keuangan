@extends('layouts.base.app')
@section('title', 'Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Pemesanan</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Pemesanan Kavling</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-pemesanan'))
            <a href="{{url('/pemesanan/form')}}">
            <button type="button" class="btn btn-sm btn-warning">
                <i class="fa fa-plus mr-2"></i>Tambah Pemesanan
            </button></a>
            <br><br>
        @endif
        <div class="table-responsive">                            
            <table id="table-pesan" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center d-sm-table-cell" style="width: 10px;"></th>
                        <th class="d-sm-table-cell">Kavling</th>
                        <th class="d-sm-table-cell">Pemesan</th>
                        <th class="d-sm-table-cell">Tanggal</th>
                        <th style="width: 15%;">Status</th>
                        <th class="text-center d-sm-table-cell">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $no = 1;    
                    @endphp
                    @foreach($daftar as $pesan)
                    <tr>
                        <td class="text-center">{{$no++}}</td>
                        <td>{{$pesan->kavlings->name}}</td>
                        <td>{{$pesan->buyer}}</td>
                        <td>{{$pesan->tgl_pesan}} </td>
                        <td>
                            @if($pesan->status_pembatalan==BATAL_PESAN)
                                <span class="badge badge-danger">Batal Pesan</span>
                            @elseif($pesan->status_pembatalan==BATAL_BELI)
                                <span class="badge badge-danger">Batal Beli</span>
                            @elseif($pesan->status==PENDING)
                                <span class="badge badge-warning">Pending</span>
                            @elseif($pesan->status==UTJ)
                                <span class="badge badge-warning">UTJ</span>
                            @elseif($pesan->status==DP1)
                                <span class="badge badge-primary">DP1</span>
                            @elseif($pesan->status==DP2)
                                <span class="badge badge-primary">DP2</span>
                            @elseif($pesan->status==DP3)
                                <span class="badge badge-primary">DP3</span>
                            @elseif($pesan->status==Angsur)
                                <span class="badge badge-primary">Mengangsur</span>
                            @elseif($pesan->status==Lunas)
                                <span class="badge badge-success">Lunas</span>
                            @endif
                            @if($pesan->status_ppjb==PENDING && !is_null($pesan->ppjb))
                                <span class="badge badge-danger">PPJB belum disetujui</span>
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="{{url('/pemesanan/detail/'.$pesan->id)}}"><button type="button" class="btn btn-sm btn-secondary mb-2" data-toggle="tooltip" title="Detail Pesanan">
                                <i class="fa fa-book"></i> Detail
                            </button></a>
                            {{-- <a href="{{url('angsuran/form')}}"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Angsur Pesanan">
                                <i class="fa fa-money"></i> Angsur
                            </button></a> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
</div>
@endsection
@section('moreJS')
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
        {{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
        <script>
            $(document).ready(function(){
                $('#table-pesan').DataTable({
                    "autoWidth": true,
                    "ordering": false,
                });
            });
        </script>
        @endsection