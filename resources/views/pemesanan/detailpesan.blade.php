@extends('layouts.base.app')
@section('title', ' Detail Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan')}}">Pemesanan</a>
    <span class="breadcrumb-item active">Detail</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail Pemesanan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama</p>
                        <h5>{{$data->buyer}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>{{$data->email}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>{{$data->kavlings->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Tanggal Pemesanan</p>
                        <h5>{{\Carbon\Carbon::parse($data->tgl_pesan)->format('d F Y')}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Diinput Oleh</p>
                        <h5>{{$data->penginput->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Disetujui Oleh</p>
                        @if($data->penyetujui_id!=null)
                        <h5>{{$data->penyetujui->name}}</h5>
                        @else
                        <h5 class="text-warning">Belum disetujui</h5>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat</p>
                        <h5>{{$data->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor KTP</p>
                        <h5>{{$data->no_ktp}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Pribadi</p>
                        <h5>{{$data->hp_pribadi}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Kerabat</p>
                        <h5>{{$data->hp_kerabat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Marketing</p>
                        @if ($data->marketing)
                            <h5>{{$data->marketing}}</h5>
                        @else
                            <h5 class="text-warning">Tanpa Marketing</h5>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-bottom:8px">
                        <p style="margin-bottom:3px">PPJB</p>
                        @if($data->ppjb!=null)
                        <a href="{{ url('/pemesanan/detail/'.$data->ppjb) }}" target="_blank">
                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail PPJB">
                            <i class="fa fa-book"></i> PPJB
                        </button>
                        </a>
                        @endif
                    </div>
                    @role('Admin|Manajer|Komisaris')
                    <div class="col-md-12" style="margin-bottom:8px">
                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-ppjb-tambah" title="Tambah PPJB">
                            <i class="fa fa-plus"></i> Tambah PPJB
                        </button>
                    </div>
                    <div class="modal fade" id="modal-ppjb-tambah" tabindex="-1" role="dialog" aria-labelledby="modal-ppjb-tambah" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-top" role="document">
                        <form class="form" action="{{url('/ppjb')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input value="{{$data->id}}" type="text" class="form-control" id="id" name="id" hidden>
                            <div class="modal-content">
                                <div class="block block-themed block-transparent mb-0">
                                    <div class="block-header bg-primary-dark">
                                        <h3 class="block-title">Upload PPJB</h3>
                                        <div class="block-options">
                                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                                <i class="si si-close"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="block-content">
                                        <div class="form-group row">
                                            <label for="upload" class="col-12">Pilih File</label>
                                            <div class="col-md-12">
                                                <input type="hidden" value="{{$data->id}}" name="pemesanan">
                                                <input type="file" class="form-control mb-2 mr-sm-2 mb-sm-0 " name="ppjb" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-alt-success">
                                        <i class="fa fa-check"></i> Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    @endrole
                    <div class="col-md-12" style="margin-bottom:8px">
                        <a href="javascript:void(0)" disabled data-toggle="modal" data-target="#modal-top"></i>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail Pembayaran">
                                <i class="fa fa-print"></i> Print Surat PPJB
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jatuh Tempo Setiap Tanggal</p>
                        <h5>{{$data->tgl_tempo}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Angsuran Pertama Tanggal</p>
                        <h5>{{$data->tgl_angsur1}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Angsuran Terkahir Tanggal</p>
                        <h5>{{$data->tgl_angsur2}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Harga PPJB</p>
                        <h5>Rp. {{ number_format($data->harga_deal,2,',','.') }}</h5>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h5 style="text-align:center">History UTJ dan DP</h5>
                    </div>
                    <div class="table-responsive">
                        <table class="stripe table table-stripped text-center">
                            <thead>
                                <tr>
                                    <th>Jenis</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal Pembayaran</th>
                                    <th>Status</th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>UTJ</td>
                                    <td>Rp. {{ number_format($data->utj,2,',','.') }}</td>
                                    <td>{{$data->utj_tgl}}</td>
                                    <td>
                                        @if($data->status<UTJ)
                                        <span class="badge badge-warning">Belum Dibayar</span>
                                    </td>
                                    <td></td>
                                        @else
                                        <span class="badge badge-success">Telah Dibayar</span>
                                    </td>
                                    <td>
                                        @if(!is_null($utj))
                                        <a href="{{url('kwitansi/'.$utj->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                            <i class="fa fa-book"></i> Kwitansi
                                        </button></a>
                                        <a href="{{asset('storage/'.$utj->bukti)}}">
                                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail Pembayaran">
                                            <i class="fa fa-book"></i> Bukti
                                        </button>
                                        </a>
                                        @endif
                                    </td>
                                        @endif
                                </tr>
                                <tr>
                                    <td>DP1</td>
                                    <td>Rp. {{ number_format($data->dp1,2,',','.') }}</td>
                                    <td>{{$data->dp1_tgl}}</td>
                                    <td>
                                        @if($data->status<DP1)
                                        <span class="badge badge-warning">Belum Dibayar</span>
                                    </td>
                                    <td></td>
                                        @else
                                        <span class="badge badge-success">Telah Dibayar</span>
                                    </td>
                                    <td>
                                        @if(isset($dp[0]->bukti))
                                        <a href="{{url('kwitansi/'.$dp[0]->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                            <i class="fa fa-book"></i> Kwitansi
                                        </button></a>
                                        <a href="{{asset('storage/'.$dp[0]->bukti)}}">
                                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail Pembayaran">
                                            <i class="fa fa-book"></i> Bukti
                                        </button>
                                        </a>
                                        @endif
                                    </td>
                                        @endif
                                </tr>
                                <tr>
                                    <td>DP2</td>
                                    <td>Rp. {{ number_format($data->dp2,2,',','.') }}</td>
                                    <td>{{$data->dp2_tgl}}</td>
                                    <td>
                                        @if($data->status<DP2)
                                        <span class="badge badge-warning">Belum Dibayar</span>
                                    </td>
                                    <td></td>
                                        @else
                                        <span class="badge badge-success">Telah Dibayar</span>
                                    </td>
                                    <td>
                                        @if(isset($dp[1]->bukti))
                                        <a href="{{url('kwitansi/'.$dp[1]->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                            <i class="fa fa-book"></i> Kwitansi
                                        </button></a>
                                        <a href="{{asset('storage/'.$dp[1]->bukti)}}">
                                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail Pembayaran">
                                            <i class="fa fa-book"></i> Bukti
                                        </button>
                                        </a>
                                        @endif
                                    </td>
                                        @endif
                                </tr>
                                <tr>
                                    <td>DP3</td>
                                    <td>Rp. {{ number_format($data->dp3,2,',','.') }}</td>
                                    <td>{{$data->dp3_tgl}}</td>
                                    <td>
                                        @if($data->status<DP3)
                                        <span class="badge badge-warning">Belum Dibayar</span>
                                    </td>
                                    <td></td>
                                        @else
                                        <span class="badge badge-success">Telah Dibayar</span>
                                    </td>
                                    <td>
                                        @if(isset($dp[2]->bukti))
                                        <a href="{{url('kwitansi/'.$dp[2]->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                            <i class="fa fa-book"></i> Kwitansi
                                        </button></a>
                                        <a href="{{asset('storage/'.$dp[2]->bukti)}}">
                                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#modal-tp" title="Detail Pembayaran">
                                            <i class="fa fa-book"></i> Bukti
                                        </button>
                                        </a>
                                        @endif
                                    </td>
                                        @endif
                                </tr>
                                <tr>
                                    <td>Angsuran</td>
                                    <td colspan=2>{{$data->angsur_jml}} X Rp. {{ number_format($data->angsur,2,',','.') }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5 style="text-align:center">History Angsuran</h5>
                    </div>
                </div>
                <div class="table-responsive">                            
                    <table id="table-angsuran" class="stripe table table-stripped text-center">
                        <thead>
                            <tr>
                                <th>Angsuran ke</th>
                                <th>Tanggal Pembayaran</th>
                                <th>Status</th>
                                <th>Bukti Pembayaran</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no = 1;    
                            @endphp
                            @foreach($angsur as $ang)
                            <tr>
                                <td>
                                    @if ($ang->status == DITOLAK)
                                        {{$no}}
                                    @else
                                        {{$no++}}
                                    @endif
                                </td>
                                <td>{{\Carbon\Carbon::parse($ang->tgl_transaksi)->format('d/m/Y')}}</td>
                                <td>
                                    @if ($ang->status == PENDING)
                                        <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                    @elseif ($ang->status == DISETUJUI)
                                        <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    @elseif ($ang->status == DITOLAK)
                                        <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{url('kwitansi/'.$ang->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                            <i class="fa fa-book"></i> Kwitansi
                                        </button></a>
                                    <a href="{{asset('storage/'.$ang->bukti)}}"><button type="button" class="btn btn-sm btn-secondary">
                                        <i class="fa fa-book"></i> Bukti
                                    </button></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
    <!-- Top Modal -->
    <div class="modal fade" id="modal-top" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
            <div class="modal-dialog modal-dialog-top" role="document">
            <form method="POST" action="{{url('ppjb/pdf/')}}" enctype="multipart/form-data" target="_blank">
            {{ csrf_field() }}
            <input value="{{$data->id}}" type="text" class="form-control" id="id" name="id" hidden>
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Pilih Pihak Pertama</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="si si-close"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group row">
                                <label class="col-12" for="">Nama</label>
                                <div class="col-md-12">
                                    <select id="nomorkavling" name="Nomor kavling" class="form-control">
                                        <option value=""></option>
                                        @foreach($user as $users)
                                            <option value="{{$users->id}}">
                                                {{$users->name}} - {{$users->no_ktp}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-alt-success">
                            <i class="fa fa-check"></i> Submit
                        </button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <!-- END Top Modal -->
</div>
@endsection
@section('moreJS')
<script src="select2.min.js"></script>
<script>
    $(document).ready(function () {
        $("#nomorkavling").select2({
            placeholder: "Please Select",
            width:'100%'
        });
    });
</script>
@endsection