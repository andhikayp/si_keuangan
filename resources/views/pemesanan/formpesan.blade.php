@extends('layouts.base.app')
@section('title', 'Form Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan')}}">Pemesanan</a>
    <span class="breadcrumb-item active">Tambah Pemesanan</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Pemesanan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <form action="{{url('/tambah_pesan')}}" method="post" enctype="multipart/form-data" id="add_pesan">
            {{@csrf_field()}}
            <div class="form-group row">
                <label class="col-12">Nomor Kavling</label>
                <div class="col-md-9">
                    <select id="nomor-kavling" name="Nomor kavling" class="form-control">
                        <option value=""></option>
                        @foreach($kavling as $kav)
                            <option value="{{$kav->id}}">{{$kav->name}}</option>
                        @endforeach
                        @foreach ($kavling as $kav)
                            <input type="hidden" style="display:none" id="item-kavling-harga{{$kav->id}}" value="{{$kav->harga}}">
                            <input type="hidden" style="display:none" id="item-kavling-diskon{{$kav->id}}" value="{{$kav->diskon}}">
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="Nama pemesan">Nama Pemesan</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="nama-pemesan-input" name="Nama pemesan" placeholder="Nama Pemesan.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="Alamat">Alamat</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="alamat-input" name="Alamat" placeholder="Alamat.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="Email">Email</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="email-input" name="Email" placeholder="Email.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="Ktp">No KTP</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="ktp-input" name="Ktp" placeholder="No KTP.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="Hp Pribadi">No HP Pribadi</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="hp-pribadi-input" name="Hp Pribadi" placeholder="No HP Pribadi.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="Hp Kerabat">No HP Kerabat</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="hp-kerabat-input" name="Hp Kerabat" placeholder="No HP Kerabat.." required>
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label class="col-12" for="Harga PPJB">Harga Deal</label>
                <div class="col-md-9">
                    <input type="text" class="form-control uang" id="harga-deal-input" name="Harga PPJB" placeholder="Harga Deal.." required>
                </div>
            </div>
            <div class="dp-angsuran table-responsive">
                <table class="table">
                    <tr>
                        <td style="width:10px"> </td>
                        <td style="width:55px">UTJ</td>
                        <td style="width:30px">Rp  </td><td style="width:164px"><input style="width:164px" type="text" class="form-control uang" id="utj-input" name="UTJ" placeholder="UTJ.." required></td>
                        <td style="width:60px">Tanggal  </td><td><input style="width:164px" type="date" class="form-control" id="harga-deal-input" name="Tanggal UTJ" placeholder="tanggal.." required></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>DP1</td>
                        <td>Rp  </td>
                        <td><input style="width:164px" type="text" class="form-control uang" id="dp1-input" name="DP1" placeholder="DP1.." required></td>
                        <td>Tanggal  </td><td><input style="width:164px" type="date" class="form-control" id="harga-deal-input" name="Tanggal DP1" placeholder="tanggal.." required></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>DP2</td>
                        <td>Rp  </td><td><input style="width:164px" type="text" class="form-control uang" id="dp2-input" name="DP2" placeholder="DP2.." required></td>
                        <td>Tanggal  </td><td><input style="width:164px" type="date" class="form-control" id="harga-deal-input" name="Tanggal DP2" placeholder="tanggal.." required></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>DP3</td>
                        <td>Rp  </td><td><input style="width:164px" type="text" class="form-control uang" id="dp3-input" name="DP3" placeholder="DP3.." required></td>
                        <td>Tanggal  </td><td><input style="width:164px" type="date" class="form-control" id="harga-deal-input" name="Tanggal DP3" placeholder="tanggal.." required></td>
                    </tr>
                    <tr>
                        <td> </td>
                        <td>Angsuran</td>
                        <td></td><td><input style="width:164px" type="text" class="form-control" id="harga-deal-input" name="Jumlah Angsuran" placeholder="Jumlah.." required></td>
                        <td>X   Rp </td><td><input style="width:164px" type="text" class="form-control uang" id="angsuran-input" name="Angsuran" placeholder="Angsuran.." required></td>
                    </tr>
                </table>
            </div><br>
            <div class="form-group row">
                <label class="col-12" for="jatuh-tempo-input">Jatuh Tempo Pembayaran (setiap tanggal..)</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="jatuh-tempo-input" name="Tanggal Jatuh Tempo" placeholder="Jatuh Tempo.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="angsuran1-input">Angsuran pertama (pada tanggal..)</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="angsuran1-input" name="Angsuran pertama" placeholder="Angsuran pertama.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="angsuran2-input">Angsuran terakhir (pada tanggal..)</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="angsuran2-input" name="Angsuran terakhir" placeholder="Angsuran terakhir.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tanggal-pesan">Tanggal Pemesanan</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="tanggal-pesan" name="Tanggal Pemesanan" placeholder="Tanggal Pemesanan.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tanggal-pesan">Nama Marketing</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="nama_marketing" name="nama_marketing" placeholder="Jika tanpa menggunakan jasa marketing jangan diisi">
                </div>
            </div>
            {{-- <div class="form-group row">
                <label class="col-12">Nama Marketing</label>
                <div class="col-md-9">
                    <select id="nama_marketing" name="nama_marketing" class="form-control">
                        <option value=""disabled selected></option>
                        <option value="null">Tanpa Marketing</option>
                        @foreach($marketing as $marketings)
                            <option value="{{$marketings->id}}">{{$marketings->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div> --}}
            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-alt-primary" id="yakin">Submit</button>
                </div>
            </div>
        </form>

    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
            width:'100%'
        });
    });
    $(document).ready(function(){
        $('#nomor-kavling').change(function () {
            var value = $(this).val();
            $('#harga-deal-input').val($('#item-kavling-harga'+value).val()-$('#item-kavling-diskon'+value).val());
        });
    });

    $(document).ready(function () {
        $("#nama_marketing").select2({
            placeholder: "Please Select",
            width:'100%'
        });
    });
    $(document).ready(function () {
        $(".uang").on('keyup', function(evt){
            var 	number_string = $(this).val().replace(/[^,\d]/g, '').toString(),
            split	= number_string.split(','),
            sisa 	= split[0].length % 3,
            rupiah 	= split[0].substr(0, sisa),
            ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            $(this).val(rupiah) ;
        });
        $('#yakin').click(function(){
            event.preventDefault();
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menambah pesanan? Mohon cek ulang data apabila belum yakin",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, submit!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    var harga = $("#harga-deal-input").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    $("#harga-deal-input").val(harga);
                    harga = $("#utj-input").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    if(Number.isNaN(harga)){
                        harga = null
                    }
                    $("#utj-input").val(harga);
                    harga = $("#dp1-input").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    $("#dp1-input").val(harga);
                    harga = $("#dp2-input").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    if(Number.isNaN(harga)){
                        harga = null
                    }
                    $("#dp2-input").val(harga);
                    harga = $("#dp3-input").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    if(Number.isNaN(harga)){
                        harga = null
                    }
                    $("#dp3-input").val(harga);
                    harga = $("#angsuran-input").val().toString().replace(/[^,\d]/g, '');
                    harga = parseFloat(harga.replace(/\,/g,'.'));
                    $("#angsuran-input").val(harga);
                    $('#add_pesan').submit();
                }
            })
        });
    });

</script>
@endsection
