@extends('layouts.base.app')
@section('title', 'Detail Pembatalan Pesan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Pembatalan</span>
    <a class="breadcrumb-item" href="{{url('/pembatalan_pesan')}}">Batal Pesan</a>
    <span class="breadcrumb-item active">Detail</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail Pembatalan Pemesanan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pemesan</p>
                        <h5>{{$dataPesans->buyer}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>{{$dataPesans->email}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>{{$dataPesans->kavlings->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alasan Pembatalan</p>
                        <h5>{{$dataBatals->alasan_pembatalan}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Tanggal Pembatalan</p>
                        <h5>{{ date('d-m-Y', strtotime($dataBatals->tgl_pembatalan)) }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Diinput Oleh</p>
                        <h5>{{$dataBatals->penginput->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Disetujui Oleh</p>
                        @if ($dataBatals->penyetujui != null)
                            <h5>{{$dataBatals->penyetujui->name}}</h5>
                        @else
                            <h5 class="text-warning">Belum Disetujui</h5>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat</p>
                        <h5>{{$dataPesans->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor KTP</p>
                        <h5>{{$dataPesans->no_ktp}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Pribadi</p>
                        <h5>{{$dataPesans->hp_pribadi}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Kerabat</p>
                        <h5>{{$dataPesans->hp_kerabat}}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection