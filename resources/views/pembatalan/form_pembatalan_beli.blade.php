@extends('layouts.base.app')
@section('title', 'Form Pembatalan Beli')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Pembatalan</span>
    <a class="breadcrumb-item" href="{{url('/pembatalan_beli')}}">Batal Beli</a>
    <span class="breadcrumb-item active">Form Pembatalan Beli</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Pembatalan Beli</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <form action="{{action('PembatalanBeliController@tambah_batal')}}" method="post" enctype="multipart/form-data" id="add-batal-beli">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-12">Nomor Kavling</label>
                <div class="col-md-9">
                    <select id="nomor-kavling" name="nomor-kavling" class="form-control">
                        <option value=""></option>
                        @foreach ($dataKavlings as $dataKavling)
                            <option value="{{$dataKavling->kavlings_id}}">{{$dataKavling->kavlings->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="nama-pemesan">Nama Pemesan</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="nama-pemesan" name="nama-pemesan" placeholder="Nama Pemesan.." disabled> 
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="alamat-pemesan">Alamat</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="alamat-pemesan" name="alamat-pemesan" placeholder="Alamat.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="email-pemesan">Email</label>
                <div class="col-md-9">
                    <input type="email" class="form-control" id="email-pemesan" name="email-pemesan" placeholder="Email.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="ktp-pemesan">No KTP</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="ktp-pemesan" name="ktp-pemesan" placeholder="No KTP.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="hp-pribadi-pemesan">No HP Pribadi</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="hp-pribadi-pemesan" name="hp-pribadi-pemesan" placeholder="No HP Pribadi.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="hp-kerabat-pemesan">No HP Kerabat</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="hp-kerabat-pemesan" name="hp-kerabat-pemesan" placeholder="No HP Kerabat.." disabled>
                </div>
            </div>
            <hr/>
            <div class="form-group row">
                <label class="col-12" for="jumlah-uang-kembali">Kesepakatan Uang yang Dikembalikan</label>
                <div class="col-md-9">
                    <input type="number" class="form-control" id="jumlah-uang-kembali" name="jumlah-uang-kembali" placeholder="Uang yang Dikembalikan" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="deskripsi-setor">Alasan Pembatalan</label>
                <div class="col-md-9">
                    <textarea name="alasan-batal-beli" id="alasan-batal-beli" class="form-control" rows="7" placeholder="Alasan Pembatalan Pemesanan" required></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tanggal-batal-beli">Tanggal Pembatalan</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="tanggal-batal-beli" name="tanggal-batal-beli" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-alt-primary" id="yakin">Submit</button>
                </div>
            </div>
        </form>
        
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
<script>
    $(document).ready(function () {
        $("#nomor-kavling").select2({
            placeholder: "Please Select",
                width: '100%'
        });
    });
    $('#yakin').click(function(){
        event.preventDefault();
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Apakah anda yakin akan membatalkan pembelian? Mohon cek ulang data apabila belum yakin",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, submit!',
            cancelButtonText: 'Tidak'
            }).then((result) => {
            if (result.value) {
                $('#add-batal-beli').submit();
            }
        })
    });
</script>
<script src="{{asset('js/getPemesan.js')}}"></script>
@endsection