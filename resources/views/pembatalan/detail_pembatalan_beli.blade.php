@extends('layouts.base.app')
@section('title', 'Detail Pembatalan Beli')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Pembatalan</span>
    <a class="breadcrumb-item" href="{{url('/pembatalan_beli')}}">Batal Beli</a>
    <span class="breadcrumb-item active">Detail</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail Pembatalan Beli</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pembeli</p>
                        <h5>{{$dataPesans->buyer}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>{{$dataPesans->email}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>{{$dataPesans->kavlings->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alasan Pembatalan</p>
                        <h5>{{$dataBatals->alasan_pembatalan}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Total Uang yang Dikembalikan</p>
                        <h5>Rp {{ number_format($dataBatals->jumlah_dikembalikan,2,',','.')}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Tanggal Pembatalan</p>
                        <h5>{{ date('d-m-Y', strtotime($dataBatals->tgl_pembatalan)) }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Diinput Oleh</p>
                        <h5>{{$dataBatals->penginput->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Disetujui Oleh</p>
                        @if ($dataBatals->penyetujui != null)
                            <h5>{{$dataBatals->penyetujui->name}}</h5>
                        @else
                            <h5 class="text-warning">Belum Disetujui</h5>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat</p>
                        <h5>{{$dataPesans->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor KTP</p>
                        <h5>{{$dataPesans->no_ktp}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Pribadi</p>
                        <h5>{{$dataPesans->hp_pribadi}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP Kerabat</p>
                        <h5>{{$dataPesans->hp_kerabat}}</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        @php
            $jumlahKembali = $dataBatals->jumlah_dikembalikan;
            $sudahKembali = $dataTransaksis->where('status', DISETUJUI)->sum('jumlah');
        @endphp
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jumlah yang Sudah Dikembalikan</p>
                        <h5>Rp {{ number_format($sudahKembali,2,',','.') }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jumlah yang Belum Dikembalikan</p>
                        <h5>Rp {{ number_format($jumlahKembali - $sudahKembali,2,',','.') }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="block-content block-content-full">
                <div class="col-md-12 text-center">
                    <h5>History Pengembalian</h5>
                </div>
                <div class="table-responsive">                            
                    <table id="table-angsuran" class="stripe table table-stripped text-center">
                        <thead>
                            <tr>
                                <th>Pengembalian ke</th>
                                <th>Jumlah</th>
                                <th>Tanggal Transfer</th>
                                <th>Status</th>
                                <th>Bukti Transfer</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($dataTransaksis as $dataTransaksi)
                                <tr>
                                    <td>
                                        @if ($dataTransaksi->status == DITOLAK)
                                            {{$no}}
                                        @else
                                            {{$no++}}
                                        @endif
                                    </td>
                                    <td>Rp {{ number_format($dataTransaksi->jumlah,0,',','.') }}</td>
                                    <td>{{date('d-m-Y', strtotime($dataTransaksi->tgl_transaksi))}}</td>
                                    <td>
                                        @if ($dataTransaksi->status == PENDING)
                                            <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                        @elseif ($dataTransaksi->status == DISETUJUI)
                                            <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                        @elseif ($dataTransaksi->status == DITOLAK)
                                            <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{asset('storage/'.$dataTransaksi->bukti)}}"><button type="button" class="btn btn-sm btn-secondary">
                                            <i class="fa fa-book"></i> Bukti
                                        </button></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection