@extends('layouts.base.app')
@section('title', 'Form Pengembalian Uang')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-md-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Pembatalan</span>
    <a class="breadcrumb-item" href="{{url('/pembatalan_beli')}}">Batal Beli</a>
    <span class="breadcrumb-item active">Form Pengembalian Uang</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Pengembalian Uang</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>A-2</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pemesan</p>
                        <h5>Karin</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Total Uang yang Dikembalikan</p>
                        <h5>Rp 2.000.000,00</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jumlah yang Sudah Dikembalikan</p>
                        <h5>Rp 500.000,00</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Sisa yang Belum Dikembalikan</p>
                        <h5>Rp 1.500.000,00</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        <form class="js-validation-bootstrap" action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
            <div class="form-group row" novalidate="novalidate">
                <label class="col-lg-4 col-form-label" for="jumlah-uang-kembali">Jumlah Uang yang Dikembalikan</label>
                <div class="col-lg-6">
                    <input name="val-digits" class="form-control valid" id="jumlah-uang-kembali" aria-invalid="false" aria-describedby="val-digits-error" type="text" placeholder="Jumlah Uang.." required>
                </div>
            </div>
            <div class="form-group row">
                <label for="bukti-pengembalian" class="col-lg-4 col-form-label">Bukti Pengembalian</label>
                <div class="col-lg-6">
                    <input type="file" name="bukti-pengembalian" id="bukti-pengembalian">
                </div>
            </div>
            <div class="form-group row">
                <label for="tanggal-pengembalian" class="col-lg-4 col-form-label">Tanggal Pengembalian</label>
                <div class="col-lg-6">
                    <input type="date" class="form-control" name="tanggal-pengembalian" id="tanggal-pengembalian" required>
                </div>
            </div>
            <br>
            <div class="form-group row">
                <div class="col-lg-4"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>
        </form>
        
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
    <script src="{{ asset('codebase/src/assets/js/pages/be_forms_validation.min.js')}}"></script>
@endsection