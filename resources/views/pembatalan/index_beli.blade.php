@extends('layouts.base.app')
@section('title', ' Pembatalan Beli')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="#">Dashboard</a>
    <span class="breadcrumb-item active">Pembatalan</span>
    <span class="breadcrumb-item active">Batal Beli</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Pembatalan Beli</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-pembatalan'))
            <a href="{{url('/pembatalan_beli/form')}}" class="btn btn-sm btn-warning mb-3"><i class="fa fa-plus mr-2"></i>Tambah Pembatalan Pembelian</a>
        @endif
        <div class="table-responsive">                            
            <table id="table-beli" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center d-sm-table-cell" style="width: 10px;">#</th>
                        <th class="d-sm-table-cell">Kavling</th>
                        <th class="d-sm-table-cell">Pembeli</th>
                        <th class="d-sm-table-cell">Total Pengembalian</th>
                        <th class="d-sm-table-cell">Jumlah Belum Dikembalikan</th>
                        <th class="d-sm-table-cell" style="width: 15%;">Status</th>
                        <th class="text-center d-sm-table-cell" style="width: 15%;">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($dataBatals as $dataBatal)
                        @php
                            $dataTransaksi = App\Transaksi::where('pembatalans_id',$dataBatal->id)
                                                            ->where('status',DISETUJUI)
                                                            ->get();
                            $sudahKembali = $dataTransaksi->sum('jumlah');
                            $belumKembali = $dataBatal->jumlah_dikembalikan - $sudahKembali;
                        @endphp
                        <tr>
                            <td class="text-center">{{$no++}}</td>
                            <td>{{$dataBatal->pemesanans->kavlings->name}}</td>
                            <td>{{$dataBatal->pemesanans->buyer}}</td>
                            <td>Rp {{ number_format($dataBatal->jumlah_dikembalikan,2,',','.') }}</td>
                            <td>Rp {{ number_format($belumKembali,2,',','.') }}</td>
                            <td class="text-center">
                                @if ($dataBatal->status == PENDING)
                                    <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                @elseif ($dataBatal->status == DISETUJUI)
                                    <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                @elseif ($dataBatal->status == DITOLAK)
                                    <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                @elseif ($dataBatal->status == DALAM_PROSES)
                                    <span class="badge badge-warning"><i class="fa fa-cogs"></i></i> Dalam Proses</span>
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{url('/pembatalan_beli/detail/'.$dataBatal->id)}}">
                                    <button type="button" class="btn btn-sm btn-secondary mb-2" data-toggle="tooltip" title="Lihat Detail">
                                        <i class="fa fa-book"></i> Detail
                                    </button>
                                </a>
                                {{--<a href="{{url('/pembatalan_beli/form_pengembalian')}}"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Bayar Uang yang Dikembalikan">
                                    <i class="fa fa-money"></i> Bayar
                                </button></a>--}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#table-beli').DataTable({
                "autoWidth": true,
                "ordering": false,
            });
        });
    </script>
@endsection