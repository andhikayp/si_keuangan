@extends('layouts.base.app')
@section('title', 'Detail Investor')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/investor')}}">Investor</a>
    <span class="breadcrumb-item active">Detail Investor</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail Investor</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Investor</p>
                        <h5>{{$dataUsers->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Email</p>
                        <h5>{{$dataUsers->email}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Tanggal Penyetoran</p>
                        <h5>{{ date('d-m-Y', strtotime($dataInvestors->tgl_perjanjian)) }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Diinput Oleh</p>
                        <h5>{{$dataInvestors->penginput->name}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Disetujui Oleh</p>
                        @if ($dataInvestors->penyetujui != null)
                            <h5>{{$dataInvestors->penyetujui->name}}</h5>
                        @else
                            <h5 class="text-warning">Belum Disetujui</h5>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Alamat</p>
                        <h5>{{$dataUsers->alamat}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor KTP</p>
                        <h5>{{$dataUsers->no_ktp}}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">No HP</p>
                        <h5>{{$dataUsers->telp}}</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        @php
            $jumlahSetor = $dataInvestors->jumlah_setor_modal;
            $modalDapat = $dataTerimaTransaksis->where('status',DISETUJUI)->sum('jumlah');
            $modalKembali = $dataBayarTransaksis->where('status',DISETUJUI)->sum('jumlah');
        @endphp
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Total Setor Modal</p>
                        <h5>Rp {{ number_format($jumlahSetor,2,',','.') }}</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jumlah Modal yang Sudah Disetor Investor</p>
                        <h5>Rp {{ number_format($modalDapat,2,',','.') }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Sisa Modal yang Belum Disetor Investor</p>
                        <h5>Rp {{ number_format($jumlahSetor - $modalDapat,2,',','.') }}</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Jumlah Modal yang Sudah Dikembalikan</p>
                        <h5>
                            @if ($modalKembali > $jumlahSetor)
                                Rp {{ number_format($jumlahSetor,2,',','.') }}
                            @else
                                Rp {{ number_format($modalKembali,2,',','.') }}
                            @endif
                        </h5>
                    </div>
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Sisa Modal yang Belum Dikembalikan</p>
                        <h5>
                            @if ($modalKembali > $jumlahSetor)
                                Rp 0,00
                            @else
                                Rp {{ number_format($jumlahSetor - $modalKembali,2,',','.') }}
                            @endif
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Bagi Hasil yang Sudah Dibayar</p>
                        <h5>
                            @if ($modalKembali < $jumlahSetor)
                                Rp 0,00
                            @else
                                Rp {{ number_format($modalKembali - $jumlahSetor,2,',','.') }}
                            @endif
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        <div class="row">
            <div class="col-md-12">
                <h5 style="text-align:center">History Penyetoran dari Investor</h5>
            </div>
        </div>
        <div class="table-responsive">                            
            <table id="table-setor-modal" class="stripe table table-stripped text-center">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                        <th>Bukti</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $noTerima = 1;
                    @endphp
                    @if (count($dataTerimaTransaksis) > 0)
                        @foreach ($dataTerimaTransaksis as $dataTerimaTransaksi)
                            <tr>
                                <td>{{$noTerima++}}</td>
                                <td>{{ date('d-m-Y', strtotime($dataTerimaTransaksi->tgl_transaksi)) }}</td>
                                <td>Rp {{ number_format($dataTerimaTransaksi->jumlah,0,',','.') }}</td>
                                <td>
                                    @if ($dataTerimaTransaksi->status == PENDING)
                                        <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                    @elseif ($dataTerimaTransaksi->status == DISETUJUI)
                                        <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    @elseif ($dataTerimaTransaksi->status == DITOLAK)
                                        <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{asset('storage/'.$dataTerimaTransaksi->bukti)}}"><button type="button" class="btn btn-sm btn-secondary">
                                        <i class="fa fa-book"></i> Bukti
                                    </button></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">Data not found</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <hr style="border-width:3px">
        <div class="row">
            <div class="col-md-12">
                <h5 style="text-align:center">History Pembayaran Investor</h5>
            </div>
        </div>
        <div class="table-responsive">                            
            <table id="table-bayar-investor" class="stripe table table-stripped text-center">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                        <th>Bukti</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $noBayar = 1;
                    @endphp
                    @if (count($dataBayarTransaksis) > 0)
                        @foreach ($dataBayarTransaksis as $dataBayarTransaksi)
                            <tr>
                                <td>{{$noBayar++}}</td>
                                <td>{{ date('d-m-Y', strtotime($dataBayarTransaksi->tgl_transaksi)) }}</td>
                                <td>Rp {{ number_format($dataBayarTransaksi->jumlah,0,',','.') }}</td>
                                <td>
                                    @if ($dataBayarTransaksi->status == PENDING)
                                        <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                    @elseif ($dataBayarTransaksi->status == DISETUJUI)
                                        <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    @elseif ($dataBayarTransaksi->status == DITOLAK)
                                        <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{asset('storage/'.$dataBayarTransaksi->bukti)}}"><button type="button" class="btn btn-sm btn-secondary">
                                        <i class="fa fa-book"></i> Bukti
                                    </button></a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">Data not found</td>
                        </tr>
                    @endif
                    
                </tbody>
            </table>
        </div>
        <hr style="border-width:3px">
        <div class="row mb-3">
            <div class="col-md-6">
                @if($dataInvestors->bukti_perjanjian != null)
                    <a href="{{ url('/investor/detail/'.$dataInvestors->bukti_perjanjian) }}" target="_blank">
                        <button type="button" class="btn btn-primary">Bukti Perjanjian</button>
                    </a>
                @else
                    <form class="form-inline" action="{{ action('InvestorController@bukti_perjanjian') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <label class="sr-only" for="upload">Upload Bukti Perjanjian</label>
                        <input type="hidden" value="{{$dataInvestors->id}}" name="id-investor">
                        <input type="file" class="form-control mb-2 mr-sm-2 mb-sm-0 " name="bukti_perjanjian" id="upload" required>
                        <button type="submit" class="btn btn-alt-primary">Upload</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection