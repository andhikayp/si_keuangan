@extends('layouts.base.app')
@section('title', 'Investor')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Investor</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('error'))
        <div class="alert alert-danger" role="alert">
            {{ session()->get('error') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Daftar Investor</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-investor'))
            <a href="{{url('/investor/form_setor_modal')}}">
            <button type="button" class="btn btn-sm btn-warning">
                <i class="fa fa-plus mr-2"></i>Tambah Setor Modal
            </button></a>
            <br><br>
        @endif
        <div class="table-responsive">                            
            <table id="table-modal" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Nama</th>
                        <th>Modal</th>
                        <th>Modal Sudah Kembali</th>
                        <th>Sisa Modal Belum Kembali</th>
                        <th>Bagi Hasil</th>
                        <th>Tanggal</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($dataInvestors as $dataInvestor)
                        @php
                            $dataTransaksi = App\Transaksi::where('investors_id',$dataInvestor->id)
                                                ->where('status',DISETUJUI)
                                                ->get();
                            $modalSudahKembali = $dataTransaksi->where('jenis_transaksi', PENGELUARAN)->sum('jumlah');
                            $bagiHasil = 0;
                            $sisaBelumKembali = 0;
                            if ($modalSudahKembali > $dataInvestor->jumlah_setor_modal) {
                                $bagiHasil = $modalSudahKembali - $dataInvestor->jumlah_setor_modal;
                                $modalSudahKembali = $modalSudahKembali - $bagiHasil;
                            } else {
                                $sisaBelumKembali = $dataInvestor->jumlah_setor_modal - $modalSudahKembali;
                            }
                        @endphp
                        <tr>
                            <td class="text-center">{{$no++}}</td>
                            <td>{{$dataInvestor->users->name}}</td>
                            <td>Rp {{ number_format($dataInvestor->jumlah_setor_modal,2,',','.') }}</td>
                            <td>Rp {{ number_format($modalSudahKembali,2,',','.') }}</td>
                            <td>Rp {{ number_format($sisaBelumKembali,2,',','.') }}</td>
                            <td>Rp {{ number_format($bagiHasil,2,',','.') }}</td>
                            <td>{{ date('d-m-Y', strtotime($dataInvestor->tgl_perjanjian)) }}</td>
                            <td class="text-center">
                                <a href="{{url('/investor/detail/'.$dataInvestor->id)}}"><button type="button" class="btn btn-sm btn-secondary mb-2">
                                    <i class="fa fa-book"></i> Detail
                                </button></a>
                                {{--<a href="{{url('investor/form_bayar_investor')}}"><button type="button" class="btn btn-sm btn-primary mb-2">
                                    <i class="fa fa-money"></i> Bayar
                                </button></a>--}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#table-modal').DataTable({
            "autoWidth": true,
            "ordering": false,
        });
    });
</script>
@endsection