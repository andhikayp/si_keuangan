@extends('layouts.base.app')
@section('title', ' Form Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Setor Modal</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Setor Modal</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <form action="{{action('InvestorController@setor_modal')}}" method="post" enctype="multipart/form-data" id="add-setor">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-12" for="nama-investor">Nama Investor</label>
                <div class="col-md-9">
                    <select id="nama-investor" name="nama-investor" class="form-control">
                        <option value=""></option>
                        @foreach ($dataInvestors as $dataInvestor)
                            <option value="{{$dataInvestor->id}}">{{$dataInvestor->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="email-investor">Email</label>
                <div class="col-md-9">
                    <input type="email" class="form-control" id="email-investor" name="email-investor" placeholder="Email.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="hp-investor">No HP</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="hp-investor" name="hp-investor" placeholder="No HP.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="alamat-investor">Alamat</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="alamat-investor" name="alamat-investor" placeholder="Alamat.." disabled>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-12" for="jumlah-perjanjian">Jumlah Setor Modal</label>
                <div class="col-md-9">
                    <input type="number" class="form-control" id="jumlah-perjanjian" name="jumlah-perjanjian" placeholder="Jumlah Setor.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tanggal-perjanjian">Tanggal Penyetoran</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="tanggal-perjanjian" name="tanggal-perjanjian" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="deskripsi-perjanjian">Bukti Perjanjian</label>
                <div class="col-md-9">
                    <input type="file" name="bukti-perjanjian" id="bukti-perjanjian">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-alt-primary" id="yakin">Submit</button>
                </div>
            </div>
        </form>
        
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
    <script>
        $(document).ready(function () {
            $("#nama-investor").select2({
                placeholder: "Please Select",
                    width: '100%'
            });
            $('#yakin').click(function(){
                event.preventDefault();
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Apakah anda yakin akan menambah data setor modal? Mohon cek ulang data apabila belum yakin",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, submit!',
                    cancelButtonText: 'Tidak'
                    }).then((result) => {
                    if (result.value) {
                        $('#add-setor').submit();
                    }
                })
            });
        });
    </script>
    <script src="{{asset('js/getInvestor.js')}}"></script>
@endsection