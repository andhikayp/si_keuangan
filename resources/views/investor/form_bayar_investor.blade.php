@extends('layouts.base.app')
@section('title', 'Form Pembayaran Investor')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-md-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/investor')}}">Investor</a>
    <span class="breadcrumb-item active">Form Pembayaran Investor</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Pembayaran Investor</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <form class="js-validation-bootstrap" action="{{action('InvestorController@bayar_investor')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-12" for="nama-investor">Nama Investor</label>
                <div class="col-md-9">
                    <select id="nama-investor" name="nama-investor" class="form-control">
                        <option value=""></option>
                        @foreach ($dataInvestors as $dataInvestor)
                            <option value="{{$dataInvestor->id}}">{{$dataInvestor->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="email-investor">Email</label>
                <div class="col-md-9">
                    <input type="email" class="form-control" id="email-investor" name="email-investor" placeholder="Email.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="hp-investor">No HP</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="hp-investor" name="hp-investor" placeholder="No HP.." disabled>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="alamat-investor">Alamat</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="alamat-investor" name="alamat-investor" placeholder="Alamat.." disabled>
                </div>
            </div>
            <hr>
            <div class="form-group row">
                <label class="col-12" for="jumlah-bayar">Jumlah Bayar ke Investor</label>
                <div class="col-md-9">
                    <input type="number" class="form-control" id="jumlah-bayar" name="jumlah-bayar" placeholder="Jumlah Setor.." required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tanggal-bayar">Tanggal Pembayaran</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="tanggal-bayar" name="tanggal-bayar" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="bukti-bayar">Bukti Pembayaran</label>
                <div class="col-md-9">
                    <input type="file" name="bukti-bayar" id="bukti-bayar" required>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>
        </form>
        {{--<form class="js-validation-bootstrap" action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
            <div class="form-group row" novalidate="novalidate">
                <label class="col-lg-4 col-form-label" for="jumlah-bayar-investor">Jumlah Bayar ke Investor</label>
                <div class="col-lg-6">
                    <input name="val-digits" class="form-control valid" id="jumlah-bayar-investor" aria-invalid="false" aria-describedby="val-digits-error" type="text" placeholder="Jumlah Uang.." required>
                </div>
            </div>
            <div class="form-group row">
                <label for="bukti-pembayaran-investor" class="col-lg-4 col-form-label">Bukti Pembayaran</label>
                <div class="col-lg-6">
                    <input type="file" name="bukti-pembayaran-investor" id="bukti-pembayaran-investor" multiple="">
                </div>
            </div>
            <div class="form-group row">
                <label for="tanggal-pembayaran-investor" class="col-lg-4 col-form-label">Tanggal Pembayaran</label>
                <div class="col-lg-6">
                    <input type="date" class="form-control" name="tanggal-pembayaran-investor" id="tanggal-pembayaran-investor" required>
                </div>
            </div>
            <br>
            <div class="form-group row">
                <div class="col-lg-4"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </div>
        </form>--}}
        
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection
@section('moreJS')
    <script>
        $(document).ready(function () {
            $("#nama-investor").select2({
                placeholder: "Please Select",
                    width: '100%'
            });
        });
    </script>
    <script src="{{ asset('codebase/src/assets/js/pages/be_forms_validation.min.js')}}"></script>
    <script src="{{ asset('js/getInvestor.js') }}"></script>
@endsection