@extends('layouts.base.app')
@section('title', 'Data Kavling')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}" >Dashboard</a>
    <span class="breadcrumb-item active">Data Kavling</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Data Kavling</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-datakavling'))
            <a href="" data-toggle="modal" data-target="#modal-top2">
                <button type="button" class="btn btn-sm btn-warning">
                    <i class="fa fa-plus mr-2"></i>Tambah Kavling
                </button></a>
            <br><br>
        @endif
        <div class="table-responsive">                            
            <table class="table table-striped table-vcenter table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center" style="vertical-align:middle">#</th>
                        <th rowspan="2" style="vertical-align:middle;text-align:center">Kavling</th>
                        <th rowspan="2" style="text-align:center">Lebar</th>
                        <th rowspan="2" style="text-align:center">Panjang</th>
                        <th rowspan="2" style="vertical-align:middle;text-align:center">Luas</th>
                        <th rowspan="2" style="vertical-align:middle;text-align:center">Harga</th>
                        <th rowspan="2" style="vertical-align:middle;text-align:center">Diskon</th>
                        <th rowspan="2" style="vertical-align:middle;text-align:center">HPP</th>
                        <th rowspan="2" style="vertical-align:middle;text-align:center">Opsi</th>
                    </tr>
                    <!-- <tr>
                        <th style="text-align:center">Depan</th>
                        <th style="text-align:center">Belakang</th>
                        <th style="text-align:center">Kiri</th>
                        <th style="text-align:center">Kanan</th>
                    </tr> -->
                </thead>
                <tbody>
                    @php
                        $nomor = 1
                    @endphp
                    @foreach($tampil as $ta)
                    <tr>
                        <td class="text-center">{{ $nomor++}}</td>
                        <td class="font-w600" style="text-align:center">{{ $ta->name}}</td>
                        <td class="d-sm-table-cell" style="text-align:center">{{ $ta->lebar}}&nbsp;m</td>
                        <td class="d-sm-table-cell" style="text-align:center">{{ $ta->panjang}}&nbsp;m</td>
                        <!-- <td class="d-sm-table-cell" style="text-align:center">{{ $ta->kiri}}&nbsp;m</td>
                        <td class="d-sm-table-cell" style="text-align:center">{{ $ta->kanan}}&nbsp;m</td> -->
                        <td class="d-sm-table-cell" style="text-align:right">{{ $ta->luas}}&nbsp;m<sup>2</sup></td>
                        <td class="d-sm-table-cell" style="text-align:right">Rp&nbsp;{{ number_format($ta->harga,2,',','.')}}</td>
                        <td class="d-sm-table-cell" style="text-align:right">Rp&nbsp;{{ number_format($ta->diskon,2,',','.')}}</td>
                        <td class="d-sm-table-cell" style="text-align:right">Rp&nbsp;{{ number_format($ta->hpp,2,',','.')}}</td>
                        @role('Komisaris|Manajer|Admin')
                        <td class="text-center d-sm-table-cell">
                        @if($ta->status == KAVLING_AVAILABLE && auth()->user()->can('create-datakavling'))
                            <button type="button" class="btn btn-primary btn-sm fa fa-pencil" data-toggle="modal" data-target="#av{{$ta->id}}" title="Edit Kavling"></button>
                            <button type="button" id="hapus{{$ta->id}}" class="btn btn-danger btn-sm fa fa-trash" title="Hapus Kavling"></button>
                            <form method="post" action="{{url('hapus-data-kavling')}}" id="hapus-data{{$ta->id}}" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" value="{{$ta->id}}" name="kavling">
                            </form>
                        @elseif($ta->status == KAVLING_BOOKING)
                            <button style="width: 80px;" type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#av{{$ta->id}}" disabled>Booked</button>
                        @endif
                        </td>
                        @endrole
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
    </div>
</div>
@foreach($tampil as $ta)
@if($ta->status == KAVLING_AVAILABLE)
<div class="modal fade" id="av{{$ta->id}}" tabindex="-1" role="dialog" aria-labelledby="av{{$ta->id}}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <form method="POST" action="{{url('editharga')}}" enctype="multipart/form-data" id="editharga{{$ta->id}}">
        {{ csrf_field() }}
        <input value="{{$ta->id}}" type="hidden" class="form-control" id="id" name="id">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Edit Kavling {{$ta->name}}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-group row">
                        <label class="col-12" for="no_kavling">Nomor Kavling</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="eno_kavling" name="No kavling" value="{{$ta->name}}"> 
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-12" for="lebar_depan">Lebar Depan</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control edit{{$ta->id}}" id="elebar_depan{{$ta->id}}" name="Lebar depan" value="{{$ta->depan}}"> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="lebar_belakang">Lebar Belakang</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control edit{{$ta->id}}" id="elebar_belakang{{$ta->id}}" name="Lebar belakang" value="{{$ta->belakang}}"> 
                        </div>
                    </div>           
                    <div class="form-group row">
                        <label class="col-12" for="panjang_kiri">Panjang Kiri</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control edit{{$ta->id}}" id="epanjang_kiri{{$ta->id}}" name="Panjang kiri" value="{{$ta->kiri}}"> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="panjang_kanan">Panjang Kanan</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control edit{{$ta->id}}" id="epanjang_kanan{{$ta->id}}" name="Panjang kanan" value="{{$ta->kanan}}"> 
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-12" for="lebar">Lebar</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="lebar" name="Lebar" placeholder="Lebar" value="{{$ta->lebar}}"> 
                        </div>
                    </div>           
                    <div class="form-group row">
                        <label class="col-12" for="panjang">Panjang</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="panjang" name="Panjang" placeholder="Panjang" value="{{$ta->panjang}}"> 
                        </div>
                    </div>  
                    <div class="form-group row">
                        <label class="col-12" for="luas">Luas</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control edit{{$ta->id}}" id="eluas{{$ta->id}}" name="Luas" value="{{$ta->luas}}"> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="harga">Harga (Rp)</label>
                        <div class="col-md-12">
                            <!-- <input type="text" class="form-control eharga" id="eharga" name="Harga" placeholder="Harga" value="{{$ta->harga}}" required>  -->
                            <input type="text" class="form-control" id="eharga" name="Harga" placeholder="Harga" value="{{$ta->harga}}" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="diskon">Diskon (Rp)</label>
                        <div class="col-md-12">
                            <!-- <input type="text" class="form-control ediskon" id="ediskon" name="Diskon" placeholder="Diskon" value="{{$ta->diskon}}" required>  -->
                            <input type="text" class="form-control" id="ediskon" name="Diskon" placeholder="Diskon" value="{{$ta->diskon}}" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="ehpp">Harga Pokok Penjualan (Rp)</label>
                        <div class="col-md-12">
                            <!-- <input type="text" class="form-control ehpp" id="ehpp" name="hpp" placeholder="Harga Pokok Penjualan" value="{{$ta->hpp}}" required>  -->
                            <input type="text" class="form-control" id="" name="hpp" placeholder="Harga Pokok Penjualan" value="{{$ta->hpp}}" required> 
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-alt-success" id="eyakin{{$ta->id}}">
                        <i class="fa fa-check"></i> Submit
                    </button>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
@endif
@endforeach
<div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <form method="POST" action="{{ url('/tambah_kavling') }}" enctype="multipart/form-data" id="add_kavling">
        {{ csrf_field() }}
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Tambah Data Kavling</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="form-group row">
                        <label class="col-12" for="no_kavling">Nomor Kavling</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="no_kavling" name="No kavling" placeholder="Nomor Kavling" required> 
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <label class="col-12" for="lebar_depan">Lebar Depan</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="lebar_depan" name="Lebar depan" placeholder="Depan" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="lebar_belakang">Lebar Belakang</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="lebar_belakang" name="Lebar belakang" placeholder="Belakang" required> 
                        </div>
                    </div>           
                    <div class="form-group row">
                        <label class="col-12" for="panjang_kiri">Panjang Kiri</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="panjang_kiri" name="Panjang kiri" placeholder="Panjang Kiri" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="panjang_kanan">Panjang Kanan</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="panjang_kanan" name="Panjang kanan" placeholder="Panjang Kanan" required> 
                        </div>
                    </div> -->
                    <div class="form-group row">
                        <label class="col-12" for="lebar">Lebar</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="lebar" name="Lebar" placeholder="Lebar" required> 
                        </div>
                    </div>           
                    <div class="form-group row">
                        <label class="col-12" for="panjang">Panjang</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control utama" id="panjang" name="Panjang" placeholder="Panjang" required> 
                        </div>
                    </div>  
                    <div class="form-group row">
                        <label class="col-12" for="luas">Luas</label>
                        <div class="col-md-12">
                            {{-- <input type="text" class="form-control" id="luas" name="Luas" placeholder="Luas" readonly="readonly" required>  --}}
                            <input type="text" class="form-control" id="luas" name="Luas" placeholder="Luas" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="harga">Harga (Rp)</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="harga" name="Harga" placeholder="Harga" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="diskon">Diskon (Rp)</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="diskon" name="Diskon" placeholder="Diskon" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="hpp">Harga Pokok Penjualan (Rp)</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="hpp" name="hpp" placeholder="Harga Pokok Penjualan" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-alt-success" id="yakin">
                    <i class="fa fa-check"></i> Submit
                </button>
            </div>
        </div>
        </form>
    </div>
</div>
@endsection
@section('moreJS')
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>
        <script>
            function formatload(num){
                var 	number_string = num.toString().replace(/[^.\d]/g, ''),
                split	= number_string.split('.'),
                sisa 	= split[0].length % 3,
                rupiah 	= split[0].substr(0, sisa),
                ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                    
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return rupiah;
            }
            $(document).ready(function(){
                $("#harga").on('keyup', function(evt){
                    var 	number_string = $("#harga").val().replace(/[^,\d]/g, '').toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    $("#harga").val(rupiah) ;
                });
                $("#diskon").on('keyup', function(evt){
                    var 	number_string = $("#diskon").val().replace(/[^,\d]/g, '').toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    $("#diskon").val(rupiah);
                });
                $("#hpp").on('keyup', function(evt){
                    var 	number_string = $("#hpp").val().replace(/[^,\d]/g, '').toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    $("#hpp").val(rupiah);
                });
                $(".ehpp").on('keyup', function(evt){
                    var 	number_string = $(this).val().replace(/[^,\d]/g, '').toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    $(this).val(rupiah);
                });
                $(".eharga").on('keyup',function(e){
                    var 	number_string = $(this).val().replace(/[^,\d]/g, '').toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    $(this).val(rupiah);
                });
                $(".ediskon").on('keyup',function(e){
                    var 	number_string = $(this).val().replace(/[^,\d]/g, '').toString(),
                    split	= number_string.split(','),
                    sisa 	= split[0].length % 3,
                    rupiah 	= split[0].substr(0, sisa),
                    ribuan 	= split[0].substr(sisa).match(/\d{1,3}/gi);
                        
                    if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                    }
                    
                    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                    $(this).val(rupiah);
                });
                if($(".eharga").length){$(".eharga").val(formatload($(".eharga").val()));}
                if($(".ediskon").length){$(".ediskon").val(formatload($(".ediskon").val()));}
                if($(".ehpp").length){$(".ehpp").val(formatload($(".ehpp").val()));}
                $("#nomor-kavling").select2({
                    placeholder: "Nomor Kavling",
                    width: '100%'
                });
                $('.utama').change(function(){
                    if($('#lebar_depan').val()!=null && $('#lebar_belakang').val()!=null && $('#panjang_kiri').val()!=null && $('#panjang_kanan').val()!=null)
                    {
                        $('#luas').attr('value',parseFloat(((parseFloat($('#lebar_depan').val())+parseFloat($('#lebar_belakang').val()))/2)*((parseFloat($('#panjang_kanan').val())+parseFloat($('#panjang_kiri').val()))/2)));
                    }
                });
                
                $('#yakin').click(function(){
                    event.preventDefault();
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Apakah anda yakin akan menambah Kavling? Mohon cek ulang data apabila belum yakin",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, submit!',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            var harga = $("#harga").val().toString().replace(/[^,\d]/g, '');
                            harga = parseFloat(harga.replace(/\,/g,'.'));
                            $("#harga").val(harga);
                            var diskon = $("#diskon").val().toString().replace(/[^,\d]/g, '');
                            diskon = parseFloat(diskon.replace(/\,/g,'.'));
                            $("#diskon").val(diskon);
                            var hpp = $("#hpp").val().toString().replace(/[^,\d]/g, '');
                            hpp = parseFloat(hpp.replace(/\,/g,'.'));
                            $("#hpp").val(hpp);
                            $('#add_kavling').submit();
                        }
                        })
                });
                @foreach($tampil as $ta)
                @if($ta->status == KAVLING_AVAILABLE)
                $('.edit{{$ta->id}}').change(function(){
                    if($('#elebar_depan{{$ta->id}}').val()!=null && $('#elebar_belakang{{$ta->id}}').val()!=null && $('#epanjang_kiri{{$ta->id}}').val()!=null && $('#epanjang_kanan{{$ta->id}}').val()!=null)
                    {
                        $('#eluas{{$ta->id}}').attr('value',parseFloat(((parseFloat($('#elebar_depan{{$ta->id}}').val())+parseFloat($('#elebar_belakang{{$ta->id}}').val()))/2)*((parseFloat($('#epanjang_kanan{{$ta->id}}').val())+parseFloat($('#epanjang_kiri{{$ta->id}}').val()))/2)));
                    }
                });
                $('#eyakin{{$ta->id}}').click(function(){
                    event.preventDefault();
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Apakah anda yakin akan mengedit Kavling? Mohon cek ulang data apabila belum yakin",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, submit!',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            // console.log("masokk")
                            // var harga = $("#eharga").val().toString().replace(/[^,\d]/g, '');
                            // harga = parseFloat(harga.replace(/\,/g,'.'));
                            // $("#eharga").val(harga);
                            // var diskon = $("#ediskon").val().toString().replace(/[^,\d]/g, '');
                            // diskon = parseFloat(diskon.replace(/\,/g,'.'));
                            // $("#ediskon").val(diskon);
                            // var hpp = $("#ehpp").val().toString().replace(/[^,\d]/g, '');
                            // hpp = parseFloat(hpp.replace(/\,/g,'.'));
                            // $("#ehpp").val(hpp);
                            $('#editharga{{$ta->id}}').submit();
                        }
                        })
                });
                $('#hapus{{$ta->id}}').click(function(){
                    event.preventDefault();
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Apakah anda yakin akan menghapus Kavling? Data akan hilang dan tidak bisa dikembalikan!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, hapus!',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            $('#hapus-data{{$ta->id}}').submit();
                        }
                        })
                });
                @endif
                @endforeach
            });
        </script>
        @endsection