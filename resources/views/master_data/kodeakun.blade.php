@extends('layouts.base.app')
@section('title', 'Kode Akun')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
@php
function recursive($kode){
    foreach($kode->child as $anak){
        if(!is_null($anak)){
            echo('<tr>
                <td colspan="1" class="text-center"></td>
                <td colspan="1" style="width:5%">'.substr($anak->kode_akun,1).'</td>
                <td colspan="1">'.$anak->nama_akun.'</td></tr>');
                recursive($anak);
        }
    }
}
@endphp 
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Kode Akun</span>
</nav>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Kode Akun</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        @if (auth()->user()->can('create-kodeakun'))
            <a href="" data-toggle="modal" data-target="#modal-top2">
                <button type="button" class="btn btn-sm btn-warning">
                    <i class="fa fa-plus mr-2"></i>Tambah Kode Akun
                </button></a>
            <br><br>
        @endif
        <div class="row">   
            <div class="col-md-6 col-xl-6">
                <h5>PENERIMAAN</h5>                        
                <table class="table table-hover table-sm">
                    <tbody>
                        @foreach($penerimaan as $kode)
                        <tr class="table-active">
                            <td colspan="1" class="text-center" style="width:50px"><b>{{substr($kode->kode_akun,1)}}</b></td>
                            <td colspan="2"><b>{{$kode->nama_akun}}</b></td>
                        </tr>
                        @php
                        recursive($kode);    
                        @endphp
                        {{-- <tr>
                            <td colspan="1" class="text-center"></td>
                            <td colspan="1" style="width:5%">{{substr($kode->kode_akun,1)}}</td>
                            <td colspan="1">{{$kode->nama_akun}}</td>
                        </tr>  --}}
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6 col-xl-6">
                <h5>PENGELUARAN</h5>                       
                <table class="table table-hover table-sm">
                    <tbody>
                        @foreach($pengeluaran as $kode)
                        <tr class="table-active">
                            <td colspan="1" class="text-center" style="width:50px"><b>{{substr($kode->kode_akun,1)}}</b></td>
                            <td colspan="2"><b>{{$kode->nama_akun}}</b></td>
                        </tr>
                        @php
                        recursive($kode);    
                        @endphp
                        {{-- <tr>
                            <td colspan="1" class="text-center"></td>
                            <td colspan="1" style="width:5%">{{substr($kode->kode_akun,1)}}</td>
                            <td colspan="1">{{$kode->nama_akun}}</td>
                        </tr>  --}}
                        
                        @endforeach
                        <!-- end baris-->
                        <!-- Tiap Baris-->
                    </tbody>
                </table>
            </div>
        </div>  
    </div>
    <div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-top" role="document">
            <form action="{{url('/tambah_akun')}}" enctype="multipart/form-data" method="post" >
            {{csrf_field()}}
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Tambah Kode Akun</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            <label class="col-12">Jenis Akun</label>
                            <div class="col-md-12">
                                <select id="jenis" name="jenis" class="form-control" aria-placeholder="Jenis Akun">
                                    <option disabled selected hidden>Jenis Akun</option>
                                    <option value="0"></option>
                                    <option value="1">Penerimaan</option>
                                    <option value="2">Pengeluaran</option>
                                </select>
                            </div>
                        </div>
                        <div id="internal" style="display:none">
                        <div class="form-group row" id="group-akun-group">
                            <label class="col-12">Group Akun</label>
                            <div class="col-md-12">
                                <select id="group-akun" name="group-akun" class="form-control" aria-placeholder="Grup Akun">
                                    <option value="" disabled selected hidden>Grup Akun</option>
                                    <option value="new">--Tambah Baru--</option>
                                    @foreach($akun as $kode)
                                    @if(is_null($kode->parent))
                                    <option value="{{$kode->id}}"
                                        @if($kode->tipe == 0)
                                        class="in"
                                        @elseif($kode->tipe==1)
                                        class="out"
                                        @endif>{{substr($kode->kode_akun,1)}}.&nbsp;{{$kode->nama_akun}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="parent-group">
                            <label class="col-12" for="parent">Parent Kode Akun</label>
                            <div class="col-md-12">
                                <select id="parent" name="parent" class="form-control" aria-placeholder="">
                                    <option value="" disabled selected hidden>Parent Akun</option>
                                    @foreach($akun as $kode)
                                    <option value="{{$kode->id}}"
                                        @if(is_null($kode->parent))
                                        class="parent-{{$kode->id}}"
                                        @elseif(!is_null($kode->parent))
                                        class="parent-{{$kode->parent}}"
                                        @endif
                                        >{{substr($kode->kode_akun,1)}}. {{$kode->nama_akun}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="kode-akun">Kode Akun</label>
                            {{-- <span class="nomor-dari-parent col-1" style="padding-right: 0;padding-top:8px;padding-bottom:8px"></span> --}}
                            <div class="col-12">
                                <input type="text" class="form-control" id="kode-akun" name="kode-akun" placeholder="Nomor Kode Akun" required> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="nama-akun">Nama Akun</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="nama-akun" name="nama-akun" placeholder="Nama Akun" required> 
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-alt-success">
                        <i class="fa fa-check"></i> Submit
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $("#internal").hide();
        $("[class^='parent-']").hide();
        $(".out").hide();
        $(".in").hide();
        $('#parent-group').hide();
        // $('.nomor-dari-parent').text("");
        // $("#group-akun").select2({
        //     placeholder: "Grup Akun",
        //     width: '100%'
        // });
        // $("#parent").select2({
        //     placeholder: "Parent Akun",
        //     width: '100%'
        // });
        $("#jenis").change(function(){
            $('#group-akun option').prop('selected', function() {
                return this.defaultSelected;
            });
            $('#parent option').prop('selected', function() {
                return this.defaultSelected;
            });
            $('#kode-akun').val("");
            if($("#jenis").val()==1){
                $(".in").show();
                $(".out").hide();
                $('#internal').show();
            }
            else if($("#jenis").val()==2){
                $(".out").show();
                $(".in").hide();
                $('#internal').show();
            }
            else if($("#jenis").val()==0){
                $(".out").hide();
                $(".in").hide();
                $('#internal').hide();
            }
        });
        $('#group-akun').change(function(){
            $("[class^='parent-']").hide();
            $('#kode-akun').val("");
            if($('#group-akun').val()=="new"){
                $('#parent-group').hide();
                $('#kode-akun').val("");
            }else if($('#group-akun').val()!="new"){
                $('#parent option').prop('selected', function() {
                    return this.defaultSelected;
                });
                $('#parent-group').show();
                $(".parent-"+$('#group-akun').val()).show();
            }
        });
        $('#parent').change(function(){
            $('#kode-akun').val("");
            var parent = $('#parent option:selected').text();
            $('#kode-akun').val(parent.split(" ")[0]);
        });
    });
</script>
@endsection