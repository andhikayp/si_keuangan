@extends('layouts.base.app')
@section('title', 'Dashboard')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <span class="breadcrumb-item active">Dashboard</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Dashboard</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    @php
        var_dump(Session::get('proyek'));return;
    @endphp
    <div class="block-content">
        <div class="row">
            <!-- Row #2 -->
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default border-b">
                        <h3 class="block-title">
                            Penjualan <small>Minggu Ini {{session::get('proyek')}}</small>
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="pull-all pt-50">
                            <!-- Lines Chart Container functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->
                            <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
                            <canvas class="js-chartjs-dashboard-lines"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default border-b">
                        <h3 class="block-title">
                            Penjualan <small>Bulan Ini</small>
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="pull-all pt-50">
                            <!-- Lines Chart Container functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->
                            <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
                            <canvas class="js-chartjs-dashboard-lines2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Row #2 -->
        </div>
        <div class="table-responsive">                            
            <table class="table table-sm">
                <tbody>
                    <!-- pendapatan hari ini-->
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Penjualan Hari Ini</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">0 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">06/07/2019</td>
                        <th colspan="1" class="text-right">0 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Penjualan Minggu Ini</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">10 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">06/07/2019</td>
                        <th colspan="1" class="text-right">2 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">05/07/2019</td>
                        <th colspan="1" class="text-right">0 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">04/07/2019</td>
                        <th colspan="1" class="text-right">3 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">03/07/2019</td>
                        <th colspan="1" class="text-right">1 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">02/07/2019</td>
                        <th colspan="1" class="text-right">1 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">01/07/2019</td>
                        <th colspan="1" class="text-right">1 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">30/06/2019</td>
                        <th colspan="1" class="text-right">2 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Penjualan Bulan Ini</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">36 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Penjualan</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">154 unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Booking</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">186 unit</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<!-- Page JS Plugins -->
<script src="{{asset('codebase/src/assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>

<!-- Page JS Code -->
<script src="{{asset('codebase/src/assets/js/pages/be_pages_dashboard.min.js')}}"></script>
@endsection