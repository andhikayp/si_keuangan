@extends('layouts.base.app')
@section('title', 'Pengeluaran')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Keuangan</span>
    <span class="breadcrumb-item active">Pengeluaran</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Daftar Pengeluaran</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class="table-responsive">                            
            <table id="table-pengeluaran" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="">Kode Akun</th>
                        <th class="">Jumlah Pengeluaran</th>
                        <th class="text-center" style="width: 15%;">Option</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($pengeluaran as $pengeluarans)
                    <tr>
                        <td class="text-center">
                            @php
                                echo $no;
                                $no++;  
                            @endphp
                        </td>
                        <td class="">
                            @php
                                $kode_akun = App\kodeakun::find($pengeluarans->kode_akuns_id);
                            @endphp
                            {{ $kode_akun->kode_akun }} - {{ $kode_akun->nama_akun }}
                        </td>
                        <td class="">
                            @php
                                $transaksi = App\transaksi::where('kode_akuns_id','=', $pengeluarans->kode_akuns_id)
                                    ->where('status', '=', 1)
                                    ->where('proyeks_id', '=', session('id_proyek'))
                                    ->sum('jumlah');
                            @endphp
                            Rp. {{ number_format($transaksi,2,',','.') }}
                        </td>
                        <td class="text-center">
                            <a href="{{url('/keuangan/pengeluaran/detail/'.$pengeluarans->kode_akuns_id)}}">
                                <button type="button" class="btn btn-sm btn-secondary">
                                    <i class="fa fa-book"></i> Detail
                                </button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
    {{-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script> --}}
    <script>
        $(document).ready(function(){
            $('#table-pengeluaran').DataTable({
                "autoWidth": true,
                "ordering": false,
            });
        });
    </script>
@endsection