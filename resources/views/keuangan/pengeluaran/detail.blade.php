@extends('layouts.base.app')
@section('title', 'Detail Pengeluaran')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Keuangan</span>
    <span class="breadcrumb-item active"><a class="breadcrumb-item" href="{{url('/keuangan/pengeluaran')}}">Pengeluaran</a></span>
    <span class="breadcrumb-item active">Detail</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        @php
            $kode_akun = App\kodeakun::find($pengeluaran[0]->kode_akuns_id);
        @endphp
        <h3 class="block-title">Detail Pengeluaran Kode {{ $kode_akun->kode_akun }} - {{ $kode_akun->nama_akun }}</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        {{-- <select name="kode-akun-pengeluaran" id="kode-akun-pengeluaran" class="form-control">
            <option value=""></option>
            <option value="B.1.1">B.1.1 Pembayaran lahan</option>
            <option value="B.1.2">B.1.2 Fee broker</option>
            <option value="B.1.3">B.1.3 Uang transport</option>
            <option value="B.2.1">B.2.1 Pengukuran</option>
            <option value="B.2.2">B.2.2 Urugan</option>
            <option value="">dll..</option>
        </select> --}}
        <h6 id="pilihan-kode"></h6>
        <div class="table-responsive">                            
            <table id="table-pengeluaran" class="stripe table table-stripped">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="">Pengeluaran</th>
                        <th class="">Jumlah</th>
                        <th class="">Tgl</th>
                        <th class="">Input oleh</th>
                        <th class="">Disetujui oleh</th>
                        <th class="text-center" style="width: 15%;">Option</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($pengeluaran as $pengeluarans)
                    <tr>
                        <td class="text-center">
                            @php
                                echo $no;
                                $no++;
                            @endphp
                        </td>
                        <td class="">{{ $pengeluarans->deskripsi }}</td>
                        <td class="">
                            Rp. {{ number_format($pengeluarans->jumlah,2,',','.') }}
                        </td>
                        <td class="">{{ $pengeluarans->tgl_transaksi }}</td>
                        <td class="">
                            @php
                                $penginput = App\user::find($pengeluarans->penginput_id);
                            @endphp
                            {{ $penginput->name }}
                        </td>
                        <td class="">
                            @php
                                if ($pengeluarans->penyetujui_id) {
                                    $penyetuju = App\user::find($pengeluarans->penyetujui_id);
                                    echo $penyetuju->name;
                                }
                                else 
                                {
                                    echo "Belum disetujui";
                                }
                            @endphp
                        </td>
                        <td class="text-center">
                            <a href="{{asset('storage/'.$pengeluarans->bukti)}}" target="_blank">
                                <button type="button" class="btn btn-sm btn-secondary">
                                    <i class="fa fa-image"></i> Bukti
                                </button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#table-pengeluaran').DataTable({
                "autoWidth": true,
                "ordering": false,
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $("#kode-akun-pengeluaran").select2({
                placeholder: "Please Select",
                width: '100%'
            });
        });
    </script>
@endsection