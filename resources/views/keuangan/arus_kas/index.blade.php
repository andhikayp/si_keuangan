@extends('layouts.base.app')
@section('title', 'Arus Kas')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Keuangan</span>
    <span class="breadcrumb-item active">Arus Kas</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Laporan Arus Kas</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
    <div class="block-content block-content-full">         
        <div class="table-responsive">                            
            <table class="table table-bordered table-vcenter">
                <tbody>
                    <tr>
                        <th colspan="1"></th>
                        <td class="text-right">Saldo</td>
                    </tr>

                    <!-- Operational -->
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9">Transaksi Operasional</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Net Income</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Depreciation Expense</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Loss on sale of equipment</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Decrease in account receivable</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in Inventory</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in prepaid expenses</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in accounts payable</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase in income taxes payable</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Transaksi Operasional</td>
                        <th colspan="1" style="background-color: #d9d9d9" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- End Operational -->

                    <!-- Investment -->
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9">Transaksi Investasi</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Purchase of building</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Sale of building</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Purchase of equipment</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Sale of equipment</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Transaksi Investasi</td>
                        <th colspan="1" style="background-color: #d9d9d9" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- end Investment -->

                    <!-- Financing -->
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9" >Transaksi Pendanaan</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Increase of Capital</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Capital Withdrawal</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Transaksi Pendanaan</td>
                        <th colspan="1" style="background-color: #d9d9d9" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- End Financing -->

                    <!-- Konklusi-->
                    <br>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Kenaikan (penurunan) Kas</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp 0,00
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Total revaluasi bank</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp 0,00
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Saldo Awal Kas</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp 0,00
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Saldo Akhir Kas</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">
                            Rp 0,00
                        </th>
                    </tr>
                    <!-- end Konklusi -->
                </tbody>
            </table>
        </div>  
    </div>
    </div>
</div>
@endsection