@extends('layouts.base.app')
@section('title', 'Neraca')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Keuangan</span>
    <span class="breadcrumb-item active">Neraca</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Laporan Neraca</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
    <div class="block-content block-content-full">         
        <div class="table-responsive">                            
            <table class="table ">
                <tbody>
                    <tr>
                        <th colspan="1"></th>
                        <td class="text-right">Saldo</td>
                    </tr>

                    <!-- pendapatan -->
                    <tr>
                        <td  style="background-color: #d9d9d9" colspan="2">Aktiva</td>
                    </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Aktiva Lancar</td>
                            <th colspan="1"></th>
                            
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Total Aktiva Lancar</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        
                        <tr>
                            <td colspan="1" style="padding-left:5%">Aktiva Tetap</td>
                            <th colspan="1"></th>
                            
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Total Aktiva Tetap</td>
                            
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                    <tr>
                        <td colspan="1" style="padding-left:1%;background-color: #f2f248">Total Aktiva</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">Rp 0,00</th>
                    </tr>

                    <tr>
                        <td  style="background-color: #d9d9d9" colspan="2">Kewajiban dan Modal</td>
                    </tr>
                        <!-- harga pokok penjualan -->
                        <tr>
                            <td colspan="1" style="padding-left:5%">Kewajiban Lancar</td>
                            <th colspan="1"></th>
                            
                        </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Total Kewajiban Lancar</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <!-- end harga pokok penjualan -->

                        <!-- harga pokok penjualan -->
                        <tr>
                            <td colspan="2" style="padding-left:5%">Modal Pemilik</td>
                        </tr>
                            <tr>
                                <th colspan="1" style="padding-left:7%">Keuntungan</th>
                                <th colspan="1" class="text-right">Rp 0,00</th>
                            </tr>
                        <tr>
                            <td colspan="1" style="padding-left:5%">Total Modal Pemilik</td>
                            <th colspan="1" class="text-right">Rp 0,00</th>
                        </tr>
                        <!-- end harga pokok penjualan -->

                    <!-- TOTAL-->
                    <br>
                    <tr>
                        <td colspan="1" style="padding-left:1%;background-color: #f2f248">Total Kewajiban dan Modal</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- end harga pokok penjualan -->
                </tbody>
            </table>
        </div>  
    </div>
    </div>
</div>
@endsection