@extends('layouts.base.app')
@section('title', 'Penerimaan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message'))
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="#">Dashboard</a>
    <span class="breadcrumb-item active">Laporan</span>
    <span class="breadcrumb-item active">Penerimaan</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Daftar Penerimaan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        {{-- <div class="form-group row">
            <div class="col-md-12">
                <label for="nomor-kavling-penerimaan">Nomor Kavling</label>
                <select name="nomor-kavling-penerimaan" id="nomor-kavling-penerimaan" class="form-control">
                    <option value=""></option>
                    <option value="A-1">A-1</option>
                    <option value="A-2">A-2</option>
                    <option value="A-3">A-3</option>
                    <option value="B-1">B-1</option>
                    <option value="B-2">B-2</option>
                    <option value="">dll..</option>
                </select>
            </div>
        </div>
        <h6 id="pilihan-kavling"></h6> --}}
        <div class="table-responsive">
            <table id="table-penerimaan" class="stripe table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th>Nomor Kavling</th>
                        <th>Buyer</th>
                        <th>Harga PPJB</th>
                        <th>Total Diterima</th>
                        {{--<th>Total Piutang</th>--}}
                        <th class="text-center" style="width: 15%;">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no = 1;
                    @endphp
                    @foreach($penerimaan as $penerimaans)
                    <tr>
                        <td class="text-center">
                            @php
                                $pemesanan = App\pemesanan::find($penerimaans->pemesanans_id);
                                $kavling = App\kavling::find($pemesanan->kavlings_id);
                                echo $no;
                                $no++;
                            @endphp
                        </td>
                        <td>
                            {{ $kavling->name }}
                        </td>
                        <td>
                            {{-- {{ $pemesan->buyer }} --}}hehe
                        </td>
                        <td>
                            Rp {{ number_format($pemesanan->harga_deal,2,',','.') }}
                        </td>
                        <td>
                            @php
                                $transaksi = App\transaksi::where('kode_akuns_id','!=', null)
                                    ->where('jenis_transaksi','=', 0)
                                    ->where('pemesanans_id','=', $penerimaans->pemesanans_id)
                                    ->where('status', '=', 1)
                                    ->sum('jumlah');
                            @endphp
                            Rp {{ number_format($transaksi,2,',','.') }}
                        </td>
                        {{--<td>
                            Rp. {{ number_format($pemesanan->harga_deal-$transaksi,2,',','.') }}
                        </td>--}}
                        <td class="text-center">
                            <a href="{{url('keuangan/penerimaan/detail/'.$penerimaans->pemesanans_id)}}">
                                <button type="button" class="btn btn-sm btn-secondary">
                                    <i class="fa fa-book"></i> Detail
                                </button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{-- <div class="table-responsive">
            <table id="table-penerimaan2" class="stripe table table-striped">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th class="">Kode</th>
                        <th class="">Deskripsi</th>
                        <th class="">Jumlah</th>
                        <th class="">Tgl</th>
                        <th class="">Input oleh</th>
                        <th class="">Disetujui oleh</th>
                        <th class="text-center" style="width: 15%;">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="">A.3</td>
                        <td class="">Pembayaran angsuran ke 5</td>
                        <td class="">Rp 2.000.000,00</td>
                        <td class="">23/04/2019 14:03:47</td>
                        <td class="">Dhana</td>
                        <td class="">Azzam</td>
                        <td class="text-center">
                            <a href="{{asset('img/bukti-exm.jpg')}}">
                                <button type="button" class="btn btn-sm btn-secondary">
                                    <i class="fa fa-image"></i> Bukti
                                </button>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="">A.2</td>
                        <td class="">Pembayaran DP 2</td>
                        <td class="">Rp 5.000.000,00</td>
                        <td class="">23/04/2019 14:03:47</td>
                        <td class="">Dika</td>
                        <td class="">Azzam</td>
                        <td class="text-center">
                            <a href="{{asset('img/bukti-exm.jpg')}}">
                                <button type="button" class="btn btn-sm btn-secondary">
                                    <i class="fa fa-image"></i> Bukti
                                </button>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div> --}}
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#table-penerimaan').DataTable({
                "autoWidth": true,
                "ordering": false,
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $("#nomor-kavling-penerimaan").select2({
                placeholder: "Please Select"
            });
        });
    </script>
@endsection
