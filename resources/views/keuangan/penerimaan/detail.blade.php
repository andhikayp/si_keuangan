@extends('layouts.base.app')
@section('title', 'Detail Penerimaan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="#">Dashboard</a>
    <span class="breadcrumb-item active">Laporan</span>
    <a class="breadcrumb-item" href="{{url('keuangan/penerimaan')}}">Penerimaan</a>
    <span class="breadcrumb-item active">Detail</span>
</nav>
@php
    $pemesanan = App\pemesanan::find($id);
    $kavling = App\kavling::find($pemesanan->kavlings_id);
    $transaksi = App\Transaksi::where('kode_akuns_id','!=', null)
        ->where('jenis_transaksi','=', 0)
        ->where('pemesanans_id','=', $id)
        ->where('status', '=', 1)
        ->sum('jumlah');
    $no = 1;
@endphp
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Detail Penerimaan Kavling {{ $kavling->name }}</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Harga PPJB</p>
                        <h5>
                            Rp. {{ number_format($pemesanan->harga_deal,2,',','.') }}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Total yang Sudah Diterima</p>
                        <h5>
                            Rp. {{ number_format($transaksi,2,',','.') }}
                        </h5>
                    </div>
                </div>
            </div>
            {{--<div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Total Piutang</p>
                        <h5>
                            Rp. {{ number_format($pemesanan->harga_deal-$transaksi,2,',','.') }}
                        </h5>
                    </div>
                </div>
            </div>--}}
            <div class="col-md-12">
                <a href="{{url('/pemesanan/detail/'.$pemesanan->id)}}"><button type="button" class="btn btn-sm btn-primary mb-2">
                    <i class="fa fa-book"></i> History UTJ dan Angsuran
                </button></a>
            </div>
        </div>
        <hr>        
        <div class="table-responsive">                            
            <table class="stripe table table-stripped">
                <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th>Kode Akun</th>
                        <th>Nama Kode Akun</th>
                        <th>Jumlah yang Sudah Diterima</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transaksi_all as $transaksis)
                    <tr>
                        <td class="text-center">
                            @php
                                echo $no;
                                $no++;
                            @endphp  
                        </td>
                        <td>
                            @php
                                $kode_akun = App\KodeAkun::find($transaksis->kode_akuns_id);
                            @endphp
                            {{ $kode_akun->kode_akun }}
                        </td>
                        <td>{{ $kode_akun->nama_akun }}</td>
                        <td>
                            Rp. {{ number_format($transaksis->jumlah,2,',','.') }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
@endsection