@extends('layouts.base.app')
@section('title', 'Laba Rugi')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Keuangan</span>
    <span class="breadcrumb-item active">Laba Rugi</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Laporan Laba Rugi</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
    <div class="block-content block-content-full">         
        <div class="table-responsive">                            
            <table class="table table-bordered table-vcenter"> 
                <tbody>
                    <tr>
                        <th colspan="1"></th>
                        <td class="text-right">Saldo</td>
                    </tr>

                    <!-- pendapatan -->
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9">Pendapatan</td>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">Pendapatan dari Penjualan</td>
                        <th colspan="1"></th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">Total Pendapatan dari Penjualan</td>
                        <th colspan="1" class="text-right">Rp 0,00</th>
                    </tr>
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9">Harga Pokok Penjualan</td>
                    </tr>
                    <!-- end pendapatan -->

                    <!-- harga pokok penjualan -->
                    <tr>
                        <td colspan="1" style="padding-left:5%">Harga Pokok Penjualan</td>
                        <th colspan="1"></th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">Total Beban</td>
                        <th colspan="1" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- end harga pokok penjualan -->

                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Laba Kotor</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">Rp 0,00</th>
                    </tr>
                    <tr>
                        <td colspan="2" style="background-color: #d9d9d9" >Beban</td>
                    </tr>

                    <!-- Beban -->
                    <tr>
                        <td colspan="1" style="padding-left:5%">Beban</td>
                        <th colspan="1"></th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">Total Beban</td>
                        <th colspan="1" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- end harga pokok penjualan -->

                    <!-- TOTAL-->
                    <br>
                    <tr>
                        <td colspan="1" style="background-color: #f2f248">Pendapatan Bersih</td>
                        <th colspan="1" style="background-color: #f2f248" class="text-right">Rp 0,00</th>
                    </tr>
                    <!-- end harga pokok penjualan -->
                </tbody>
            </table>
        </div>  
    </div>
    </div>
</div>
@endsection