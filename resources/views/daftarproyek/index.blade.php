@extends('layouts.base.app2')
@section('title', 'Dashboard')
@section('header')
<!-- Header -->
<header id="page-header">
    <!-- Header Content -->
    <div class="content-header">
        <!-- Left Section -->
        <div class="content-header-section">
        </div>
        <!-- END Left Section -->

        <!-- Right Section -->
        <div class="content-header-section">
            <!-- User Dropdown -->
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user d-sm-none"></i>
                    <span class="d-none d-sm-inline-block"></span>
                    <i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-200" aria-labelledby="page-header-user-dropdown">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">User</h5>
                    <a class="dropdown-item" href="{{url('/user/detail/'.auth()->user()->id)}}">
                        <i class="si si-user mr-5"></i> Profil
                    </a>
                    <!--<div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="">
                        <i class="si si-wrench mr-5"></i> Ganti Password
                    </a> -->
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{url('log-out')}}">
                        <i class="si si-logout mr-5"></i> Sign Out
                    </a>
                </div>
            </div>
            <!-- END User Dropdown -->
        </div>
        <!-- END Right Section -->
    </div>
    <!-- END Header Content -->


    <!-- Header Loader -->
    <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>
    <!-- END Header Loader -->
</header>
<!-- END Header -->
@endsection

@section('content')
<div class="my-50 text-center">
    <h2 class="font-w700 text-black mb-10">
        <i class="fa fa-building text-muted mr-5"></i> Proyek
    </h2>
    @role('Admin|Manajer|Komisaris')
    <a href="" data-toggle="modal" data-target="#modal-top2">
        <button type="button" class="btn btn-sm btn-primary">
            <i class="fa fa-plus mr-2"></i>Tambah Proyek
        </button>
    </a>
    @endrole
</div>
<div class="col-12 mb-2 mt-2">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach  
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <strong>Sukses!</strong> {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('message-error'))
        <div class="alert alert-danger" role="alert">
            <strong>Maaf!</strong> {{ session()->get('message-error') }}
        </div>
    @endif
</div>
<div class="block block-themed block-rounded">
        <div class="block-header bg-gd-lake">
            <h3 class="block-title">Manajemen Proyek</h3>
        </div>
        <div class="block-content block-content-full">
            <div class="table-responsive">                            
                <table id="table-pesan" class="stripe table table-striped table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th class="text-center d-sm-table-cell" style="width: 10px;">No</th>
                            <th class="d-sm-table-cell">Proyek</th>
                            <th class="d-sm-table-cell">Jumlah Kavling</th>
                            <th class="d-sm-table-cell">Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $no = 1;
                        @endphp
                        @foreach($proyek as $proyeks)
                            <tr>
                                <td class="text-center">
                                    {{$no}}
                                    @php 
                                        $no++;
                                    @endphp
                                </td>
                                {{-- <td><a href="{{url('/home/'.$proyeks->id)}}">{{$proyeks->name}}</a></td> --}}
                                <td>
                                    <form method="POST" action="{{ url('/choose') }}" id="tambah-proyek">
                                        {{csrf_field()}}
                                        <input type="hidden" name="nama" value="{{$proyeks->name}}">
                                        <a href="javascript:void(0)" class="pilih-proyek">{{$proyeks->name}}</a>
                                    </form>
                                </td>
                                <td>{{$proyeks->jmlh_kavling}}</td>
                                <td>{{$proyeks->alamat}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>  
        </div>
</div>
<div class="modal fade" id="modal-top2" tabindex="-1" role="dialog" aria-labelledby="modal-top2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-top" role="document">
        <form method="POST" action="{{ url('/tambah_proyek/') }}" id="tambah-proyek">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Tambah Proyek</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="form-group row">
                            <label class="col-12" for="nama-pemesan-input">Nama Proyek</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="proyek" name="proyek" placeholder="Nama Proyek" required> 
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" for="nama-pemesan-input">Alamat</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="alamat_proyek" name="alamat_proyek" placeholder="Alamat" required> 
                            </div>
                        </div>           
                        <div class="form-group row">
                            <label class="col-12" for="nama-pemesan-input">Jumlah Kavling</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="jumlah_kavling" name="jumlah_kavling" placeholder="Jumlah Kavling" required> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" id="terima" class="btn btn-alt-success">
                        <i class="fa fa-check"></i> Submit
                    </button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
{{-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
<script>
    $(document).ready(function(){
        $('#table-pesan').DataTable({
            "autoWidth": true,
            "ordering": false,
        });
        $('#terima').click(function(e){
            e.preventDefault();
            var form = $(this).parents('form');
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan membuat Proyek baru?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, buat!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                    if(result.value){
                        form.submit();
                    }
            });
        });
        $('.pilih-proyek').click(function(e){
            e.preventDefault();
            $(this).parents('form').submit();
        });
    });
</script>
@endsection