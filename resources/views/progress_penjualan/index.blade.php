@extends('layouts.base.app')
@section('title', ' Dashboard')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item active" href="#">Dashboard</a>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Dashboard</h3>
        <div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <!-- Row #2 -->
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default border-b">
                        <h3 class="block-title">
                            Penjualan <small>Minggu Ini</small>
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="pull-all">
                            {{-- <canvas class="js-chartjs-dashboard-lines" id="#mingguan"></canvas>                     --}}
                            <canvas id="mingguan"></canvas>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default border-b">
                        <h3 class="block-title">
                            Penjualan <small>Bulan Ini</small>
                        </h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                <i class="si si-refresh"></i>
                            </button>
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="pull-all">
                            <!-- Lines Chart Container functionality is initialized in js/pages/be_pages_dashboard.min.js which was auto compiled from _es6/pages/be_pages_dashboard.js -->
                            <!-- For more info and examples you can check out http://www.chartjs.org/docs/ -->
                            <canvas id="bulanan"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Row #2 -->
        </div>
        <div class="table-responsive">                            
            <table class="table table-sm">
                <tbody>
                    <!-- pendapatan hari ini-->
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Penjualan Hari Ini</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">
                            @if(isset($penjualan7[0]->tgl_pesan))
                                @if($penjualan7[0]->tgl_pesan == \Carbon\Carbon::today()->toDateString())
                                {{$penjualan7[0]->total}}
                                @else
                                0
                                @endif
                            @else
                            0
                            @endif
                             unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="padding-left:5%">{{\Carbon\Carbon::today()->format('d, M Y')}}</td>
                        <th colspan="1" class="text-right">
                            @if(isset($penjualan7[0]->tgl_pesan))
                                @if($penjualan7[0]->tgl_pesan == \Carbon\Carbon::today()->toDateString())
                                {{$penjualan7[0]->total}}
                                @else
                                0
                                @endif
                            @else
                            0
                            @endif
                             unit
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Penjualan Minggu Ini</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">
                            @if(isset($penjualan7[0]->total))
                            {{collect($penjualan7)->sum('total')}}
                            @else
                            0
                            @endif
                             unit
                        </th>
                    </tr>
                    @for($i=0;$i<7;$i++)
                    <tr>
                        <td colspan="1" style="padding-left:5%">{{\Carbon\Carbon::today()->subDays($i)->format('d, M Y')}}</td>
                        <th colspan="1" class="text-right">
                            @if(isset($penjualan7[0]->total))
                            @php
                            $date = \Carbon\Carbon::today()->subDays($i)->toDateString();
                            $ada = 0;
                            foreach($penjualan7 as $jual){
                                if($jual->tgl_pesan == $date){
                                    $ada = 1;
                                    echo($jual->total);
                                }
                            }
                            if($ada == 0){
                                echo('0');
                            }   
                            @endphp
                            @else
                            0
                            @endif
                             unit
                        </th>
                    </tr>
                    @endfor
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Penjualan Bulan Ini</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">
                        @if(isset($penjualan30[0]->total))
                        {{collect($penjualan30)->sum('total')}}
                        @else
                        0
                        @endif
                         unit
                        </th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Total Penjualan</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">{{$penjualan}} unit</th>
                    </tr>
                    <tr>
                        <td colspan="1" style="background-color: #d9d9d9">Booking</td>
                        <th colspan="1" class="text-right "style="background-color: #d9d9d9">{{$booking}} unit</th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<!-- Page JS Plugins -->
{{-- <script src="{{asset('codebase/src/assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>

<!-- Page JS Code -->
{{-- <script src="{{asset('codebase/src/assets/js/pages/be_pages_dashboard.min.js')}}"></script> --}}

<script>
// var url = "{{url('stock/chart')}}";
// var Years = new Array();
// var Labels = new Array();
// var Prices = new Array();
$(document).ready(function(){
    // $.get(url, function(response){
    //     response.forEach(function(data){
    //         Years.push(data.stockYear);
    //         Labels.push(data.stockName);
    //         Prices.push(data.stockPrice);
    //     });
        var ctx = document.getElementById('mingguan').getContext('2d');
            var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels:[
                    @for($i=6;$i>=0;$i--)
                    '{{\Carbon\Carbon::today()->subDays($i)->toDateString()}}',
                    @endfor
                ],
                datasets: [{
                    label: 'Unit Penjualan',
                    data: [
                        @php
                        for($i=6;$i>=0;$i--){
                            $date = \Carbon\Carbon::today()->subDays($i)->toDateString();
                            $ada = 0;
                            foreach($penjualan7 as $jual){
                                if($jual->tgl_pesan == $date){
                                    $ada = 1;
                                    echo($jual->total.',');
                                }
                            }
                            if($ada == 0){
                                echo('0,');
                            }
                        }
                        @endphp
                    ],
                    backgroundColor: 'rgba(54, 162, 235, 0.2)',
                    borderColor:'rgba(54, 162, 235, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            stepSize:1
                        },
                        gridLines: {
                            display:true
                        }
                    }]
                }
            }
        });
        var ctx = document.getElementById('bulanan').getContext('2d');
            var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels:[
                    @for($i=29;$i>=0;$i--)
                    '{{\Carbon\Carbon::today()->subDays($i)->toDateString()}}',
                    @endfor
                ],
                datasets: [{
                    label: 'Unit Penjualan',
                    data: [
                        @php
                        for($i=29;$i>=0;$i--){
                            $date = \Carbon\Carbon::today()->subDays($i)->toDateString();
                            $ada = 0;
                            foreach($penjualan30 as $jual){
                                if($jual->tgl_pesan== $date){
                                    $ada = 1;
                                    echo($jual->total.',');
                                }
                            }
                            if($ada == 0){
                                echo('0,');
                            }
                        }
                        @endphp
                    ],
                    backgroundColor:'rgba(255, 99, 132, 0.2)',
                    borderColor:'rgba(255,99,132,1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            stepSize:1
                        },
                        gridLines: {
                            display:true
                        }
                    }]
                }
            }
        });
    // });
});
</script>
@endsection