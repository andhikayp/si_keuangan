@extends('layouts.base.app')
@section('title', 'Approval Pembatalan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
    <link rel="stylesheet" href="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Approval Pembatalan</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Approval Pembatalan</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">   
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist" style="text-align:center;">
            <li class="nav-item col-6" style="padding:0;">
                <a class="nav-link active" href="#batal-pesan">Batal Pesan</a>
            </li>
            <li class="nav-item col-6" style="padding:0">
                <a class="nav-link" href="#batal-beli">Batal Beli</a>
            </li>
        </ul>
        <div class="block-content tab-content overflow-hidden">
            <div class="tab-pane fade show active" id="batal-pesan" role="tabpanel">
                <!--konten approval kuitansi-->
                <div class="table-responsive">
                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="table-pesan">
                        <thead>
                            <tr>
                                <th class="text-center d-sm-table-cell" style="width: 10px;"></th>
                                <th style="width: 7%;" class="d-sm-table-cell">Kavling</th>
                                <th style="width: 10%" class="d-sm-table-cell">Pemesan</th>
                                <th style="width: 10%" class="d-sm-table-cell">Tgl</th>
                                <th style="width: 10%" class="d-sm-table-cell">Admin</th>
                                <th style="width: 10%" class="d-sm-table-cell">Status</th>
                                <th class="text-center d-sm-table-cell">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no=1
                            @endphp
                            @foreach ($batalPesans as $batalPesan)   
                            <tr>
                                <td class="text-center">{{$no++}}</td>
                                <td class="font-w600">
                                    {{$batalPesan->pemesanans->kavlings->name}}
                                </td>
                                <td class=" d-sm-table-cell">
                                    {{$batalPesan->pemesanans->buyer}}
                                </td>
                                <td class=" d-sm-table-cell">{{$batalPesan->tgl_pembatalan}}</td>
                                <td class=" d-sm-table-cell">{{$batalPesan->penginput->name}}</td>
                                <td class=" d-sm-table-cell text-center">
                                    @if ($batalPesan->status == PENDING)
                                        <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                    @elseif ($batalPesan->status == DISETUJUI)
                                        <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    @elseif ($batalPesan->status == DITOLAK)
                                        <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    @endif
                                </td>
                                <td class="text-center d-sm-table-cell">
                                    <a href="{{url('/pembatalan_pesan/detail/'.$batalPesan->id)}}"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Detail">
                                        <i class="fa fa-book"></i> Detail
                                    </button></a>
                                    @if($batalPesan->status == PENDING)
                                    <a href="javascript:void(0)" id="inacc{{$batalPesan->id}}">
                                        <form method="post" action="{{action('ApprovalController@terimapembatalan')}}" id="inacckwt{{$batalPesan->id}}" style="display: none">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$batalPesan->id}}" name="id-batal">
                                            <input type="hidden" value="{{$batalPesan->pemesanans->kavlings_id}}" name="id-kavling">
                                        </form>
                                        <button type="button" class="btn btn-sm btn-success mb-2" data-toggle="tooltip" title="Terima">
                                            <i class="fa fa-check"></i> Terima
                                        </button>
                                    </a>
                                    <a href="javascript:void(0)" id="indec{{$batalPesan->id}}">
                                        <form method="post" action="{{action('ApprovalController@tolakpembatalan')}}" id="indeckwt{{$batalPesan->id}}" style="display: none">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$batalPesan->id}}" name="id-batal">
                                            <input type="hidden" value="{{$batalPesan->pemesanans->id}}" name="id-pesan">
                                        </form>
                                        <button type="button" id="tolak" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Tolak">
                                            <i class="fa fa-times"></i> Tolak
                                        </button>
                                    </a>
                                    @endif
                                    @if($batalPesan->status != PENDING)
                                    <a href="javascript:void(0)" id="inund{{$batalPesan->id}}">
                                        <form method="post" action="{{action('ApprovalController@batalpembatalan')}}" id="inundkwt{{$batalPesan->id}}" style="display: none">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$batalPesan->id}}" name="id-batal">
                                            <input type="hidden" value="{{$batalPesan->pemesanans->kavlings_id}}" name="id-kavling">
                                            <input type="hidden" value="{{$batalPesan->pemesanans->id}}" name="id-pesan">
                                        </form>
                                        <button type="button" id="batal" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Batalkan">
                                            <i class="fa fa-times"></i> Batal
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end konten-->
            </div>
            <div class="tab-pane fade" id="batal-beli" role="tabpanel">
                <!--konten approval kuitansi-->
                <div class="table-responsive">
                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="table-beli">
                        <thead>
                            <tr>
                                <th class="text-center d-sm-table-cell" style="width: 10px;"></th>
                                <th style="width: 7%;" class="d-sm-table-cell">Kavling</th>
                                <th style="width: 10%" class="d-sm-table-cell">Pembeli</th>
                                <th style="width: 10%" class="d-sm-table-cell">Tgl</th>
                                <th class="d-sm-table-cell" style="width: 10%;">Admin</th>
                                <th class="d-sm-table-cell" style="width: 10%;">Jumlah Pengembalian</th>
                                <th class="d-sm-table-cell" style="width: 10%;">Status</th>
                                <th class="text-center d-sm-table-cell">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no=1
                            @endphp
                            @foreach ($batalBelis as $batalBeli)    
                                <tr>
                                    <td class="text-center">{{$no++}}</td>
                                    <td class="font-w600">{{$batalBeli->pemesanans->kavlings->name}}</td>
                                    <td class=" d-sm-table-cell">{{$batalBeli->pemesanans->buyer}}</td>
                                    <td class=" d-sm-table-cell">{{$batalBeli->tgl_pembatalan}}</td>
                                    <td class=" d-sm-table-cell">{{$batalBeli->penginput->name}}</td>
                                    <td class=" d-sm-table-cell">Rp {{number_format($batalBeli->jumlah_dikembalikan),0,',','.'}}</td>
                                    <td class=" d-sm-table-cell text-center">
                                        @if ($batalBeli->status == PENDING)
                                            <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                        @elseif ($batalBeli->status == DISETUJUI)
                                            <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                        @elseif ($batalBeli->status == DITOLAK)
                                            <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                        @endif
                                    </td>
                                    <td class="text-center d-sm-table-cell">
                                        <a href="{{url('/pembatalan_beli/detail/'.$batalBeli->id)}}"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Detail">
                                            <i class="fa fa-book"></i> Detail
                                        </button></a>
                                        @if($batalBeli->status == PENDING)
                                            <a href="javascript:void(0)" id="outacc{{$batalBeli->id}}">
                                                <form method="post" action="{{action('ApprovalController@terimapembatalan')}}" id="outacckwt{{$batalBeli->id}}" style="display: none">
                                                    {{csrf_field()}}
                                                    <input type="hidden" value="{{$batalBeli->id}}" name="id-batal">
                                                    <input type="hidden" value="{{$batalBeli->pemesanans->kavlings_id}}" name="id-kavling">
                                                </form>
                                                <button type="button" class="btn btn-sm btn-success mb-2" data-toggle="tooltip" title="Terima">
                                                    <i class="fa fa-check"></i> Terima
                                                </button>
                                            </a>
                                            <a href="javascript:void(0)" id="outdec{{$batalBeli->id}}">
                                                <form method="post" action="{{action('ApprovalController@tolakpembatalan')}}" id="outdeckwt{{$batalBeli->id}}" style="display: none">
                                                    {{csrf_field()}}
                                                    <input type="hidden" value="{{$batalBeli->id}}" name="id-batal">
                                                    <input type="hidden" value="{{$batalBeli->pemesanans->id}}" name="id-pesan">
                                                </form>
                                                <button type="button" id="tolak" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Tolak">
                                                    <i class="fa fa-times"></i> Tolak
                                                </button>
                                            </a>
                                        @endif
                                        @if($batalBeli->status != PENDING)
                                            <a href="javascript:void(0)" id="outund{{$batalBeli->id}}">
                                                <form method="post" action="{{action('ApprovalController@batalpembatalan')}}" id="outundkwt{{$batalBeli->id}}" style="display: none">
                                                    {{csrf_field()}}
                                                    <input type="hidden" value="{{$batalBeli->id}}" name="id-batal">
                                                    <input type="hidden" value="{{$batalBeli->pemesanans->kavlings_id}}" name="id-kavling">
                                                    <input type="hidden" value="{{$batalBeli->pemesanans->id}}" name="id-pesan">
                                                </form>
                                                <button type="button" id="batal" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Batalkan">
                                                    <i class="fa fa-times"></i> Batal
                                                </button>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end konten-->
            </div>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<!-- Page JS Plugins -->
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#table-pesan').DataTable({
            "autoWidth": true,
            "ordering": false,
        });
        $('#table-beli').DataTable({
            "autoWidth": true,
            "ordering": false,
        });
        @foreach($batalPesans as $batalPesan)
        $('#table-pesan').on('click','#inacc{{$batalPesan->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menerima Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, terima!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                    if (result.value) {
                        $('#inacckwt{{$batalPesan->id}}').submit();
                    }
                })
        });
        $('#table-pesan').on('click','#indec{{$batalPesan->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menolak Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, tolak!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#indeckwt{{$batalPesan->id}}').submit();
                }
                })
        });
        $('#table-pesan').on('click','#inund{{$batalPesan->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan membatalkan Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, batalkan!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#inundkwt{{$batalPesan->id}}').submit();
                }
                })
        });
        @endforeach
        @foreach($batalBelis as $batalBeli)
        $('#table-beli').on('click','#outacc{{$batalBeli->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menerima Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, terima!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#outacckwt{{$batalBeli->id}}').submit();
                }
                })
        });
        $('#table-beli').on('click','#outdec{{$batalBeli->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menolak Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, tolak!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#outdeckwt{{$batalBeli->id}}').submit();
                }
                })
        });
        $('#table-beli').on('click','#outund{{$batalBeli->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan membatalkan Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, batalkan!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#outundkwt{{$batalBeli->id}}').submit();
                }
                })
        });
        @endforeach
    });
</script>
@endsection