@extends('layouts.base.app')
@section('title', 'Approval Surat')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
    <link rel="stylesheet" href="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message'))
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Approval Surat</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Approval Surat</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        <!--konten approval kuitansi-->
        <div class="table-responsive">
            <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            <table class="table table-bordered table-striped table-vcenter" id="table-surat">
                <thead>
                    <tr>
                        <th class="text-center d-sm-table-cell" style="width: 10px;"></th>
                        <th style="width: 7%;" class="d-sm-table-cell">Kavling</th>
                        <th style="width: 10%" class="d-sm-table-cell">Buyer</th>
                        <th style="width: 10%" class="d-sm-table-cell">Tgl</th>
                        <th style="width: 10%" class="d-sm-table-cell">Oleh</th>
                        <th style="width: 10%" class="d-sm-table-cell">Status</th>
                        <th class="text-center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $no = 1
                    @endphp
                    @foreach ($daftar as $belum)
                    <tr>
                        <td class="text-center">{{$no++}}</td>
                        <td class="font-w600">{{$belum->kavlings->name}}</td>
                        <td class=" d-sm-table-cell">{{$belum->buyer}}</td>
                        <td class=" d-sm-table-cell">{{$belum->tgl_pesan}}</td>
                        <td class=" d-sm-table-cell">{{$belum->penginput->name}}</td>
                        <td class="text-center d-sm-table-cell">
                            @if ($belum->status_ppjb == PENDING)
                                <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                            @elseif ($belum->status_ppjb == DISETUJUI)
                                <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                            @elseif ($belum->status_ppjb == DITOLAK)
                                <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                            @endif
                        </td>
                        <td class="text-center">
                            {{-- <a href="{{url('ppjb/pdf/'.$belum->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                        <i class="fa fa-book"></i> Kwitansi
                                    </button></a> --}}
                            {{-- <a href="{{asset('storage/'.$belum->ppjb)}}" target="pdf-frame"><button type="button" class="btn btn-sm btn-secondary mb-2" data-toggle="tooltip" title="Detail PPJB">
                                <i class="fa fa-book"></i> Detail
                            </button></a> --}}
                            <a href="{{asset('/pemesanan/detail/'.$belum->id)}}" target="pdf-frame"><button type="button" class="btn btn-sm btn-secondary mb-2" data-toggle="tooltip" title="Detail PPJB">
                                <i class="fa fa-book"></i> Detail Pemesanan
                            </button></a>
                            @if($belum->status_ppjb == PENDING)
                            <a href="javascript:void(0)" id="terima{{$belum->id}}">
                                <form method="post" action="{{url('terimappjb')}}" id="terimappjb{{$belum->id}}" style="display: none">
                                    {{csrf_field()}}
                                    <input type="hidden" value="{{$belum->id}}" name="ppjb">
                                </form>
                                <button type="button" class="btn btn-sm btn-success mb-2" data-toggle="tooltip" title="Terima">
                                    <i class="fa fa-check"></i> Terima
                                </button>
                            </a>
                            <a href="javascript:void(0)" id="tolak{{$belum->id}}">
                                <form method="post" action="{{url('tolakppjb')}}" id="tolakppjb{{$belum->id}}" style="display: none">
                                        {{csrf_field()}}
                                    <input type="hidden" value="{{$belum->id}}" name="ppjb">
                                </form>
                                <button type="button" id="tolak" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Tolak">
                                    <i class="fa fa-times"></i> Tolak
                                </button>
                            </a>
                            @endif
                            @if($belum->status_ppjb != PENDING)
                            <a href="javascript:void(0)" id="batal{{$belum->id}}">
                                <form method="post" action="{{url('batalppjb')}}" id="batalppjb{{$belum->id}}" style="display: none">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$belum->id}}" name="ppjb">
                                </form>
                                <button type="button" id="batal" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Batalkan">
                                    <i class="fa fa-times"></i> Batal
                                </button>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!--end konten-->
    </div>
</div>
@endsection
@section('moreJS')
        <!-- Page JS Plugins -->
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('#table-surat').DataTable({
                    "autoWidth": true,
                    "ordering": false,
                });
                @foreach($daftar as $belum)
                $('#table-surat').on('click','#terima{{$belum->id}}',function(){
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Apakah anda yakin akan menerima PPJB ini?",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, terima!',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            $('#terimappjb{{$belum->id}}').submit();
                        }
                        })
                });
                $('#table-surat').on('click','#tolak{{$belum->id}}',function(){
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Apakah anda yakin akan menolak PPJB ini?",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, tolak!',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            $('#tolakppjb{{$belum->id}}').submit();
                        }
                        })
                });
                $('#table-surat').on('click','#batal{{$belum->id}}',function(){
                    Swal.fire({
                        title: 'Apakah anda yakin?',
                        text: "Apakah anda yakin akan membatalkan PPJB ini?",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya, batalkan!',
                        cancelButtonText: 'Tidak'
                        }).then((result) => {
                        if (result.value) {
                            $('#batalppjb{{$belum->id}}').submit();
                        }
                        })
                });
            @endforeach
            });
        </script>
@endsection
