@extends('layouts.base.app')
@section('title', 'Approval Kuitansi')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
    <link rel="stylesheet" href="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.css')}}">
@endsection

@section('content')
<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <span class="breadcrumb-item active">Approval Kuitansi</span>
</nav>
<div class="col-12 mb-2 mt-2">
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success" role="alert">
                <strong>Sukses!</strong> {{ session()->get('message') }}
            </div>
        @endif
    </div>

<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Approval Kuitansi</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content block-content-full">
        <ul class="nav nav-tabs nav-tabs-alt" data-toggle="tabs" role="tablist" style="text-align:center;">
            <li class="nav-item col-6" style="padding:0;">
                <a class="nav-link active" href="#penerimaan">Penerimaan</a>
            </li>
            <li class="nav-item col-6" style="padding:0">
                <a class="nav-link" href="#pengeluaran">Pengeluaran</a>
            </li>
        </ul>
        <div class="block-content tab-content overflow-hidden">
            <div class="tab-pane fade show active" id="penerimaan" role="tabpanel">
                <!--konten approval kuitansi-->
                <div class="table-responsive">
                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter" id="tabel-penerimaan">
                        <thead>
                            <tr>
                                <th class="text-center d-sm-table-cell" style="width: 10px;"></th>
                                <th class="d-sm-table-cell">Income</th>
                                <th style="width: 10%" class="d-sm-table-cell">Tgl</th>
                                <th class="d-sm-table-cell">Admin</th>
                                <th class="d-sm-table-cell" style="width: 10%;">Akun</th>
                                <th class="d-sm-table-cell" style="width: 10%;">Jumlah</th>
                                <th class="d-sm-table-cell" style="width: 10%;">Status</th>
                                <th class="text-center d-sm-table-cell">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no=1
                            @endphp
                            @foreach ($in as $item)
                            <tr>
                                <td class="text-center">{{$no++}}</td>
                                <td class="font-w600">
                                    @if($item->pemesanans)
                                    Kavling : {{$item->pemesanans->kavlings->name}}
                                    @elseif($item->investors)
                                    Investor : {{$item->investors->users->name}}
                                    @endif
                                </td>
                                <td class=" d-sm-table-cell">{{$item->tgl_transaksi}}</td>
                                <td class=" d-sm-table-cell">{{$item->penginput->name}}</td>
                                <td class=" d-sm-table-cell">
                                    @if($item->pemesanans)
                                    <a href="{{url('/pemesanan/detail/'.$item->pemesanans->id)}}"><span class="badge badge-warning">{{$item->kode_akuns->nama_akun}}</span></a>
                                    @elseif($item->investors)
                                    <a href="{{url('/investor/detail/'.$item->investors->id)}}"><span class="badge badge-warning">{{$item->kode_akuns->nama_akun}}</span></a>
                                    @endif
                                </td>
                                <td class=" d-sm-table-cell">Rp {{number_format($item->jumlah),0,',','.'}}</td>
                                <td class="d-sm-table-cell text-center">
                                    @if ($item->status == PENDING)
                                        <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                    @elseif ($item->status == DISETUJUI)
                                        <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    @elseif ($item->status == DITOLAK)
                                        <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    @endif
                                </td>
                                <td class="text-center d-sm-table-cell">
                                    @if($item->pemesanans)
                                    <a href="{{url('kwitansi/'.$item->id)}}" target="_blank"><button type="button" class="btn btn-sm btn-primary mb-2" data-toggle="tooltip" title="Cetak Kwitansi">
                                        <i class="fa fa-book"></i> Kwitansi
                                    </button></a>
                                    @endif
                                    {{-- <a href="{{asset('storage/'.$item->bukti)}}"><button type="button" class="btn btn-sm btn-secondary mb-2" data-toggle="tooltip" title="Detail Pesanan">
                                        <i class="fa fa-book"></i> Detail
                                    </button></a> --}}
                                    @if($item->status < 1)
                                    <a href="javascript:void(0)" id="inacc{{$item->id}}">
                                        <form method="post" action="{{url('terimakwt')}}" id="inacckwt{{$item->id}}" style="display: none">
                                            {{csrf_field()}}
                                            <input type="hidden" value="{{$item->id}}" name="kwt">
                                        </form>
                                        <button type="button" class="btn btn-sm btn-success mb-2" data-toggle="tooltip" title="Terima">
                                            <i class="fa fa-check"></i> Terima
                                        </button>
                                    </a>
                                    <a href="javascript:void(0)" id="indec{{$item->id}}">
                                        <form method="post" action="{{url('tolakkwt')}}" id="indeckwt{{$item->id}}" style="display: none">
                                                {{csrf_field()}}
                                            <input type="hidden" value="{{$item->id}}" name="kwt">
                                        </form>
                                        <button type="button" id="tolak" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Tolak">
                                            <i class="fa fa-times"></i> Tolak
                                        </button>
                                    </a>
                                    @endif
                                    @if($item->status > 0)
                                    <a href="javascript:void(0)" id="inund{{$item->id}}">
                                        <form method="post" action="{{url('batalkwt')}}" id="inundkwt{{$item->id}}" style="display: none">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$item->id}}" name="kwt">
                                        </form>
                                        <button type="button" id="batal" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Batalkan">
                                            <i class="fa fa-times"></i> Batal
                                        </button>
                                    </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!--end konten-->
            </div>
            <div class="tab-pane fade" id="pengeluaran" role="tabpanel">
                <!--konten approval kuitansi-->
                <div class="table-responsive">
                        <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                        <table class="table table-bordered table-striped table-vcenter" id="tabel-pengeluaran">
                            <thead>
                                <tr>
                                    <th class="text-center d-sm-table-cell" style="width: 10px;">#</th>
                                    <th style="width: 7%;" class="d-sm-table-cell">Expense</th>
                                    <th style="width: 10%" class="d-sm-table-cell">Akun</th>
                                    <th style="width: 10%" class="d-sm-table-cell">Tgl</th>
                                    <th class="d-sm-table-cell">Admin</th>
                                    <th class="d-sm-table-cell" style="width: 10%;">Harga</th>
                                    <th class="d-sm-table-cell" style="width: 10%">Status</th>
                                    <th class="text-center d-sm-table-cell">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no=1
                                @endphp
                                @foreach ($out as $item)
                                <tr>
                                    <td class="text-center">{{$no++}}</td>
                                    <td class="font-w600">{{$item->deskripsi}}</td>
                                    <td class=" d-sm-table-cell">{{substr($item->kode_akuns->kode_akun,1)}}. {{$item->kode_akuns->nama_akun}}</td>
                                    <td class=" d-sm-table-cell">{{$item->tgl_transaksi}}</td>
                                    <td class=" d-sm-table-cell">{{$item->penginput->name}}</td>
                                    <td class=" d-sm-table-cell">Rp {{number_format($item->jumlah),0,',','.'}}</td>
                                    <td class="d-sm-table-cell text-center">
                                        @if ($item->status == PENDING)
                                            <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                        @elseif ($item->status == DISETUJUI)
                                            <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                        @elseif ($item->status == DITOLAK)
                                            <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                        @endif
                                    </td>
                                    <td class="text-center d-sm-table-cell">
                                        <a href="{{asset('storage/'.$item->bukti)}}"><button type="button" class="btn btn-sm btn-secondary mb-2" data-toggle="tooltip" title="Detail Pesanan">
                                            <i class="fa fa-book"></i> Detail
                                        </button></a>
                                        @if($item->status < 1)
                                        <a href="javascript:void(0)" id="outacc{{$item->id}}">
                                            <form method="post" action="{{url('terimakwt')}}" id="outacckwt{{$item->id}}" style="display: none">
                                                {{csrf_field()}}
                                                <input type="hidden" value="{{$item->id}}" name="kwt">
                                            </form>
                                            <button type="button" class="btn btn-sm btn-success mb-2" data-toggle="tooltip" title="Terima">
                                                <i class="fa fa-check"></i> Terima
                                            </button>
                                        </a>
                                        <a href="javascript:void(0)" id="outdec{{$item->id}}">
                                            <form method="post" action="{{url('tolakkwt')}}" id="outdeckwt{{$item->id}}" style="display: none">
                                                    {{csrf_field()}}
                                                <input type="hidden" value="{{$item->id}}" name="kwt">
                                            </form>
                                            <button type="button" id="tolak" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Tolak">
                                                <i class="fa fa-times"></i> Tolak
                                            </button>
                                        </a>
                                        @endif
                                        @if($item->status > 0)
                                        <a href="javascript:void(0)" id="outund{{$item->id}}">
                                            <form method="post" action="{{url('batalkwt')}}" id="outundkwt{{$item->id}}" style="display: none">
                                                    {{csrf_field()}}
                                                    <input type="hidden" value="{{$item->id}}" name="kwt">
                                            </form>
                                            <button type="button" id="batal" class="btn btn-sm btn-danger mb-2" data-toggle="tooltip" title="Batalkan">
                                                <i class="fa fa-times"></i> Batal
                                            </button>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end konten-->
            </div>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
<!-- Page JS Plugins -->
<script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#tabel-penerimaan').DataTable({
            "autoWidth": true,
        });
        $('#tabel-pengeluaran').DataTable({
            "autoWidth": true,
        });
        @foreach($in as $item)
        $('#tabel-penerimaan').on('click','#inacc{{$item->id}}',function(){
            console.log('masuk');
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menerima Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, terima!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#inacckwt{{$item->id}}').submit();
                }
                })
        });
        $('#tabel-penerimaan').on('click','#indec{{$item->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menolak Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, tolak!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#indeckwt{{$item->id}}').submit();
                }
                })
        });
        $('#tabel-penerimaan').on('click','#inund{{$item->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan membatalkan Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, batalkan!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#inundkwt{{$item->id}}').submit();
                }
                })
        });
        @endforeach
        @foreach($out as $item)
        $('#tabel-pengeluaran').on('click','#outacc{{$item->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menerima Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, terima!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#outacckwt{{$item->id}}').submit();
                }
                })
        });
        $('#tabel-pengeluaran').on('click','#outdec{{$item->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan menolak Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, tolak!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#outdeckwt{{$item->id}}').submit();
                }
                })
        });
        $('#tabel-pengeluaran').on('click','#outund{{$item->id}}',function(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Apakah anda yakin akan membatalkan Kuitansi ini?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, batalkan!',
                cancelButtonText: 'Tidak'
                }).then((result) => {
                if (result.value) {
                    $('#outundkwt{{$item->id}}').submit();
                }
                })
        });
        @endforeach
    });
</script>
@endsection
