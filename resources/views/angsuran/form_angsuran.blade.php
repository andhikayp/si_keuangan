@extends('layouts.base.app')
@section('title', 'Form Pemesanan')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-md-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('/home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan')}}">Pemesanan</a>
    <span class="breadcrumb-item active">Tambah Angsuran</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">Form Angsuran</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class ="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>A-2</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Keterangan</p>
                        <h5>Pembayaran angsuran ke: 5 / 12</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pemesan</p>
                        <h5>Karin</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Harga Deal</p>
                        <h5>Rp 100.000.000,00</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Harga Angsuran Per Bulan</p>
                        <h5>Rp 12.000.000,00</h5>
                    </div>
                </div>
            </div>
        </div>
        <hr style="border-width:3px">
        <form action="be_forms_elements_bootstrap.html" method="post" enctype="multipart/form-data" onsubmit="return false;">
            <div class="form-group row">
                <label class="col-12" for="bukti-angsuran">Upload Bukti Pembayaran</label>
                <div class="col-md-9">
                    <input type="file" id="bukti-angsuran" name="bukti-angsuran">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-12" for="tanggal-angsuran">Tanggal Pembayaran</label>
                <div class="col-md-9">
                    <input type="date" class="form-control" id="tanggal-angsuran" name="tanggal-angsuran" required>
                </div>
            </div>
            <br>
            <div class="form-group row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
        
    </div>
    <!-- END DIV BLOCK -->
</div>
@endsection