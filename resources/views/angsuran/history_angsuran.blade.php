@extends('layouts.base.app')
@section('title', 'History Angsuran')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<div class="col-12 mb-2 mt-2">
    {{-- @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif --}}
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{url('home')}}">Dashboard</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan')}}">Pemesanan</a>
    <a class="breadcrumb-item" href="{{url('/pemesanan/detail')}}">Detail</a>
    <span class="breadcrumb-item active">History Angsuran</span>
</nav>
<div class="block block-themed block-rounded">
    <div class="block-header bg-gd-lake">
        <h3 class="block-title">History Angsuran</h3>
        <!--<div class="block-options">
            <button type="button" class="btn-block-option">
                <i class="si si-wrench"></i>
            </button>
        </div>-->
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Proyek</p>
                        <h5>Proyek Ciputra</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nomor Kavling</p>
                        <h5>A-1</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Diangsur Sebanyak</p>
                        <h5>12x</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Nama Pembeli</p>
                        <h5>Natalia</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-bottom:3px">Angsuran Per Bulan</p>
                        <h5>Rp 2.000.000,00</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="block-content block-content-full">       
                <div class="table-responsive">                            
                    <table id="table-angsuran" class="stripe table table-striped text-center table-bordered table-vcenter">
                        <thead>
                            <tr>
                                <th>Angsuran ke</th>
                                <th>Tanggal Pembayaran</th>
                                <th>Status</th>
                                <th>Bukti Pembayaran</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>05/07/2019</td>
                                <td>
                                    <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                </td>
                                <td>
                                    <a href="{{asset('img/bukti-exm.jpg')}}"><button type="button" class="btn btn-sm btn-secondary">
                                        <i class="fa fa-book"></i> Bukti
                                    </button></a>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>06/08/2019</td>
                                <td>
                                    <span class="badge badge-success"><i class="fa fa-check"></i> Disetujui</span>
                                    <span class="badge badge-danger"><i class="fa fa-times"></i> Ditolak</span>
                                    <span class="badge badge-primary"><i class="fa fa-spinner"></i></i> Pending</span>
                                </td>
                                <td>
                                    <a href="{{asset('img/bukti-exm.jpg')}}"><button type="button" class="btn btn-sm btn-secondary">
                                        <i class="fa fa-book"></i> Bukti
                                    </button></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection
@section('moreJS')
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script src="{{asset('codebase/src/assets/js/pages/be_tables_datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('#table-angsuran').DataTable({
                    "autoWidth": true,
                    "ordering": false,
                    "paging": false,
                    "searching": false,
                    "info": false,
                });
            });
        </script>
        @endsection