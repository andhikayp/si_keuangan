<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJurnalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurnals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('transaksis_id')->unsigned();
            $table->bigInteger('proyeks_id')->unsigned();
            $table->string('nomor_jurnal');
            $table->bigInteger('kode_akun_coas_id')->unsigned();
            $table->date('tanggal');
            $table->double('debit')->nullable();
            $table->double('kredit')->nullable();
            $table->timestamps();

            $table->foreign('transaksis_id')->references('id')->on('transaksis');
            $table->foreign('proyeks_id')->references('id')->on('proyeks');
            $table->foreign('kode_akun_coas_id')->references('id')->on('kode_akun_coas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurnals');
    }
}
