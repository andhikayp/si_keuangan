<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKavlingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kavlings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('proyeks_id')->unsigned();
            $table->string('name', 30);
            $table->double('lebar');
            $table->double('panjang');
            // $table->double('kanan');
            // $table->double('kiri');
            // $table->double('depan');
            // $table->double('belakang');
            $table->double('luas');
            $table->double('harga');
            $table->double('diskon');
            $table->double('hpp');
            $table->integer('status');
            $table->timestamps();

            $table->foreign('proyeks_id')->references('id')->on('proyeks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kavlings');
    }
}
