<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAkunCoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('akun_coas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_akun');
            $table->string('nama_akun');
            $table->bigInteger('coas_id')->unsigned();
            $table->timestamps();

            $table->foreign('coas_id')->references('id')->on('coas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('akun_coas');
    }
}
