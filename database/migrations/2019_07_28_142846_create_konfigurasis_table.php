<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKonfigurasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konfigurasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('email', 64)->nullable();
            $table->text('subjek_tenggat')->nullable();
            $table->text('isi_tenggat')->nullable();
            $table->text('subjek_peringatan')->nullable();
            $table->text('isi_peringatan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konfigurasis');
    }
}
