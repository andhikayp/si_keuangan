<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->unsigned();
            $table->bigInteger('proyeks_id')->unsigned();
            $table->date('tgl_perjanjian');
            $table->integer('jumlah_setor_modal');
            $table->string('bukti_perjanjian')->nullable();
            $table->bigInteger('penginput_id')->unsigned();
            $table->bigInteger('penyetujui_id')->nullable()->unsigned();
            //$table->integer('status');
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('proyeks_id')->references('id')->on('proyeks');
            $table->foreign('penginput_id')->references('id')->on('users');
            $table->foreign('penyetujui_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investors');
    }
}
