<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('proyeks_id')->unsigned();
            $table->bigInteger('kavlings_id')->unsigned();
            $table->string('buyer', 30);
            $table->string('alamat', 40);
            $table->string('email', 40);
            $table->string('no_ktp', 40);

            $table->bigInteger('marketing_id')->nullable()->unsigned();
            $table->integer('status_fee_marketing')->nullable();
            $table->integer('fee_marketing')->nullable();
            $table->integer('fee_diambil')->nullable();

            $table->string('hp_pribadi', 15);
            $table->string('nama_marketing', 50)->nullable();
            $table->string('hp_kerabat', 15);
            $table->integer('harga_deal');
            $table->integer('utj')->nullable();
            $table->date('utj_tgl')->nullable();
            $table->integer('dp1');
            $table->date('dp1_tgl');
            $table->integer('dp2')->nullable();
            $table->date('dp2_tgl')->nullable();
            $table->integer('dp3')->nullable();
            $table->date('dp3_tgl')->nullable();
            $table->integer('dp4')->nullable();
            $table->date('dp4_tgl')->nullable();
            $table->integer('angsur_jml');
            $table->integer('angsur');
            $table->integer('tgl_tempo');
            $table->date('tgl_angsur1');
            $table->date('tgl_angsur2');
            $table->date('tgl_pesan');
            $table->string('ppjb')->nullable();

            $table->bigInteger('penginput_id')->unsigned();
            $table->bigInteger('penyetujui_id')->nullable()->unsigned();
            $table->integer('status');
            $table->integer('status_ppjb');
            $table->integer('status_pembatalan');
            $table->timestamps();

            $table->foreign('proyeks_id')->references('id')->on('proyeks');
            $table->foreign('kavlings_id')->references('id')->on('kavlings');
            $table->foreign('marketing_id')->references('id')->on('users');
            $table->foreign('penginput_id')->references('id')->on('users');
            $table->foreign('penyetujui_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanans');
    }
}
