<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kode_akuns_id')->unsigned();
            $table->bigInteger('pemesanans_id')->nullable()->unsigned();
            $table->bigInteger('investors_id')->nullable()->unsigned();
            $table->bigInteger('pembatalans_id')->nullable()->unsigned();
            $table->bigInteger('proyeks_id')->unsigned();
            $table->integer('jenis_transaksi');
            $table->date('tgl_transaksi');
            $table->integer('jumlah');
            $table->text('deskripsi')->nullable();
            $table->string('bukti')->nullable();
            $table->bigInteger('penginput_id')->unsigned();
            $table->bigInteger('penyetujui_id')->nullable()->unsigned();
            $table->integer('status');
            $table->timestamps();

            $table->foreign('kode_akuns_id')->references('id')->on('kode_akuns');
            $table->foreign('pemesanans_id')->references('id')->on('pemesanans');
            $table->foreign('investors_id')->references('id')->on('investors');
            $table->foreign('pembatalans_id')->references('id')->on('pembatalans');
            $table->foreign('proyeks_id')->references('id')->on('proyeks');
            $table->foreign('penginput_id')->references('id')->on('users');
            $table->foreign('penyetujui_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
