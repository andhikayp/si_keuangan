<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKodeAkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kode_akuns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_akun', 10);
            $table->string('nama_akun', 64);
            $table->integer('tipe')->nullable();
            $table->bigInteger('parent')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('parent')->references('id')->on('kode_akuns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kode_akuns');
    }
}
