<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKodeAkunCoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kode_akun_coas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('kode_akuns_id')->unsigned();
            $table->bigInteger('debit')->unsigned()->nullable();
            $table->bigInteger('kredit')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('kode_akuns_id')->references('id')->on('kode_akuns');
            $table->foreign('debit')->references('id')->on('akun_coas');
            $table->foreign('kredit')->references('id')->on('akun_coas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kode_akun_coas');
    }
}
