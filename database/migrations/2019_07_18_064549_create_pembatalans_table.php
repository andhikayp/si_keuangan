<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembatalansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembatalans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pemesanans_id')->unsigned();
            $table->bigInteger('proyeks_id')->unsigned();
            $table->integer('jenis_pembatalan');
            $table->date('tgl_pembatalan');
            $table->integer('jumlah_dikembalikan')->nullable();
            $table->text('alasan_pembatalan');
            $table->bigInteger('penginput_id')->unsigned();
            $table->bigInteger('penyetujui_id')->nullable()->unsigned();
            $table->integer('status');
            $table->timestamps();

            $table->foreign('pemesanans_id')->references('id')->on('pemesanans');
            $table->foreign('proyeks_id')->references('id')->on('proyeks');
            $table->foreign('penginput_id')->references('id')->on('users');
            $table->foreign('penyetujui_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembatalans');
    }
}
