<?php

use Illuminate\Database\Seeder;
use App\Coa;
class CoasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Coa::firstOrCreate(['name' => 'Aset Lancar']);
        Coa::firstOrCreate(['name' => 'Kewajiban']);
        Coa::firstOrCreate(['name' => 'Ekuitas']);
        Coa::firstOrCreate(['name' => 'Pendapatan']);
        Coa::firstOrCreate(['name' => 'HPP']);
        Coa::firstOrCreate(['name' => 'Beban']);
    }
}
