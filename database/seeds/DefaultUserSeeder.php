<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'system',
            'telp' => '007',
            'email' => 'system@system',
            'alamat' => '007',
            'password' => bcrypt('system'),
            'aktif' => AKTIF,
        ]);

        User::find(1)->assignRole('Komisaris')->save();
    }
}
