<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::firstOrCreate(['name' => 'create-pemesanan']);
        Permission::firstOrCreate(['name' => 'view-pemesanan']);
        Permission::firstOrCreate(['name' => 'approval-kuitansi']);
        Permission::firstOrCreate(['name' => 'approval-ppjb']);

        Permission::firstOrCreate(['name' => 'approval-pembatalan']);
        Permission::firstOrCreate(['name' => 'create-kodeakun']);
        Permission::firstOrCreate(['name' => 'view-kodeakun']);
        Permission::firstOrCreate(['name' => 'create-datakavling']);

        Permission::firstOrCreate(['name' => 'view-datakavling']);
        Permission::firstOrCreate(['name' => 'create-transaksi']);
        Permission::firstOrCreate(['name' => 'view-transaksi']);
        Permission::firstOrCreate(['name' => 'create-pembatalan']);

        Permission::firstOrCreate(['name' => 'view-pembatalan']);
        Permission::firstOrCreate(['name' => 'view-laporan']);
        Permission::firstOrCreate(['name' => 'create-feemarketing']);
        Permission::firstOrCreate(['name' => 'view-feemarketing']);

        Permission::firstOrCreate(['name' => 'create-siteplan']);
        Permission::firstOrCreate(['name' => 'view-siteplan']);
        Permission::firstOrCreate(['name' => 'create-investor']);
        Permission::firstOrCreate(['name' => 'view-investor']);

        Permission::firstOrCreate(['name' => 'create-user']);
        Permission::firstOrCreate(['name' => 'view-user']);
        Permission::firstOrCreate(['name' => 'config-email']);
        Permission::firstOrCreate(['name' => 'manage-rolepermission']);
    }
}
