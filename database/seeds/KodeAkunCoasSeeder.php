<?php

use Illuminate\Database\Seeder;
use App\KodeAkun;
use App\AkunCoa;
use App\KodeAkunCoa;


class KodeAkunCoasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //PENERIMAAN
        $daftar_akun = KodeAkun::where('kode_akun','=','A1')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Pendapatan Penjualan')->pluck('id')->first(),
        ]);

        ////////////////////////////// DP
        $daftar_akun = KodeAkun::where('kode_akun','=','A2')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Pendapatan Diterima Dimuka')->pluck('id')->first(),
        ]);

        /*KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Pendapatan Diterima Dimuka')->pluck('id')->first(),
        ]);
        
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);*/
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Pendapatan Diterima Dimuka')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Piutang Usaha')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Diskon Penjualan')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Harga Pokok Penjualan')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Pendapatan Penjualan')->pluck('id')->first(),
        ]);
        //////////////////////////////

        $daftar_akun = KodeAkun::where('kode_akun','=','A3')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Piutang Usaha')->pluck('id')->first(),
        ]);

        //PENGELUARAN
        $daftar_akun = KodeAkun::where('kode_akun','like','B1.%')
                                ->orWhere('kode_akun','like','B2.%')
                                ->orWhere('kode_akun','like','B3.%')
                                ->orWhere('kode_akun','like','B4.%')->pluck('id');
        foreach($daftar_akun as $daftar){
            KodeAkunCoa::firstOrCreate([
                'kode_akuns_id' => $daftar,
                'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
            ]);
            KodeAkunCoa::firstOrCreate([
                'kode_akuns_id' => $daftar,
                'debit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
            ]);
        }
        
        $daftar_akun = KodeAkun::where('kode_akun','=','B5.1')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Tools Marketing')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B5.2')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Fee Marketing')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','like','B6.%')
                                ->orWhere('kode_akun','like','B7.%')->pluck('id');
        foreach($daftar_akun as $daftar){
            KodeAkunCoa::firstOrCreate([
                'kode_akuns_id' => $daftar,
                'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
            ]);
            KodeAkunCoa::firstOrCreate([
                'kode_akuns_id' => $daftar,
                'debit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
            ]);
        }

        $daftar_akun = KodeAkun::where('kode_akun','=','B8')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B9.1')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Gaji')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B9.2')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Sewa kantor dan set up')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B9.3')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Biaya Kantor')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B9.4')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B9.5')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B10.1')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','PBB')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B10.2')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','BPHTB')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B10.3')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','PPh')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B10.4')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Zakat')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','A4')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Hutang Usaha')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B11.1')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Hutang Usaha')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B11.2')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Bagi Hasil')->pluck('id')->first(),
        ]);

        $daftar_akun = KodeAkun::where('kode_akun','=','B12.1')->pluck('id')->first();
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Kas')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'kredit' => AkunCoa::where('nama_akun','=','Piutang Usaha')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Persediaan')->pluck('id')->first(),
        ]);
        KodeAkunCoa::firstOrCreate([
            'kode_akuns_id' => $daftar_akun,
            'debit' => AkunCoa::where('nama_akun','=','Kerugian Pembatalan')->pluck('id')->first(),
        ]);
    }
}
