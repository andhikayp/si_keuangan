<?php

use Illuminate\Database\Seeder;
use App\AkunCoa;
use App\Coa;

class AkunCoasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        AkunCoa::firstOrCreate([
            'kode_akun' => '1-10001',
            'nama_akun' => 'Kas',
            'coas_id' => Coa::where('name','=','Aset Lancar')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '1-10002',
            'nama_akun' => 'Kas di Rekening Bank',
            'coas_id' => Coa::where('name','=','Aset Lancar')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '1-10003',
            'nama_akun' => 'Piutang Usaha',
            'coas_id' => Coa::where('name','=','Aset Lancar')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '1-10004',
            'nama_akun' => 'Persediaan',
            'coas_id' => Coa::where('name','=','Aset Lancar')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '2-10101',
            'nama_akun' => 'Hutang Usaha',
            'coas_id' => Coa::where('name','=','Kewajiban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '2-10102',
            'nama_akun' => 'Pendapatan Diterima Dimuka',
            'coas_id' => Coa::where('name','=','Kewajiban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '3-10001',
            'nama_akun' => 'Modal Pemilik',
            'coas_id' => Coa::where('name','=','Ekuitas')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '3-10002',
            'nama_akun' => 'Penarikan Modal Pemilik',
            'coas_id' => Coa::where('name','=','Ekuitas')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '3-10003',
            'nama_akun' => 'Laba ditahan',
            'coas_id' => Coa::where('name','=','Ekuitas')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '4-10101',
            'nama_akun' => 'Pendapatan Penjualan',
            'coas_id' => Coa::where('name','=','Pendapatan')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '4-10102',
            'nama_akun' => 'Diskon Penjualan',
            'coas_id' => Coa::where('name','=','Pendapatan')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '4-19999',
            'nama_akun' => 'Pendapatan Lain-lain',
            'coas_id' => Coa::where('name','=','Pendapatan')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '5-10001',
            'nama_akun' => 'Harga Pokok Penjualan',
            'coas_id' => Coa::where('name','=','HPP')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10001',
            'nama_akun' => 'Gaji',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10002',
            'nama_akun' => 'Bagi Hasil',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10003',
            'nama_akun' => 'Sewa kantor dan set up',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10004',
            'nama_akun' => 'Biaya Kantor',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10005',
            'nama_akun' => 'PBB',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10006',
            'nama_akun' => 'BPHTB',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10007',
            'nama_akun' => 'PPh',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10008',
            'nama_akun' => 'Zakat',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10009',
            'nama_akun' => 'Tools Marketing',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10010',
            'nama_akun' => 'Fee Marketing',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
        AkunCoa::firstOrCreate([
            'kode_akun' => '6-10011',
            'nama_akun' => 'Kerugian Pembatalan',
            'coas_id' => Coa::where('name','=','Beban')->pluck('id')->first(),
        ]);
    }
}
