<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\KodeAkun;

class KodeakunsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'A1',
            'nama_akun' => 'UTJ',
            'tipe' => 0,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'A2',
            'nama_akun' => 'DP',
            'tipe' => 0,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'A3',
            'nama_akun' => 'Angsuran',
            'tipe' => 0,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'A4',
            'nama_akun' => 'Setor Modal',
            'tipe' => 0,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B1',
            'nama_akun' => 'Akuisisi Lahan',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B1.1',
            'nama_akun' => 'Pembayaran Lahan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B1')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B1.2',
            'nama_akun' => 'Fee Broker',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B1')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B1.3',
            'nama_akun' => 'Uang Transport',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B1')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B2',
            'nama_akun' => 'Pematangan Lahan',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B2.1',
            'nama_akun' => 'Pengukuran',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B2')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B2.2',
            'nama_akun' => 'Urungan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B2')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B2.3',
            'nama_akun' => 'Cut n Fill',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B2')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B2.4',
            'nama_akun' => 'Pematokan Kavling',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B2')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3',
            'nama_akun' => 'Legalitas dan Perijinan',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.1',
            'nama_akun' => 'Balik Nama Petok a.n. Adam Jaya',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.2',
            'nama_akun' => 'Balik Nama Petok a.n. Konsumen',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.3',
            'nama_akun' => 'Sertifikat Induk',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.4',
            'nama_akun' => 'Pecah Sertifikat',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.5',
            'nama_akun' => 'Pecah SPPT',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.6',
            'nama_akun' => 'IPL & Site Plan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.7',
            'nama_akun' => 'IMB',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B3.8',
            'nama_akun' => 'Uang Transport',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B3')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B4',
            'nama_akun' => 'Perencanaan',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B4.1',
            'nama_akun' => 'Gambar',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B4')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B4.2',
            'nama_akun' => 'Video',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B4')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B5',
            'nama_akun' => 'Marketing',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B5.1',
            'nama_akun' => 'Tools Marketing',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B5')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B5.2',
            'nama_akun' => 'Fee Marketing',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B5')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6',
            'nama_akun' => 'Prasarana',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.1',
            'nama_akun' => 'Jalan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.2',
            'nama_akun' => 'Saluran, Gorong-gorong, dan Jembatan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.3',
            'nama_akun' => 'Sumur',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.4',
            'nama_akun' => 'Jaringan Air Bersih',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.5',
            'nama_akun' => 'Sambungan Rumah Air Bersih',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.6',
            'nama_akun' => 'Jaringan Listrik',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B6.7',
            'nama_akun' => 'Sambungan Rumah Listrik',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B6')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B7',
            'nama_akun' => 'Fasum',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B7.1',
            'nama_akun' => 'Gerbang dan Pos Jaga',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B7')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B7.2',
            'nama_akun' => 'Taman',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B7')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B7.3',
            'nama_akun' => 'Penerangan Jalan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B7')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B8',
            'nama_akun' => 'Pembangunan Rumah',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B9',
            'nama_akun' => 'Biaya Umum',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B9.1',
            'nama_akun' => 'Gaji',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B9')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B9.2',
            'nama_akun' => 'Sewa Kantor dan Set Up',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B9')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B9.3',
            'nama_akun' => 'Biaya Kantor',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B9')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B9.4',
            'nama_akun' => 'Biaya Notaris',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B9')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B9.5',
            'nama_akun' => 'Biaya Lingkungan',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B9')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B10',
            'nama_akun' => 'Pajak dan Zakat',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B10.1',
            'nama_akun' => 'PBB',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B10')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B10.2',
            'nama_akun' => 'BPHTB',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B10')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B10.3',
            'nama_akun' => 'PPh',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B10')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B10.4',
            'nama_akun' => 'Zakat',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B10')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
            'kode_akun' => 'B11',
            'nama_akun' => 'Pengembalian Modal',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
            'kode_akun' => 'B11.1',
            'nama_akun' => 'Bayar Modal',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B11')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
            'kode_akun' => 'B11.2',
            'nama_akun' => 'Bagi Hasil',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B11')->pluck('id')->first(),
        ]);
        DB::table('kode_akuns')->insert([
            'kode_akun' => 'B12',
            'nama_akun' => 'Pengembalian',
            'tipe' => 1,
        ]);
        DB::table('kode_akuns')->insert([
        	'kode_akun' => 'B12.1',
            'nama_akun' => 'Pengembalian Batal Beli',
            'tipe' => 1,
            'parent' => KodeAkun::where('kode_akun','=','B12')->pluck('id')->first(),
        ]);
    }
}
